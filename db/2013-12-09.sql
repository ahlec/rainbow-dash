-- --------------------------------------------------------
-- Host:                         108.175.149.243
-- Server version:               5.5.30-30.1 - Percona Server (GPL), Release rel30.1, Revision 465
-- Server OS:                    Linux
-- HeidiSQL Version:             8.0.0.4396
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table deitloff_rainbow.attendance
CREATE TABLE IF NOT EXISTS `attendance` (
  `recordID` int(200) unsigned NOT NULL AUTO_INCREMENT,
  `member` int(100) NOT NULL,
  `meeting` int(100) NOT NULL,
  PRIMARY KEY (`recordID`)
) ENGINE=MyISAM AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;


-- Dumping structure for table deitloff_rainbow.buildings
CREATE TABLE IF NOT EXISTS `buildings` (
  `buildingID` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `streetAddress` varchar(100) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `isUniversity` tinyint(1) NOT NULL DEFAULT '1',
  `university` int(10) unsigned NOT NULL,
  PRIMARY KEY (`buildingID`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- Dumping data for table deitloff_rainbow.buildings: 5 rows
/*!40000 ALTER TABLE `buildings` DISABLE KEYS */;
INSERT INTO `buildings` (`buildingID`, `name`, `streetAddress`, `isUniversity`, `university`) VALUES
	(1, 'William Pitt Union', 'Forbes Ave', 1, 1),
	(2, 'Cathedral of Learning', '4200 Fifth Avenue', 1, 1),
	(3, 'O\'Hara Student Center', '4024 O\'Hara St', 1, 1),
	(4, 'David Lawrence Hall', 'Forbes Ave', 1, 1),
	(5, 'Bellefield Hall', 'S Bellefield Ave', 1, 1);
/*!40000 ALTER TABLE `buildings` ENABLE KEYS */;


-- Dumping structure for table deitloff_rainbow.checkouts
CREATE TABLE IF NOT EXISTS `checkouts` (
  `checkoutID` int(100) unsigned NOT NULL AUTO_INCREMENT,
  `item` int(10) unsigned NOT NULL,
  `member` int(100) unsigned NOT NULL,
  `checkedOut` date NOT NULL,
  `checkedIn` date DEFAULT NULL,
  PRIMARY KEY (`checkoutID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Dumping data for table deitloff_rainbow.checkouts: 0 rows
/*!40000 ALTER TABLE `checkouts` DISABLE KEYS */;
/*!40000 ALTER TABLE `checkouts` ENABLE KEYS */;


-- Dumping structure for table deitloff_rainbow.kioskPages
CREATE TABLE IF NOT EXISTS `kioskPages` (
  `handle` varchar(30) NOT NULL,
  `fileName` varchar(30) NOT NULL,
  UNIQUE KEY `handle` (`handle`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Dumping data for table deitloff_rainbow.kioskPages: 1 rows
/*!40000 ALTER TABLE `kioskPages` DISABLE KEYS */;
INSERT INTO `kioskPages` (`handle`, `fileName`) VALUES
	('signin', 'signin.php');
/*!40000 ALTER TABLE `kioskPages` ENABLE KEYS */;


-- Dumping structure for table deitloff_rainbow.libraryItems
CREATE TABLE IF NOT EXISTS `libraryItems` (
  `itemID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `media` enum('book','DVD','VHS','magazine','CD') NOT NULL,
  `title` text CHARACTER SET utf8 NOT NULL,
  `authors` text CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `description` text CHARACTER SET utf8 COLLATE utf8_bin,
  `yearPublished` year(4) DEFAULT NULL,
  `monthPublished` int(2) unsigned zerofill DEFAULT NULL,
  `dateAcquired` date DEFAULT NULL,
  `copies` int(3) NOT NULL DEFAULT '1',
  `isbn` varchar(20) NOT NULL,
  `isbnHyphenated` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`itemID`)
) ENGINE=MyISAM AUTO_INCREMENT=274 DEFAULT CHARSET=latin1;

-- Dumping data for table deitloff_rainbow.libraryItems: 273 rows
/*!40000 ALTER TABLE `libraryItems` DISABLE KEYS */;
INSERT INTO `libraryItems` (`itemID`, `media`, `title`, `authors`, `description`, `yearPublished`, `monthPublished`, `dateAcquired`, `copies`, `isbn`, `isbnHyphenated`) VALUES
	(1, 'book', 'Getting Bi: Voices of Bisexuals Around the World', 'Robyn Ochs, Sarah Rowley', 'Getting Bi: Voices of Bisexuals Around the World, Second Edition, edited by Robyn Ochs and Sarah Rowley, is the broadest single collection of bisexual literature available today. Getting Bi collects 220 essays from around the world that explore bisexual identity. Topics include coming out, relationships, politics, community, and more. The book also addresses the intersection of bisexuality with race, class, ethnicity, gender identity, disability and national identity. Authors from 42 countries discuss bisexuality from personal perspectives and their own cultural contexts providing insight into societal views on bisexuality from countries ranging from Colombia to China.', NULL, NULL, NULL, 1, '9780965388153', '978-0-9653881-5-3'),
	(2, 'book', 'Middlesex', 'Jeffrey Eugenides', NULL, NULL, NULL, NULL, 1, '9780374199692', '978-0-374-19969-2'),
	(3, 'book', 'The Cambridge Introduction to Literature and the Environment (Cambridge Introductions to Literature)', 'Timothy Clark, ', 'Environmental criticism is a relatively new discipline that brings the global problem of environmental crisis to the forefront of literary and cultural studies. This introduction defines what ecocriticism is and provides a set of conceptual tools to encourage students to look at the texts they\'re reading in a new way.', NULL, NULL, NULL, 1, '9780521720908', '978-0-521-72090-8'),
	(4, 'book', 'In the courts of the Lord: a gay priest\'s story', 'James Ferry; foreword by John S. Spong', NULL, NULL, NULL, NULL, 1, '9780824513917', '978-0-8245-1391-7'),
	(5, 'book', 'Self-made man: one woman\'s journey into manhood and back again', 'Norah Vincent', 'For more than a year and a half Vincent ventured into the world as Ned, with an ever-present five o\'clock shadow and a crew cut--a perfect disguise that enabled her to observe the world of men as an insider. With her buddies on the bowling league she enjoyed the rough and rewarding embrace of male camaraderie; a stint in a high-octane sales job taught her the gut-wrenching pressures endured by men who would do anything to succeed; she frequented sex clubs, dated women hungry for love but bitter about men, and infiltrated all-male communities including a men\'s therapy group and even a monastery. She ended her journey astounded--and exhausted--by the rigid codes and rituals of masculinity.--From publisher description.', NULL, NULL, NULL, 1, '9780670034666', '978-0-670-03466-6'),
	(6, 'book', 'The love of good women', 'Isabel Miller', NULL, NULL, NULL, NULL, 1, '9780930044817', '978-0-930044-81-7'),
	(7, 'book', 'Oil And Gasoline', 'Billi Gordon, Taylor-Anne Wentworth, ', NULL, NULL, NULL, NULL, 1, '9781555831912', '978-1-55583-191-2'),
	(8, 'book', 'Paz', 'Camarin Grae', NULL, NULL, NULL, NULL, 1, '9780913017029', '978-0-913017-02-9'),
	(9, 'book', 'The well of loneliness', 'Radclyffe Hall; with a commentary by Havelock Ellis', NULL, NULL, NULL, NULL, 1, '9780385416092', '978-0-385-41609-2'),
	(10, 'book', 'The desert of the heart', 'Jane Rule', NULL, NULL, NULL, NULL, 1, '9780930044398', '978-0-930044-39-8'),
	(11, 'book', 'Daughters of a Coral Dawn : A Novel', 'Katherine V. Forrest, ', 'Katherine Forrest\'s best-selling Daughters of a Coral Dawn first appeared in 1984 and became an instant classic. Through seven printings, including the 10th anniversary edition published in 1994, this story of women creating their own world after escaping an oppressive society has continued to gain fans and influence writers for 18 years. Late in the 22nd century, 4,000 women escape the tyranny of a male-dominated Earth and colonize the planet of Maternas. The story of how these pioneers created a society and culture in accord with their nature makes up the heart of this exhilarating, erotic, and hauntingly beautiful novel. But men eventually discover Maternas, and the women are faced with a critical choice.Katherine Forrest is also the author of the lesbian romantic classic Curious Wine as well as the groundbreaking Kate Delafield mystery series that includes the best-sellers Apparition Alley, Liberty Square, and Sleeping Bones. She lives in San Francisco.', NULL, NULL, NULL, 1, '9781555836627', '978-1-55583-662-7'),
	(12, 'book', 'Sleeping bones', 'by Katherine V. Forrest', '"Katherine V. Forrest has emerged as an author of mystery novels featuring lesbian LAPD homicide detective and ex-Marine Kate Delafield." "The world continues to change around LAPD detective Kate Delafield. She has a new, iron-willed female lieutenant, a tough new partner who may turn out to be a much-needed ally, and a new case that is going to test her mettle to the utmost. When a reclusive old man is brutally and silently murdered at the La Brea Tar Pits, Kate is inexorably drawn into a mystery that can uncover the truth of humanity\'s ancient past and that threatens to expose the corruption and violence of the present. And everyone involved - from an alluring scientist with a dark secret to a treacherous CIA officer with his own agenda - is a suspect. With more at stake than just a pile of bones, Kate has to gather together the pieces of a timeless puzzle, and make sure they all fit - before a remorseless killer decides to make her a part of history..."--BOOK JACKET.', NULL, NULL, NULL, 1, '9780425174845', '978-0-425-17484-5'),
	(13, 'book', 'Nervous conditions: a novel', 'Tsitsi Dangarembga', NULL, NULL, NULL, NULL, 1, '9780931188749', '978-0-931188-74-9'),
	(14, 'book', 'Charleyhorse', 'Cecil Dawkins', NULL, NULL, NULL, NULL, 1, '9780140080100', '978-0-14-008010-0'),
	(15, 'book', 'Just hold me', 'by Linda Parks', NULL, NULL, NULL, NULL, 1, '9780917597022', '978-0-917597-02-2'),
	(16, 'book', 'Kick Back: A Kate Brannigan Mystery', 'V.L. McDermid, ', 'In the second in the series, Kate Brannigan investigates the bizarre case of the missing conservatories (sunrooms attached to the back of a standing house), unearthing an unscrupulous lawyer (later found hanging from his balcony in flimsy women\'s apparel); a supersalesman with an overstuffed bank account; and a brother-sister pair who are double-crossing everyone. And this real estate fiddling ties in with a separate scam involving Kate\'s best buds - and murder of the most unexpected order.', NULL, NULL, NULL, 1, '9781883523459', '978-1-883523-45-9'),
	(17, 'book', 'Breakfast on Pluto tie-in', 'Patrick McCabe, ', 'Conceived in a moment of mad passion by a randy Irish priest and his temporary housekeeper -- and abandoned on a doorstep in a Rinso box as an infant -- her ladyship "Pussy" (né Patrick) Braden grew up fabulous and escaped tiny Tyreelin, Ireland, to start life anew in London. In blousy tops and satin miniskirts she plies her trade as a transvestite rent boy on Picadilly\'s Meat Rack, risking life and limb among the city\'s flotsam and jetsam. But it is the 1970s, and fear haunts the streets of London and Belfast -- and as radioactive history approaches critical mass, the coming explosion of violence and tragedy may well blow Pussy\'s fragile soul asunder.', NULL, NULL, NULL, 1, '9780061121869', '978-0-06-112186-9'),
	(18, 'book', 'Patience and Sarah', 'by Isabel Miller', NULL, NULL, NULL, NULL, 1, '9780449238509', '978-0-449-23850-9'),
	(19, 'book', 'The new Joy of gay sex', 'Charles Silverstein and Felice Picano; preface by Edmund White', NULL, NULL, NULL, NULL, 1, '9780060168131', '978-0-06-016813-1'),
	(20, 'book', 'Close range: Wyoming stories', 'Annie Proulx; watercolors by William Matthews', NULL, NULL, NULL, NULL, 1, '9780684852225', '978-0-684-85222-5'),
	(21, 'book', 'The Laramie project', 'by Moisés Kaufman and the members of Tectonic Theatre Project; head writer, Leigh Fondakowski; associate writers, Stephen Belber, Greg Pierotti, Stephen Wangh; dramaturgs, Amanda Gronich... [et al.]', NULL, NULL, NULL, NULL, 1, '9780375727191', '978-0-375-72719-1'),
	(22, 'book', 'Geography Club', 'Brent Hartinger', 'A group of gay and lesbian teenagers finds mutual support when they form the "Geography Club" at their high school.', NULL, NULL, NULL, 1, '9780060012236', '978-0-06-001223-6'),
	(23, 'book', 'Rainbow Boys', 'Alex Sanchez', 'Three high school seniors, a jock with a girlfriend and an alcoholic father, a closeted gay, and a flamboyant gay rights advocate, struggle with family issues, gay bashers, first sex, and conflicting feelings about each other.', NULL, NULL, NULL, 1, '9780689841002', '978-0-689-84100-2'),
	(24, 'book', 'Rainbow High', 'Alex Sanchez', 'Summary: Follows three gay high school seniors as they struggle with issues of coming out, safe sex, homophobia, being in love, and college choices.', NULL, NULL, NULL, 1, '9780689854781', '978-0-689-85478-1'),
	(25, 'book', 'Rainbow road', 'Alex Sanchez', 'While driving across the United States during the summer after high school graduation, three young gay men encounter various bisexual and homosexual people and make some decisions about their own relationships and lives.', NULL, NULL, NULL, 1, '9781416911913', '978-1-4169-1191-3'),
	(26, 'book', 'Queer and pleasant danger: writing out my life', 'Louise Rafkin', NULL, NULL, NULL, NULL, 1, '9780939416615', '978-0-939416-61-5'),
	(27, 'book', 'Push: a novel', 'by Sapphire', NULL, NULL, NULL, NULL, 1, '9780679766759', '978-0-679-76675-9'),
	(28, 'book', 'Tipping the Velvet', 'Sarah Waters, ', 'This stunning and steamy debut chronicles the adventures of Nan King, a small-town girl at the turn of the century whose life takes a wild turn of its own when she follows a local music hall star to London..."Glorious...a sexy, sinewy sojourn of a young woman in turn-of-the-century England."--The Boston Globe "Erotic and absorbing...If lesbian fiction is to reach a wider readership, Waters is the person to carry the banner."--The New York Times Book Review"Wonderful...a sensual experience that leaves the reader marveling at the author\'s craftsmanship, idiosyncrasy and sheer effort."--The San Francisco Chronicle"Amazing....This is the lesbian novel we\'ve all been waiting for."--Salon.com"Compelling...Readers of all sexes and orientations should identify with this gutsy hero as she learns who she is and how to love."--Newsday"Echoes of Tom Jones, Great Expectations...Waters\'s debut offers terrific entertainment: pulsating with highly charged (and explicitly presented) erotic heat."--Kirkus Reviews (starred review)', NULL, NULL, NULL, 1, '9781573227889', '978-1-57322-788-9'),
	(29, 'book', 'Bi men: coming out every which way', 'Ron Jackson Suresha, Pete Chvany, editors', NULL, NULL, NULL, NULL, 1, '9781560236153', '978-1-56023-615-3'),
	(30, 'book', 'The woman who gave birth to rabbits: stories', 'Emma Donoghue', NULL, NULL, NULL, NULL, 1, '9780151009374', '978-0-15-100937-4'),
	(31, 'book', 'Slammerkin', 'Emma Donoghue', 'Born to rough cloth in working-class London in 1748, Mary Saunders hungers for linen and lace. Her lust for a shiny red ribbon leads her to a life of prostitution at a young age, where she encounters a freedom unknown to virtuous young women. But a dangerous misstep sends her fleeing to Monmouth and the refuge of the middle-class household of Mrs. Jones, to become the seamstress her mother always expected her to be and to live the ordinary life of an ordinary girl. Although Mary becomes a close confidante of Mrs. Jones, her desire for a better life leads her back to prostitution. She remains true only to the three rules she learned on the streets of London: Never give up your liberty; Clothes make the woman; Clothes are the greatest lie ever told. In the end, it is clothes, their splendor and their deception, that lead Mary to disaster. Emma Donoghue\'s daring, sensually charged prose casts a new sheen on the squalor and glamour of eighteenth-century England. Accurate, masterfully written, and infused with themes that still bedevil us today, Slammerkin is historical fiction for all readers.', NULL, NULL, NULL, 1, '9780156007474', '978-0-15-600747-4'),
	(32, 'book', 'Voyages Out 2: Lesbian short fiction', 'by Julie Blackwomon and Nona Caspers', NULL, NULL, NULL, NULL, 1, '9780931188909', '978-0-931188-90-9'),
	(33, 'book', 'Last September', 'by Helen R. Hull', NULL, NULL, NULL, NULL, 1, '9780941483094', '978-0-941483-09-4'),
	(34, 'book', 'What are big girls made of?: poems', 'by Marge Piercy', NULL, NULL, NULL, NULL, 1, '9780679765943', '978-0-679-76594-3'),
	(35, 'book', 'Love alone: 18 elegies for Rog', 'by Paul Monette', NULL, NULL, NULL, NULL, 1, '9780312026028', '978-0-312-02602-8'),
	(36, 'book', 'The new gay teenager', 'Ritch C. Savin-Williams', NULL, NULL, NULL, NULL, 1, '9780674022560', '978-0-674-02256-0'),
	(37, 'book', 'Fun home: a family tragicomic', 'Alison Bechdel', NULL, NULL, NULL, NULL, 1, '9780618477944', '978-0-618-47794-4'),
	(38, 'book', 'Openly Bob', 'Bob Smith', NULL, NULL, NULL, NULL, 1, '9780688151201', '978-0-688-15120-1'),
	(39, 'book', 'Am I blue?: coming out from the silence', 'edited by Marion Dane Bauer', 'A collection of short stories about homosexuality by such authors as Bruce Coville, M.E. Kerr, William Sleator, and Jane Yolen.', NULL, NULL, NULL, 1, '9780064405874', '978-0-06-440587-4'),
	(40, 'book', 'Look homeward, erotica: more mischief', 'by the Kensington Ladies\' Erotica Society; illustrated by Pat Adler', NULL, NULL, NULL, NULL, 1, '9780898151954', '978-0-89815-195-4'),
	(41, 'book', 'Why marriage matters: America, equality, and gay people\'s right to marry', 'Evan Wolfson', NULL, NULL, NULL, NULL, 1, '9780743264587', '978-0-7432-6458-7'),
	(42, 'book', 'Dreams and swords', 'Katherine V. Forrest', NULL, NULL, NULL, NULL, 1, '9780941483032', '978-0-941483-03-2'),
	(43, 'book', 'Out front: contemporary gay and lesbian plays', 'edited and with an introduction by Don Shewey', NULL, NULL, NULL, NULL, 1, '9780802130259', '978-0-8021-3025-9'),
	(44, 'book', 'You\'re not from around here, are you?: a lesbian in small-town America', 'Louise A. Blum', NULL, NULL, NULL, NULL, 1, '9780299170943', '978-0-299-17094-3'),
	(45, 'book', 'Fire in the wind: the life of Dickey Chapelle', 'Roberta Ostroff', NULL, NULL, NULL, NULL, 1, '9780345362742', '978-0-345-36274-2'),
	(46, 'book', 'Out on Fraternity Row: Personal Accounts of Being Gay in a College Fraternity', 'Shane L. Windmeyer, Pamela W. Freeman, Lambda 10 Project, ', NULL, NULL, NULL, NULL, 1, '9781555834098', '978-1-55583-409-8'),
	(47, 'book', 'That\'s Mr. Faggot to You : Further Trials from My Queer Life', 'Michael Thomas Ford, ', 'Michael Thomas Ford garnered lots of laughs in 1998 with Alec Baldwin Doesn\'t Love Me and Other Trials from My Queer Life. The follow-up collection of pieces from his syndicated column, That\'s Mr. Faggot to You, continues Ford\'s exploration of contemporary gay life. In the title essay, reports of a teenager who successfully sued his school district for failing to prevent physical and mental abuse by his classmates prompts Ford to recall his own traumatic high school experiences and leads him to recognize that, years later, "he is happier, more successful, and a great deal more attractive" than his classmates. In other essays, he discusses the you-and-me-against-the-world relationship he has with his black Labrador, proposes a new line of Christian-friendly action figures (including a Jonah and the Whale Play Set, "appropriate for bath-time use or fun in the pool"), and even manages, despite his uncertainties, to offer an adolescent nephew dating advice (concluding that "guy problems were guy problems, regardless of who the person creating the dilemma was or how many holes she or he had"). That\'s Mr. Faggot to You is a humorous slice of contemporary gay life that\'s bound at least to elicit a smile from any reader.', NULL, NULL, NULL, 1, '9781555834968', '978-1-55583-496-8'),
	(48, 'book', 'Two Truths and a Lie', 'Scott Turner Schofield, ', 'Two Truths and a Lie is a memoir in the form of three solo plays written and performed by Scott Turner Schofield. From inside the often hilarious-but all too real-moments of his young life on the Homecoming Court and Debutante Ball circuit (in a dress), armed with only a decoder ring and a gifted tongue, Schofield comes out with truly unbelievable stories of a body in search of an identity. By turns slapstick and slap-to-the-face, this drama invites audiences and readers to explore gender, sex, sexuality, and self in their own first person.', NULL, NULL, NULL, 1, '9780978597320', '978-0-9785973-2-0'),
	(49, 'book', 'Man in the middle', 'John Amaechi with Chris Bull', NULL, NULL, NULL, NULL, 1, '9781933060194', '978-1-933060-19-4'),
	(50, 'book', 'Jack Nichols, Gay Pioneer: "Have You Heard My Message?"', 'J. Louis Campbell III JD PhD', 'The story of a man who fought to leave a better world than he found One of the founders of the gay and lesbian liberation movement, Jack Nichols was a warrior for gay equality. Jack Nichols, Gay Pioneer: "Have You Heard My Message?" is his story, blending the man, the movement, and the moment into a memory of his life\'s work. This powerful biography captures the wisdom, passion, and spirit of a man who refused to be silent at a time when even gay thoughts were considered by society to be a crime of the soul. As a journalist, activist, co-founder of the Mattachine Societies of Washington, DC, and Florida, and editor of the first gay weekly newspaper in the United States, Jack Nichols left a legacy of gay rights, gay pride, and tremendous courage. Jack Nichols, Gay Pioneer: "Have You Heard My Message?" charts the life of this vastly important figure from his childhood in the suburbs of Washington, DC, to his final impassioned days in a Florida cancer treatment center in 2005. Braided throughout this crucial text is not only the history of the man, but also the ideas he used to bring the movement to critical mass and the sources of those ideas so influential to all of his work--before Stonewall and after, during the AIDS epidemic, and beyond. Here is everything from the early influence of Burns and Whitman to Nichols\'s revolutionary relationship with Lige Clark to the integration of androgyny and anarchism into his activist philosophy. It is the story of a prolific activist and inspirational human being. Among the many important topics detailed in Jack Nichols, Gay Pioneer: "Have You Heard My Message?" are his: Family history and its unique influence on his activist tendencies Relationship with Lige Clark becoming "the most famous homosexuals in America" Plan for the social construction of androgyny Attack on the psychiatric establishment\'s theory of homosexuality as a "sickness" Work and vision in men\'s liberation Groundbreaking editorship of American\'s first gay weekly newspaper, GAY use of Walt Whitman as the "fountainhead" of the homosexual movement Incorporation of anarchistic theory into his economic and gender ideas and countless more! Thoughtful and moving, Jack Nichols, Gay Pioneer: "Have You Heard My Message?" is a compelling look at the man and the movement. With a wealth of hard-to-find summaries of underground gay journalism, detailed references, personal photographs, and a complete bibliography of Nichols\'s major writings, Jack Nichols, Gay Pioneer: "Have You Heard My Message?" is an indispensable resource for anyone interested in the history and future of LGBT movements, as well as students, educators, and researchers seeking a comprehensive and thorough treatment of this revolutionary figure.', NULL, NULL, NULL, 1, '9781560236535', '978-1-56023-653-5'),
	(51, 'book', 'When I knew', 'edited by Robert Trachtenberg; illustrated by Tom Bachtell', NULL, NULL, NULL, NULL, 1, '9780060571467', '978-0-06-057146-7'),
	(52, 'book', 'From boys to men: gay men write about growing up', 'edited by Robert Williams and Ted Gideonse', NULL, NULL, NULL, NULL, 1, '9780786716326', '978-0-7867-1632-6'),
	(53, 'book', 'Best Date Ever (Lesbian): True Stories That Celebrate Lesbian Relationships', 'Linda Alvarez (Editor)', 'This unique new collection explores the notion of romance, inspiring couples to rekindle their love and promising the single and searching that not every date is over before it\'s even begun. Linda Alvarez is the editor of Dyke the Halls: Lesbian Erotic Christmas Tales. She lives in New York City.', NULL, NULL, NULL, 1, '9781593500092', '978-1-59350-009-2'),
	(54, 'book', 'Breaking the Surface', 'Greg Louganis, Eric Marcus (Contributor)', 'Greg Louganis won back-to-back double gold medals at the 1984 and 1988 Olympics, and his amazing physique and handsome face should have made him a media superstar. Yet Louganis\'s struggles with self-doubt lack of confidence held him back personally and professionally. He only achieved real happiness after coming out as an HIV-positive gay man. This is a thoughtful, sensitive portrait of a man whose insecurities nearly destroyed him, but who found the love and inner strength to save himself.', NULL, NULL, NULL, 1, '9781402206665', '978-1-4022-0666-5'),
	(55, 'book', 'An underground life: memoirs of a gay Jew in Nazi Berlin', 'Gad Beck; written with Frank Heibert; translated from the German by Allison Brown', NULL, NULL, NULL, NULL, 1, '9780299165048', '978-0-299-16504-8'),
	(56, 'book', 'Zami, a new spelling of my name', 'Audre Lorde', NULL, NULL, NULL, NULL, 1, '9780895941220', '978-0-89594-122-0'),
	(57, 'book', 'The Man Who Would Marry Susan Sontag: And Other Intimate Literary Portraits of the Bohemian Era (Living Out: Gay and Lesbian Autobiog)', 'Edward Field, ', 'Long before Stonewall, young Air Force veteran Edward Field, fresh from combat in WWII, threw himself into New York&rsquo;s literary bohemia, searching for fulfillment as a gay man and poet. In this vivid account of his avant-garde years in Greenwich Village and the bohemian outposts of Paris&rsquo;s Left Bank and Tangier—where you could write poetry, be radical, and be openly gay—Field&rsquo;s intimate portraits of literary contemporaries such as Susan Sontag, Alfred Chester, May Swenson, and Frank O&rsquo;Hara bring back the sadness, bawdiness, humor, and romanticism of the nigh-forgotten postwar bohemian subculture.', NULL, NULL, NULL, 1, '9780299213244', '978-0-299-21324-4'),
	(58, 'book', 'Auden in love', 'by Dorothy J. Farnan', NULL, NULL, NULL, NULL, 1, '9780452007727', '978-0-452-00772-7'),
	(59, 'book', 'Farm Boys: Lives of Gay Men from the Rural Midwest', 'Will Fellows, ', 'First time in paperback, with a newly designed cover. Homosexuality is often seen as a purely urban experience, far removed from rural and small-town life. Farm Boys undermines that cliche by telling the stories of more than three dozen gay men, ranging in age from 24 to 84, who grew up on farms in the American midwest. Named among the best books of 1996 by Esquire Magazine and the Minneapolis Star Tribune', NULL, NULL, NULL, 1, '9780299150846', '978-0-299-15084-6'),
	(60, 'book', 'February House: The Story of W. H. Auden, Carson McCullers, Jane and Paul Bowles, Benjamin Britten, and Gypsy Rose Lee, Under One Roof in Brooklyn', 'Sherill Tippins, ', 'The ramshackle Brooklyn brownstone dubbed "February House" has itself been demolished, but the legacy of this literary bastion lives on in Sherill Tippins\'s wonderful account that, as the San Francisco Chronicle notes, is a "tribute to the location that gives it mythical status." This is the incredible story of an artistic fraternity that included, among others, Carson McCullers, W. H. Auden, Paul Bowles, and the famed burlesque performer Gypsy Rose Lee, all living together under one roof. In the one year of its existence, these burgeoning talents composed many of their most famous, iconic literary works while experiencing together a crucial historical moment America at the threshold of World War II.', NULL, NULL, NULL, 1, '9780618711970', '978-0-618-71197-0'),
	(61, 'book', 'First Person Queer: Who We Are (So Far)', 'Richard LabontÃ© (Editor), Lawrence Schimel (Editor)', 'In this amazing, wide-ranging anthology of nonfiction essays, contributors write intimate and honest first-person accounts of queer experience, from coming out to "passing" as straight to growing old to living proud. These are the stories of contemporary gay and lesbian life-and by definition, are funny, sad, hopeful, and truthful. Representing a diversity of genders, ages, races, and orientations, and edited by two acclaimed writers and anthologists (who between them have written or edited almost one hundred books), First Person Queer puts the "personal" back into "queer."', NULL, NULL, NULL, 1, '9781551522272', '978-1-55152-227-2'),
	(62, 'book', 'Here\'s What We\'ll Say: Growing Up, Coming Out, and the U.S. Air Force Academy', 'Reichen Lehmkul, ', 'Reichen Lehmkuhl is perhaps best known for the ambition, intelligence, and athleticism that won him the grand prize on CBS\'s Amazing Race. Since winning the million-dollar prize, Lehmkuhl has gone on to find success acting in film and television. However, he played the biggest role of his life long before his professional acting debut, when he was forced to hide his sexuality to comply with the Air Force\'s "Don\'t Ask, Don\'t Tell" policy. Here\'s What We\'ll Say tells the harrowing inside story of what happens when cadets who are committed to serving their nation\'s military figure out that they are in fact gay. With no way out and no place to turn for protection, a new code of conduct emerged among gay and lesbian cadets that helped ensure their safety. Gathering secretly in various locations, cadets formed a hidden network. To guarantee the privacy of individuals in attendance, however, each meeting opened with, "Here\'s what we\'ll say…" — a pledge so sacred that the group had it inscribed on the inside of their class rings.', NULL, NULL, NULL, 1, '9780786720354', '978-0-7867-2035-4'),
	(63, 'book', 'Conduct unbecoming: gays and lesbians in the U. S. military', 'Randy Shilts', NULL, NULL, NULL, NULL, 1, '9780312342647', '978-0-312-34264-7'),
	(64, 'book', 'Your right to privacy: a basic guide to legal rights in an information society', 'Evan Hendricks, Trudy Hayden, Jack D. Novick', NULL, NULL, NULL, NULL, 1, '9780809316328', '978-0-8093-1632-8'),
	(65, 'book', 'My Guy: A Gay Man\'s Guide to a Lasting Relationship', 'Martin Kantor, ', 'Find and keep Mr. Right. Most single gay men wish they were in a relationship but dont recognize that they have set up obstacles to a happy, long-lasting connection. My Guy offers a step-by-step approach that any gay man can use to take charge of his life, master his flaws through self-awareness and dedicate himself to creating a great relationship for himself and his lover. Dr. Martin Kantor, a psychiatrist who specializes in gay relationships, shows readers the benefits of lasting relationships, and then tells them how to: o Find Mr. Right and avoid Mr. Wrong o develop a strong, resilient relationship o never give up on love', NULL, NULL, NULL, 1, '9781570719677', '978-1-57071-967-7'),
	(66, 'book', 'My lives', 'Edmund White', NULL, NULL, NULL, NULL, 1, '9780066213972', '978-0-06-621397-2'),
	(67, 'book', 'Moab is my washpot: an autobiography', 'Stephen Fry', NULL, NULL, NULL, NULL, 1, '9780375502644', '978-0-375-50264-4'),
	(68, 'book', 'The trials of Radclyffe Hall', 'Diana Souhami', NULL, NULL, NULL, NULL, 1, '9780297818250', '978-0-297-81825-0'),
	(69, 'book', 'Beloved sisters and loving friends: letters from Rebecca Primus of Royal Oak, Maryland, and Addie Brown of Hartford, Connecticut, 1854-1868', 'edited by Farah Jasmine Griffin', NULL, NULL, NULL, NULL, 1, '9780679451280', '978-0-679-45128-0'),
	(70, 'book', 'View from another closet: exploring bisexuality in women', 'Janet Bode', NULL, NULL, NULL, NULL, 1, '9780801583131', '978-0-8015-8313-1'),
	(71, 'book', 'Third sex, third gender: beyond sexual dimorphism in culture and history', 'edited by Gilbert Herdt', NULL, NULL, NULL, NULL, 1, '9780942299823', '978-0-942299-82-3'),
	(72, 'book', '10 Smart Things Gay Men Can Do to Find Real Love', 'Joe Kort, ', 'The author of the best-selling 10 Smart Things Gay Men Can Do to Improve Their Lives turns his attention to the burning question of love. "There are few books for gay men on not only what to look for in Mr. Right but how to become Mr. Right. My book will address both. It is not just about finding him, it is what you do after you find him," says author Joe Kort. A certified Imago Relationship Therapist, Kort has employed the ideas put forth by Imago founder Harville Hendrix to transform the lives and relationships of the countless gay couples he has worked with in 20 years of private practice. In "Your Sexual Shadow," one of his new book\'s 10 life-altering chapters, Kort unveils a surprising and groundbreaking idea that explores how decoding sexual fantasies can often unlock the mystery to what gay men are looking for in a partner and why. This will be particularly elucidating to men who have been conditioned to believe their sexual fantasies are an obstacle to long-term relationships. How can the secret logic of "dark" sexual desires help you find Mr. Right? "So many of my clients say they have to get better before they find Mr. Right," reports Kort. "I think that is often a reason to avoid relationships and simply not true." His new book is a practical guide to set gay men on the path to true love today. Joe Kort is a therapist in private practice since 1985, specializing in gay-affirmative psychotherapy as well as Imago Relationship Therapy, which is a specific program involving communication exercises designed for couples to enhance their relationship and for singles to learn relationship skills. His first book, 10 Smart Things Gay Men Can Do to Improve Their Lives, was a national gay and lesbian bestseller.', NULL, NULL, NULL, 1, '9781555838980', '978-1-55583-898-0'),
	(73, 'book', 'Ten Smart Things Gay Men Can Do to Improve Their Lives', 'Joe Kort, ', 'Openly gay therapist Joe Kort provides 10 powerful and positive steps gay men can take to isolate and overcome self-defeating behavior patterns, and move in healthier and more rewarding directions: Take Charge of Their Own Lives Affirm Themselves by Coming Out Resolve Differences With Parents and Relatives "Graduate" From Delayed Adolescence Avoid -- or Overcome -- Sexual Addiction -Learn from Successful Mentors Who\'ve Been There, Done That Take Advantage of "Therapy Workouts" Achieve -- and Maintain -- Rewarding Relationships Understand the Stages of Loves Commit to Their PartnerThese solid and reliable "Top 10" life steps that have been most helpful to Joe Kort\'s clients in his 16 years of working with hundreds of gay men, are presented in an engaging and easy-to-understand manner and are supplemented by case histories from his practice. These are time-tested, practical decisions gay men can make in their search for emotional, sexual and personal fulfillment.', NULL, NULL, NULL, 1, '9781555837822', '978-1-55583-782-2'),
	(74, 'book', 'Boyfriend 101: a gay guy\'s guide to dating, romance, and finding true love', 'Jim Sullivan', NULL, NULL, NULL, NULL, 1, '9780812992199', '978-0-8129-9219-9'),
	(75, 'book', 'The lesbian couples\' guide: finding the right woman and creating a life together', 'Judith McDaniel', NULL, NULL, NULL, NULL, 1, '9780060950217', '978-0-06-095021-7'),
	(76, 'book', 'Why can\'t Sharon Kowalski come home?', 'by Karen Thompson and Julie Andrzejewski', NULL, NULL, NULL, NULL, 1, '9780933216464', '978-0-933216-46-4'),
	(77, 'book', 'Unspoken rules: sexual orientation and women\'s human rights', 'edited by Rachel Rosenbloom; with a foreword by Charlotte Bunch', NULL, NULL, NULL, NULL, 1, '9780304337644', '978-0-304-33764-4'),
	(78, 'book', 'A face in the crowd: expressions of gay life in America', 'edited by John Peterson and Martin Bedogne', NULL, NULL, NULL, NULL, 1, '9780971961807', '978-0-9719618-0-7'),
	(79, 'book', 'St. James Press gay & lesbian almanac', 'editor, Neil Schlager; with foreword by R. Ellen Greenblatt', NULL, NULL, NULL, NULL, 1, '9781558623583', '978-1-55862-358-3'),
	(80, 'book', 'Gay New York: gender, urban culture, and the makings of the gay male world, 1890-1940', 'George Chauncey', '"This brilliant work shatters the myth that before the 1960s gay life existed only in the closet where gay men were isolated, invisible, and self-hating. Based on years of research and access to a rich trove of public and private documents, including the diaries of gay men living in New York at the turn of the century, this book is a fascinating look at a gay world that was not supposed to have existed." "Focusing on New York City, the gay capital of the nation for nearly a century, George Chauncey recreates the saloons, speakeasies, and cafeterias where gay men gathered, the intimate parties and immense drag balls where they celebrated, and the highly visible residential enclaves they built in Greenwich Village, Harlem, and Times Square. He tours New York\'s turn-of-the-century sexual underground, including gay bathhouses and backroom saloons. He chronicles the now-forgotten "pansy craze" of the Prohibition years, when Times Square\'s most successful nightclubs featured openly gay entertainers and when drag balls held in Madison Square Garden and Harlem\'s largest ballrooms drew thousands of spectators and banner headlines in city newspapers. And he reconstructs the codes of dress, speech, and style gay men developed to recognize and communicate with one another in hostile settings, which enabled many men not just to survive but to flourish. Gay New York offers new perspectives on the gay rights revolution of our time by showing that the oppression the gay and lesbian movement attacked in the 1960s was not an unchanging phenomenon. It had intensified in the 1920s and 1930s as a direct response to the visibility of the gay world in those years." "Above all, Gay New York shows that our most intimate sexual identities are stunningly recent creations. It depicts a complex prewar sexual culture in which men were not divided into homosexuals and heterosexuals but into fairies, wolves, queers, and "normal" men. Many of those "normal" men frequently engaged in sexual relations with other men, because sexual normality was not defined by exclusive heterosexuality." "This book will change forever the way we think about the gay past - and the American past."--BOOK JACKET.', NULL, NULL, NULL, 1, '9780465026210', '978-0-465-02621-0'),
	(81, 'book', 'Lesbians, Gay Men, and the Law (The New Press Law in Context Series)', 'William B. Rubenstein (Editor)', NULL, NULL, NULL, NULL, 1, '9781565840379', '978-1-56584-037-9'),
	(82, 'book', 'Same Sex in the City : So Your Prince Charming Is Really a Cinderella', 'Lauren Levin, Lauren Blitzer, Sam Bassett (Photographer)', NULL, NULL, NULL, NULL, 1, '9781416916321', '978-1-4169-1632-1'),
	(83, 'book', 'Sexual politics, sexual communities: the making of a homosexual minority in the United States, 1940-1970', 'John D\'Emilio', NULL, NULL, NULL, NULL, 1, '9780226142678', '978-0-226-14267-8'),
	(84, 'book', 'Virtually normal: an argument about homosexuality', 'Andrew Sullivan', NULL, NULL, NULL, NULL, 1, '9780679423829', '978-0-679-42382-9'),
	(85, 'book', 'Out for good: the struggle to build a gay rights movement in America', 'Dudley Clendinen and Adam Nagourney', NULL, NULL, NULL, NULL, 1, '9780684867434', '978-0-684-86743-4'),
	(86, 'book', 'Stonewall', 'Martin Duberman', NULL, NULL, NULL, NULL, 2, '9780452272064', '978-0-452-27206-4'),
	(87, 'book', 'Harvard\'s Secret Court: The Savage 1920 Purge of Campus Homosexuals', 'William Wright, ', 'In May of 1920, Cyril Wilcox, a freshman suspended from Harvard, was found sprawled dead on his bed, his room filled with gasa suicide. The note he left behind revealed his secret life as part of a circle of homosexual students.The resulting witch hunt and the lives it cost remains one of the most shameful episodes in the history of Americas premier university. Supported by legendary Harvard President Lawrence Lowell, Harvard conducted its investigation in secrecy. Several students committed suicide; others had their lives destroyed by an ongoing effort on the part of Harvard to destroy their reputations. Harvards Secret Court is a deeply moving indictment of the human toll of intolerance and the horrors of injustice that can result when a powerful institution loses its balance.', NULL, NULL, NULL, 1, '9780312322724', '978-0-312-32272-4'),
	(88, 'book', 'City Of Sisterly And Brotherly Loves: Lesbian And Gay Philadelphia, 1945-1972', 'Marc Stein, ', 'Marc Stein\'s City of Sisterly and Brotherly Loves is refreshing for at least two reasons: it centers on a city that is not generally associated with a vibrant gay and lesbian culture, and it shows that a community was forming long before the Stonewall rebellion. In this lively and well received book, Marc Stein brings to life the neighborhood bars and clubs where people gathered and the political issues that rallied the community. He reminds us that Philadelphians were leaders in the national gay and lesbian movement and, in doing so, suggests that New York and San Francisco have for too long obscured the contributions of other cities to gay culture.', NULL, NULL, NULL, 1, '9781592131303', '978-1-59213-130-3'),
	(89, 'book', 'Queer Street: Rise and Fall of an American Culture, 1947-1985', 'James McCourt, ', '"A heroically imaginative account of gay metropolitan culture, an elegy and an apologia for a generation."—New York Times Book ReviewA fierce critical intelligence animates every page of Queer Street. Its sentences are dizzying divagations. The postwar generation of queer New York has found a sophisticated bard singing \'the elders\' history\' (The New York Times). James McCourt\'s seminal Queer Street has proven unrivaled in its ability to capture the voices of a mad, bygone era. Beginning with the influx of liberated veterans into downtown New York and barreling through four decades of crisis and triumph up to the era of the floodtide of AIDS, McCourt positions his own exhilarating experience against the whirlwind history of the era. The result is a commanding and persuasive interlocking of personal, intellectual, and social history that will be read, dissected, and honored as the masterpiece it is for decades to come. A Publishers Weekly Best Book of 2003; a Lambda Award finalist.', NULL, NULL, NULL, 1, '9780393326406', '978-0-393-32640-6'),
	(90, 'book', 'Hostile Climate 2000: Report on Anti-Gay Activity', 'Carol Keys (Editor)', NULL, NULL, NULL, NULL, 2, '9781890780050', '978-1-890780-05-0'),
	(91, 'book', 'Behind the Mask of the Mattachine: The Hal Call Chronicles And the Early Movement for Homosexual Emancipation (Haworth Gay and Lesbian Studies)', 'James T. Sears, ', 'A 19th Annual Lambda Literary Awards Finalist! Take a revealing look at gay history--and the man who helped kickstart gay activism in today\'s society The Mattachine is the origin of the contemporary American gay movement. One of the major players in this movement was Hal Call, America\'s first openly gay journalist and the man most responsible for the end of government censorship of frontal male nude photography through the mail. Behind the Mask of the Mattachine: The Early Movement for Homosexual Emancipation, the Hal Call Chronicles travels back to the times before Stonewall and its aftermath, to the beginnings of the modern homosexual movement and the lesser-known individuals who started it. This stunning chronicle gives the unexpurgated history of the activists who organized homosexuals--using the biography of the controversial Hal Call as its springboard. Behind the Mask of the Mattachine provides a revealing illustration of gay life in the past through an intergenerational history of the early gay men\'s movement. Noted author James T. Sears generously weaves oral history, seldom seen historical documents, and rare photographs to provide a rich behind-the-scenes look at the first wave of Mattachine activists and the emerging gay pornography industry. This historical chronicle of a previously neglected era is packed with details of Call\'s personal struggles, his celebration of the phallus, and his assertion linking homophobia and heteronormativity to our culture\'s sex-negative tradition. The reader is transported to the underworld of youthful hustlers, porno kingpins, spurned lovers, sex clubs, cruising grounds, secretive societies, and personal in-fighting over the direction of gay activism. This enthralling narrative is impeccably referenced. Behind the Mask of the Mattachine examines: The origins of the Mattachine Society The Mattachine Foundation of Harry Hay and others of the "Fifth Order" The Weimar Republic in Germany--the roots of the modern homosexual movement Networking of homosexuals through correspondence clubs and speakeasies in Depression-era America The intense rivalries between San Francisco and New York City Mattachine groups Censorship of books, magazines, and films And much more! The book explores the lives of three generations of pre-Stonewall gay activists: Magnus Hirschfeld and Benedikt Friedländer Henry Gerber and Manual boyFrank Harry Hay and Hal Call Behind the Mask of the Mattachine puts a needed spotlight on a time in lesser-known gay history, and makes illuminating reading for historians and gay persons interested in the history of the gay men\'s movement.', NULL, NULL, NULL, 1, '9781560231875', '978-1-56023-187-5'),
	(92, 'book', 'The riddle of gender: science, activism, and transgender rights', 'Deborah Rudacille', NULL, NULL, NULL, NULL, 1, '9780375421624', '978-0-375-42162-4'),
	(93, 'book', 'Surpassing the love of men: romantic friendship and love between women from the Renaissance to the present', 'Lillian Faderman', NULL, NULL, NULL, NULL, 1, '9780704339774', '978-0-7043-3977-4'),
	(94, 'book', 'Odd girls and twilight lovers: a history of lesbian life in twentieth-century America', 'Lillian Faderman', NULL, NULL, NULL, NULL, 1, '9780140171228', '978-0-14-017122-8'),
	(95, 'book', 'Becoming visible: an illustrated history of lesbian and gay life in twentieth-century America', 'Molly McGarry and Fred Wasserman', NULL, NULL, NULL, NULL, 1, '9780670864010', '978-0-670-86401-0'),
	(96, 'book', 'Intimate companions: a triography of George Platt Lynes, Paul Cadmus, Lincoln Kirstein, and their circle', 'David Leddick', NULL, NULL, NULL, NULL, 1, '9780312208981', '978-0-312-20898-1'),
	(97, 'book', 'Out of the past: gay and lesbian history from 1869 to the present', 'Neil Miller', NULL, NULL, NULL, NULL, 1, '9781555838706', '978-1-55583-870-6'),
	(98, 'book', 'Gay roots: twenty years of Gay sunshine: an anthology of gay history, sex, politics, and culture', 'edited by Winston Leyland', NULL, NULL, NULL, NULL, 1, '9780940567153', '978-0-940567-15-3'),
	(99, 'book', 'We are everywhere: a historical sourcebook of gay and lesbian politics', 'edited by Mark Blasius and Shane Phelan', NULL, NULL, NULL, NULL, 1, '9780415908597', '978-0-415-90859-7'),
	(100, 'book', 'To Believe in Women: What Lesbians Have Done For America - A History', 'Lillian Faderman, ', 'This landmark work of lesbian history focuses on how certain late-nineteenth-century and twentieth-century women whose lives can be described as lesbian were in the forefront of the battle to secure the rights and privileges that large numbers of Americans enjoy today. Lillian Faderman persuasively argues that their lesbianism may in fact have facilitated their accomplishments. A book of impeccable research and compelling readability, TO BELIEVE IN WOMEN will be a source of enlightenment for all, and for many a singular source of pride.', NULL, NULL, NULL, 1, '9780618056972', '978-0-618-05697-2'),
	(101, 'book', 'Queering the color line: race and the invention of homosexuality in American culture', 'Siobhan B. Somerville', NULL, NULL, NULL, NULL, 1, '9780822324430', '978-0-8223-2443-0'),
	(102, 'book', 'Down Low Secrets : A Story about Black Men Who Have Sex with other Men', 'Kevin Ames, ', 'Trent Matthews is attractive, educated and he is engaged to the beautiful, Lisa Cooper. To the world Trent is a heterosexual man. But in truth, Trent has secret sexual encounters with men. Lisa Cooper is engaged to Trent Matthews. How will she react when she learns that he has sex with men? Read their story and find out.', NULL, NULL, NULL, 1, '9780595323487', '978-0-595-32348-7'),
	(103, 'book', 'Is it a choice?: answers to the most frequently asked questions about gay and lesbian people', 'Eric Marcus', NULL, NULL, NULL, NULL, 1, '9780060832803', '978-0-06-083280-3'),
	(104, 'book', 'LGBT Studies and Queer Theory: New Conflicts, Collaborations, and Contested Terrain (Journal of Homosexuality)', 'Karen E. Lovaas (Editor), John P. Elia (Editor), Gust A. Yep (Editor)', 'Find out how the tension between LGBT studies and queer theory exists in the classroom, politics, communities, and relationships LGBT Studies and Queer Theory: New Conflicts, Collaborations, and Contested Terrain examines the similarities and differences between LGBT studies and queer theory and the uneasy relationship between the two in the academic world. This unique book meets the challenge that queer theory presents to the study and politics of gay and lesbian studies with a collection of essays from leading academics who represent a variety of disciplines. These original pieces place queer theory in social and historical contexts, exploring the implications for social psychology, religious studies, communications, sociology, philosophy, film studies, and women\'s studies. The book\'s contributors address queer theory\'s connections to a wide range of issues, including the development of capitalism, the evolution of the gay and lesbian movement, and the study of bisexuality and gender. Many scholars working in gay and lesbian studies still question the intellectual and political value of queer theory. As a result, queer theory has often been concentrated in the humanities, while gay and lesbian studies are concentrated in the social sciences and history. But this has begun to change in the past 10-15 years, as documented by the 12 essays presented in LGBT Studies and Queer Theory: New Conflicts, Collaborations, and Contested Terrain. LGBT Studies and Queer Theory: New Conflicts, Collaborations, and Contested Terrain includes: historical notes on LGBT studies and queer theory some continuing tensions between LGBT studies and queer theory doubts about whether queer theory can lead to social change an analysis of the current state of "proto-fields" of LGBT studies and queer studies in religion concerns that queer theory\'s "erasure of identity" feeds into late capitalism an analysis of variability in social psychologists\' studies of anti-homosexual prejudice an exploration of the commodification of queer identities in independent cinema how and why the category of bisexuality has been marginalized a historical review and assessment of recent bisexual theory a case study of Provincetown, Massachusetts an investigation of the interarticulation of race/ethnicity and gender a case study of the struggle to introduce LGBT studies in the curriculum at West Chester University and much more! LGBT Studies and Queer Theory: New Conflicts, Collaborations, and Contested Terrain is an essential read for researchers, academics, and practitioners involved in exploring multifaceted aspects of LGBT Studies and Queer Theory and their points of convergence and divergence.', NULL, NULL, NULL, 1, '9781560233176', '978-1-56023-317-6'),
	(105, 'book', 'Fundamentals of human sexuality', 'Herant A. Katchadourian, Donald T. Lunde', NULL, NULL, NULL, NULL, 1, '9780030429415', '978-0-03-042941-5'),
	(106, 'book', 'Readings in human sexuality: contemporary perspectives', 'Chad Gordon and Gayle Johnson, editors', NULL, NULL, NULL, NULL, 1, '9780060423995', '978-0-06-042399-5'),
	(107, 'book', 'Two-spirit people: Native American gender identity, sexuality, and spirituality', 'edited by Sue-Ellen Jacobs, Wesley Thomas, and Sabine Lang', NULL, NULL, NULL, NULL, 1, '9780252066450', '978-0-252-06645-0'),
	(108, 'book', 'Ceremonies: Prose and Poetry', 'Essex Hemphill, Charles Nero, ', 'Ceremonies offers provocative commentary on highly charged topics such as Robert Mapplethorpe\'s photographs of African-American men, feminism among men, and AIDS in the black community.', NULL, NULL, NULL, 1, '9781573441018', '978-1-57344-101-8'),
	(109, 'book', 'The Gay Metropolis: The Landmark History of Gay Life in America', 'Charles Kaiser, ', 'Charles Kaiser\'s The Gay Metropolis: 1940-1996, a history of gay life centered in New York, is packed with tales of writers and literature. Kaiser provides a kaleidoscope of details and stories that create a vision of how gay people lived, and illuminates a culture that had enormous influence on both New York and American society. Kaiser writes about such luminaries as Gore Vidal, Edward Albee, Truman Capote, and James Baldwin, but the real drive of The Gay Metropolis is how gay art and writings transformed the lives of everyday gay people. By the end of the book it is clear that gay artistic influence has transformed the American metropolis for both heterosexuals and homosexuals.', NULL, NULL, NULL, 1, '9780802143174', '978-0-8021-4317-4'),
	(110, 'book', 'Spirited: affirming the soul and Black gay/lesbian identity', 'edited by G. Winston James, Lisa C. Moore', NULL, NULL, NULL, NULL, 1, '9780965665933', '978-0-9656659-3-3'),
	(111, 'book', 'The Leading edge: an anthology of lesbian sexual fiction', 'edited by Lady Winston; introduction by Pat Califia', NULL, NULL, NULL, NULL, 1, '9780917597091', '978-0-917597-09-1'),
	(112, 'book', 'Health care for lesbians and gay men: confronting homophobia and heterosexism', 'K. Jean Peterson, editor', NULL, NULL, NULL, NULL, 1, '9781560230793', '978-1-56023-079-3'),
	(113, 'book', 'The zenith of desire: contemporary lesbian poems about sex', 'edited by Gerry Gomez Pearlberg', NULL, NULL, NULL, NULL, 1, '9780517702819', '978-0-517-70281-9'),
	(114, 'book', 'The Ethical Slut: A Guide to Infinite Sexual Possibilities', 'Dossie Easton, Catherine A. Liszt', NULL, NULL, NULL, NULL, 1, '9781890159016', '978-1-890159-01-6'),
	(115, 'book', 'The intimate connection: male sexuality, masculine spirituality', 'James B. Nelson', NULL, NULL, NULL, NULL, 1, '9780664240653', '978-0-664-24065-3'),
	(116, 'book', 'Man Enough To Be Woman: The Autobiography of Jayne County', 'Jane County, ', 'Wayne/Jayne County\'s weird fierce autobiography', NULL, NULL, NULL, 1, '9781852423384', '978-1-85242-338-4'),
	(117, 'book', 'Read My Lips: Sexual Subversion and the End of Gender', 'Riki Wilchins, ', 'Over the course of the past decade transgender politics have become the cutting edge of sexual liberation. While the sexual and political freedom of homosexuals has yet to be fully secured, questions of who is sleeping with whom pale in the face of the battle by transgender activists to dismantle the idea of what it means to be a man or a woman. Riki Anne Wilchins\'s Read My Lips is a passionate, witty, and extraordinarily intelligent look at how society not only creates men and women--ignoring the fluidity of maleness and femaleness in most people--but also explains how those categories generate crisis for most individuals. It is impossible to read Wilchins\'s ideas and not be provoked in fundamental and mysterious ways.', NULL, NULL, NULL, 1, '9781563410901', '978-1-56341-090-1'),
	(118, 'book', 'AIDS in America', 'Susan Hunter, ', 'With more than one million people currently infected and half a million already dead, the U.S. ranks among the top ten most severe sites of the AIDS epidemic in the world. Americans need to know more about the current state of the epidemic so we can protect ourselves and demand that the government act responsibly to reduce the danger of HIV in this country. Hunter exposes the ways in which the U.S. shamefully resembles a developing country, and the many fronts on which the government has failed to control the spread of the disease. In this startling book, she also shows what we must do to change the future of AIDS.', NULL, NULL, NULL, 1, '9781403971999', '978-1-4039-7199-9'),
	(119, 'book', 'GLBTQ: The Survival Guide for Queer and Questioning Teens', 'Kelly Huegel, ', 'Are you queer or questioning? If you are, this book is for you. Do you know someone who might be queer or questioning? If so, this book is for you, too. Or are you someone who just wants to learn more about what it\'s like to be queer or questioning? This book is a great place to begin. Discovering that you, or someone you love, might be GLBTQ is a revelation. Accepting it is a process. One thing that can help that process is information. This book can\'t answer all of your questions or counter all of the misinformation, misconceptions, myths, half-truths, and outright lies you might have heard about being GLBTQ, but it\'s a start.', NULL, NULL, NULL, 1, '9781575421261', '978-1-57542-126-1'),
	(120, 'book', 'The Fenway guide to lesbian, gay, bisexual, and transgender health', '[editors], Harvey J. Makadon... [et al.]', NULL, NULL, NULL, NULL, 1, '9781930513952', '978-1-930513-95-2'),
	(121, 'book', 'Empathy', 'Sarah Schulman', NULL, NULL, NULL, NULL, 1, '9780525935216', '978-0-525-93521-6'),
	(122, 'book', 'The joy of gay sex: an intimate guide for gay men to the pleasures of a gay lifestyle', 'Charles Silverstein & Edmund White; illustrated by Michael Leonard, Ian Beck & Julian Graddon', NULL, NULL, NULL, NULL, 1, '9780517531587', '978-0-517-53158-7'),
	(123, 'book', 'On the road to same-sex marriage: a supportive guide to psychological, political, and legal issues', 'Robert P. Cabaj, David W. Purcell, editors', NULL, NULL, NULL, NULL, 1, '9780787909628', '978-0-7879-0962-8'),
	(124, 'book', 'The invisible cure: Africa, the West, and the fight against AIDS', 'Helen Epstein', NULL, NULL, NULL, NULL, 1, '9780374281526', '978-0-374-28152-6'),
	(125, 'book', 'The wombat strategy: a Kylie Kendall mystery', 'by Claire McNab', 'Running a pub in the outback town of Woliegudgerie doesn\'t offer much fun or future for knockabout Aussie dyke Kylie Kendall, so when the father she barely knew dies and leaves her 51% of his Los Angeles-based private-eye agency, it\'s bright lights, big city for America bound Kylie. Decidely peeved is her father\'s former business partner, the beautiful, enigmatic Ariana Creeling, who wants to buy Kylie out and gives her a chilly reception.', NULL, NULL, NULL, 1, '9781555838362', '978-1-55583-836-2'),
	(126, 'book', 'Written on the body', 'Jeanette Winterson', NULL, NULL, NULL, NULL, 1, '9780679744474', '978-0-679-74447-4'),
	(127, 'book', 'Bi Any Other Name: Bisexual People Speak Out', 'Loraine Hutchins, Lani Kaahumanu, ', NULL, NULL, NULL, NULL, 1, '9781555831745', '978-1-55583-174-5'),
	(128, 'book', 'Major conflict: one gay man\'s life in the Don\'t-Ask-Don\'t-Tell military', 'Jeffrey McGowan', 'McGowan enlisted in the army in the late 1980s with a secret: he was gay. In the don\'t-ask-don\'t-tell world of the Clinton-era army, being gay meant automatic expulsion, so he hid his sexual identity and continued to serve with distinction for ten years. He commanded U.S. troops in the Persian Gulf, eventually rising to the rank of major, but ultimately realized that the army held no future for gay men--even closeted ones.', NULL, NULL, NULL, 1, '9780767918992', '978-0-7679-1899-2'),
	(129, 'book', 'All She Wanted', 'Aphrodite Jones, ', NULL, NULL, NULL, NULL, 1, '9780671023881', '978-0-671-02388-1'),
	(130, 'book', 'Hello Cruel World: 101 Alternatives to Suicide for Teens, Freaks and Other Outlaws', 'Kate Bornstein, Sara Quin (Foreword)', 'Celebrated transsexual trailblazer Kate Bornstein has, with more humor and spunk than any other, ushered us into a world of limitless possibility through a daring re-envisionment of the gender system as we know it.Here, Kate bravely and wittily shares personal and unorthodox methods of survival for navigating an often cruel world. A one-of-a-kind guide to staying alive outside the box, Hello, Cruel World is a much-needed unconventional approach to teenage suicide prevention for marginalized youth who want to stay on the edge, but alive.Hello, Cruel World features a catalog of 101 Alternatives to Suicide that range from the playful (Moisturize), to the irreverent (Disbelieve the Binary), to the highly controversial (Get Laid. Please). Designed to encourage readers to give themselves permission to unleash their hearts\' harmless desires, the book has only one directive: "Don\'t be mean." It is this guiding principle that brings its reader on a self-validating journey, which forges wholly new paths toward a resounding decision to choose life.Tenderly intimate and unapologetically edgy, Kate is the radical role model, the affectionate best friend, and the guiding mentor all in one kind and spirited package.A celebrated pioneer for the LGBTQI community, transsexual author and performance artist, Kate Bornstein is the author of the wildly successful books My Gender Workbook and Gender Outlaw: On Men, Women and The Rest of Us.', NULL, NULL, NULL, 1, '9781583227206', '978-1-58322-720-6'),
	(131, 'book', 'Bhagavad-gītā as it is', 'by his divine grace, A. C. Bhaktivedanta Swami Prabhupāda', NULL, NULL, NULL, NULL, 1, '9780892131341', '978-0-89213-134-1'),
	(132, 'book', 'The ideal gay man: the story of Der Kreis', 'Hubert Kennedy', NULL, NULL, NULL, NULL, 1, '9781560231264', '978-1-56023-126-4'),
	(133, 'book', 'Come out and win: organizing yourself, your community, and your world', 'Sue Hyde', NULL, NULL, NULL, NULL, 1, '9780807079720', '978-0-8070-7972-0'),
	(134, 'book', 'Growing up free: raising your child in the 80s', 'Letty Cottin Pogrebin', NULL, NULL, NULL, NULL, 1, '9780553013405', '978-0-553-01340-5'),
	(135, 'book', 'We do: a celebration of gay and lesbian marriage', 'foreword by Gavin Newsom; edited by Amy Rennert', NULL, NULL, NULL, NULL, 2, '9780811846127', '978-0-8118-4612-7'),
	(136, 'book', 'Lesbian and gay marriage: private commitments, public ceremonies', 'edited by Suzanne Sherman', NULL, NULL, NULL, NULL, 1, '9780877229759', '978-0-87722-975-9'),
	(137, 'book', 'Still acting gay: male homosexuality in modern drama', 'John M. Clum', NULL, NULL, NULL, NULL, 1, '9780312223847', '978-0-312-22384-7'),
	(138, 'book', 'Ecofeminist natures: race, gender, feminist theory, and political action', 'Noël Sturgeon', NULL, NULL, NULL, NULL, 1, '9780415912495', '978-0-415-91249-5'),
	(139, 'book', 'Lesbian images', 'Jane Rule', NULL, NULL, NULL, NULL, 1, '9780385042550', '978-0-385-04255-0'),
	(140, 'book', 'The Commitment: Love, Sex, Marriage, and My Family', 'Dan Savage, ', 'In a time when much of the country sees red whenever the subject of gay marriage comes up, Dan Savageoutspoken author of the column Savage Love makes it personal. Dan Savage\'s mother wants him to get married. His boyfriend, Terry, says no thanks because he doesn\'t want to act like a straight person. Their six-year-old son DJ says his two dads aren\'t allowed to get married, but that he\'d like to come to the reception and eat cake. Throw into the mix Dan\'s straight siblings, whose varied choices form a microcosm of how Americans are approaching marriage these days, and you get a rollicking family memoir that will have everyonegay or straight, right or left, single or marriedhowling with laughter and rethinking their notions of marriage and all it entails. BACKCOVER: Hilarious, heartfelt. Seattle Post-Intelligencer As funny as David Sedaris\'s essay collections, but bawdier and more thought-provoking. Publisher\'s Weekly (starred review) Most of all, a book about creating and appreciating family. Seattle Times I think America would be a better place if everyone on every side of the gay marriage debate would read this book. Ira Glass, host of the public radio show This American Life The strongest argument here, which [Savage] brilliantly plays down, is that family means everything to these people: married, not married, blended, gay, straight, whatever. The Washington Post', NULL, NULL, NULL, 1, '9780452287631', '978-0-452-28763-1'),
	(141, 'book', 'Dangerous families: queer writing on surviving', 'Mattilda, a. k. a. Matt Bernstein Sycamore, editor', NULL, NULL, NULL, NULL, 1, '9781560234227', '978-1-56023-422-7'),
	(142, 'book', 'The Velvet Rage: Overcoming the Pain of Growing Up Gay in a Straight Man\'s World', 'Alan Downs, ', 'A groundbreaking examination of the psychology of homosexuality, why it leads to shame over one\'s identity and how to overcome it The gay male world today is characterized by seductive beauty, artful creativity, flamboyant sexuality, and, encouragingly, unprecedented acceptability in society. Yet despite the progress of the recent past, gay men still find themselves asking, "Are we really better off?" The inevitable byproduct of growing up gay in a straight world continues to be the internalization of shame, a shame gay men may strive to obscure with a faade of beauty, creativity, or material success. Drawing on contemporary psychological research, the author\'s own journey to be free of anger and of shame, as well as the stories of many of his friends and clients, The Velvet Rage outlines the three distinct stages to emotional well-being for gay men. Offering profoundly beneficial strategies to stop the insidious cycle of avoidance and self-defeating behavior, The Velvet Rage is an empowering book that will influence the public discourse on gay culture, and positively change the lives of gay men who read it.', NULL, NULL, NULL, 1, '9780738210612', '978-0-7382-1061-2'),
	(143, 'book', 'Another mother tongue: gay words, gay worlds', 'Judy Grahn', NULL, NULL, NULL, NULL, 1, '9780807079119', '978-0-8070-7911-9'),
	(144, 'book', 'The kid: what happened after my boyfriend and I decided to go get pregnant: an adoption story', 'Dan Savage', NULL, NULL, NULL, NULL, 1, '9780452281769', '978-0-452-28176-9'),
	(145, 'book', 'The invention of sodomy in Christian theology', 'Mark D. Jordan', NULL, NULL, NULL, NULL, 1, '9780226410401', '978-0-226-41040-1'),
	(146, 'book', 'HIV and me: firsthand information for coping with HIV and AIDS', 'by Timothy Critzer', NULL, NULL, NULL, NULL, 1, '9780974538839', '978-0-9745388-3-9'),
	(147, 'book', 'If I grow up: talking with teens about AIDS, love, and staying alive', 'Scott Fried', NULL, NULL, NULL, NULL, 1, '9780965904605', '978-0-9659046-0-5'),
	(148, 'book', 'The Advocate Guide to Gay Men\'s Health and Wellness (Advocate Guide)', 'Frank, M.D. Spinelli, ', 'Focusing on prevention, lifestyle modification, and healthcare, Dr. Frank Spinelli has channeled his years of experience and research in order to create a comprehensive medical guide for gay men. Covering topics ranging from immunization, physical exams, depression, substance abuse, and andropause, New York City\'s "hottest gay doctor" has compiled his knowledge into an invaluable guide specifically created for gay men. Frank Spinelli, M.D., is a board certified internist in private practice in New York City in addition to serving as clinical director of HIV at Cabrini Medical Center. Dr. Spinelli has made frequent television appearances and is the gay health expert on Radio with a Twist, a national talk show. Dr. Spinelli currently lives in the Chelsea section of Manhattan.', NULL, NULL, NULL, 1, '9781593500405', '978-1-59350-040-5'),
	(149, 'book', 'Social Services for Senior Gay Men and Lesbians (Monograph Published Simultaneously As the Journal of Gay & Lesbian Social Services , Vol 2, No 1)', 'Jean K Quam, ', 'Social Services for Senior Gay Men and Lesbians is an important new reference that provides those in the helping professions with practical information on how to work with the older gay and lesbian population. Although older gays and lesbians are the same in many ways as their heterosexual counterparts, they have an extra “layer” of concerns that are unique to their sexual orientation, including “coming out” to family and medical professionals, fear of discrimination, isolation, and loneliness. This new book helps social service providers address these and other concerns of the aging homosexual.Social Services for Senior Gay Men and Lesbians examines the history of homosexuality and how practitioners have developed ways to better serve this population. The book features case studies of topics that face practitioners and their older gay clients, including: housing needs of older gay and lesbian adults group therapy for older gay males long-term care dilemmas for older lesbians counseling an older gay male who is “coming out” staff development for non-gay social service providers historical review of gay and lesbian issuesBecause so little information exists in these and other areas, Social Services for Older Gay Men and Lesbians is an excellent resource for social workers, psychologists, nurses, counselors, and physicians.', NULL, NULL, NULL, 1, '9781560230847', '978-1-56023-084-7'),
	(150, 'book', 'Families like mine: children of gay parents tell it like it is', 'Abigail Garner', NULL, NULL, NULL, NULL, 1, '9780060527570', '978-0-06-052757-0'),
	(151, 'book', 'The bear book: readings in the history and evolution of a gay male subculture', 'Les K. Wright, editor', NULL, NULL, NULL, NULL, 1, '9781560238904', '978-1-56023-890-4'),
	(152, 'book', 'The Lesbian and gay studies reader', 'edited by Henry Abelove, Michèle Aina Barale, David M. Halperin', NULL, NULL, NULL, NULL, 1, '9780415905190', '978-0-415-90519-0'),
	(153, 'book', 'Lesbian, gay, bisexual, and transgender health issues: selections from the American journal of public health', 'introduction by Anthony J. Silvestre', NULL, NULL, NULL, NULL, 1, '9780875530147', '978-0-87553-014-7'),
	(154, 'book', 'Queer Facts: The Greatest Gay and Lesbian Trivia Book Ever', 'Michelle Baker, Stephen Tropiano, ', 'Homosexuality has hit the mainstream. Civil partnerships and gay marriage continue to grab headlines, while TV successes like Will & Grace and Queer Eye for the Straight Guy point to increased acceptance and appreciation. In this lively, pocket-sized guide - complete with illustrations — Michelle Baker and Stephen Tropiano have gathered an array of trivia and anecdotes drawn from every corner of queer culture. From literature to heritage and history, from politics to the always fascinating realm of popular culture, readers will find their interest piqued, and their conversations enlivened, by fun (and often funny) facts and figures. Who was the first openly gay politician elected to office? The first lesbian soap character? What\'s the most successful gay novel of all time? And what is the story behind Abba\'s "Dancing Queen"? Queer Facts answers these questions and more — with inimitable style, of course. The book features a foreword by Graham Norton, one of the most popular entertainers on British television and known in the United States for his weekly show on Comedy Central.', NULL, NULL, NULL, 1, '9781860746963', '978-1-86074-696-3'),
	(155, 'book', 'The big gay book: a man\'s survival guide for the 90\'s', 'edited by John Preston', NULL, NULL, NULL, NULL, 1, '9780452266216', '978-0-452-26621-6'),
	(156, 'book', 'The gay, lesbian, and bisexual students\' guide to colleges, universities, and graduate schools', 'Jan-Mitchell Sherrill and Craig A. Hardesty', NULL, NULL, NULL, NULL, 2, '9780814779859', '978-0-8147-7985-9'),
	(157, 'book', 'Bisexual Resource Guide', 'Robyn Ochs (Editor)', NULL, NULL, NULL, NULL, 1, '9780965388139', '978-0-9653881-3-9'),
	(158, 'book', 'Gay & Lesbian Online 4th Edition', 'Jeff Dawson (Editor)', 'Let us all drop to our knees and give thanks to Jeff Dawson, a brave man of apparently inexhaustible energy who has put together a guide to over 3,700 Web sites of all things queer on the Internet. The fourth edition of this deservedly bestselling series, Gay & Lesbian Online, is both "practical" and "an invaluable tool," as the publishers tell us, but it\'s also amusing and unexpectedly thorough, including alarming sites on the radical right and their plans to eliminate fun from America. Even if you find the Internet a tad overwhelming--much as my Aunt Bess did the first time she encountered a bidet in a Chicago hotel room, asking, "What in heaven\'s name do I do with it?"--Dawson\'s guide can lead you gently but firmly into the new world. Face it--some of us are still celebrating the invention of Wite-Out. It is time, as they say in every movie of the week, to let the healing begin. What, for example, is the latest part to pierce? What do lesbian gardeners recommend for planting during the California flood season? The mysteries are unfolding. --Jack Connolly', NULL, NULL, NULL, 1, '9781555835811', '978-1-55583-581-1'),
	(159, 'book', '50 ways to fight censorship: and important facts to know about the censors', 'by Dave Marsh and friends', NULL, NULL, NULL, NULL, 1, '9781560250111', '978-1-56025-011-1'),
	(160, 'book', 'Planning a Life in Medicine : Discover If a Medical Career is Right for You and Learn How to Make It Happen (Career Guides)', 'John Smart, Stephen L., Jr. Nelson, Julie Doherty, Cris Dornaus (Illustrator)', 'More than a comprehensive plan for getting into medical school, this is a handbook that will help you cultivate the skills and habits-- such as compartmentalizing knowledge and improving concentration-- that wll serve you well throughout your education and medical career.', NULL, NULL, NULL, 1, '9780375764608', '978-0-375-76460-8'),
	(161, 'book', 'Light Before Day', 'Christopher Rice, ', 'In West Hollywood, journalist Adam Murphy is abruptly fired while chasing a career-making story. A Marine pilot is killed when his helicopter spirals into the Pacific Ocean -- and Adam suspects the death was not accidental. Battling his own demons in a city of temptations, Adam pursues the truth alongside his new boss, a famous curmudgeonly mystery writer, and discovers more than he planned about his recently estranged lover, a string of murders of other young men -- and a conspiracy so extraordinary that it threatens his sanity and his life. With a talent for creating suspense-tinged fiction that is "chillingly perverse" (USA Today) and "vivid and intense" (The Boston Globe), Christopher Rice delivers a dark thriller of revenge and sexual obsession.', NULL, NULL, NULL, 1, '9780743470407', '978-0-7434-7040-7'),
	(162, 'book', 'Sudden Death', 'RITA MAE BROWN, ', NULL, NULL, NULL, NULL, 1, '9780553269307', '978-0-553-26930-7'),
	(163, 'book', 'Under Suspicion', 'Claire McNab, ', NULL, NULL, NULL, NULL, 1, '9781562802615', '978-1-56280-261-5'),
	(164, 'book', 'Recognition Factor: A Denise Cleever Thriller (Denise Cleever Thrillers)', 'Claire McNab, ', 'Australian undercover agent Denise Cleever is the only member of the worldwide intelligence community who has seen the never-photographed international terrorist Red Wolf face-to-face . . . and lived. Now the fear is that Red Wolf is in the United States to coordinate a series of terrorist attacks -- and Denise is the only person who can recognize and stop him.', NULL, NULL, NULL, 1, '9781931513241', '978-1-931513-24-1'),
	(165, 'book', 'Body Guard: A Detective Inspector Carol Ashton Mystery (Mcnab, Claire. Detective Inspector Carol Ashton Mystery, 6.)', 'Claire McNab, ', NULL, NULL, NULL, NULL, 1, '9781562800734', '978-1-56280-073-4'),
	(166, 'book', 'Common Murder: The Second Lindsay Gordon Mystery', 'Val McDermid, ', NULL, NULL, NULL, NULL, 1, '9781883523084', '978-1-883523-08-4'),
	(167, 'book', 'Successful writing at work', 'Philip C. Kolin', NULL, NULL, NULL, NULL, 1, '9780618593705', '978-0-618-59370-5'),
	(168, 'book', 'Density of souls', 'Christopher Rice', NULL, NULL, NULL, NULL, 1, '9780786886463', '978-0-7868-8646-3'),
	(169, 'book', 'Southbound the sequel to Faultline', 'by Sheila Ortiz Taylor', NULL, NULL, NULL, NULL, 1, '9780941483780', '978-0-941483-78-0'),
	(170, 'book', 'Murder Undercover: A Denise Cleever Thriller', 'Claire McNab, ', NULL, NULL, NULL, NULL, 1, '9781562802592', '978-1-56280-259-2'),
	(171, 'book', 'Bingo', 'Rita Mae Brown', NULL, NULL, NULL, NULL, 1, '9780553282207', '978-0-553-28220-7'),
	(172, 'book', 'The Snow Garden', 'Christopher Rice, ', 'In the darkening chill of winter, three Atherton University freshmen are being mysteriously drawn together by fate, and a compulsion they cannot comprehend. Though they come from vastly differing backgrounds, the college holds the promise of a better life for each of them -- and an opportunity to break away from the anguish and desolation of their former lives. But the past does not die so easily, and the intricate webs of deceit that they have spun to protect themselves are slowly twisted into shackles that chain them to their doom. Snowbound and trapped on the campus, the friends find themselves unwilling pawns in the machinations of a malevolent force that has taken hold of Atherton -- and all their oldest fears, veiled passions, and secret nightmares are about to come to life in...The Snow Garden.', NULL, NULL, NULL, 1, '9780743470384', '978-0-7434-7038-4'),
	(173, 'book', 'Understanding and managing organizational behavior', 'Jennifer M. George, Gareth R. Jones', 'The authors provide an authoritative and practical introduction to organisational behaviour. Coverage of topics and issues combined with a wealth of learning tools help students experience organisational behaviour and guide them to becoming better managers.', NULL, NULL, NULL, 1, '9780132394574', '978-0-13-239457-4'),
	(174, 'book', 'The Quotable Queer : Fabulous Wit and Wisdom from the Gays, the Straight, and Everybody In-Between', 'Minnie Van Pileup, ', 'Gay culture has always been celebrated in Hollywood and artistic circles, but never before has it hit the mainstream the way it has in the last five years -- from TV shows to politics to fashion, the gays are stealing center stage! This book celebrates the unique wit and wisdom of famous gay people, from Rock Hudson to Liberace to Ellen DeGeneres, as well as the wit and wisdom of their friends and the idiocy of their foes. Packaged in a cheap-and-cheerful format, the book is the perfect impulse buy for gays and the straights who love them. Quotable Queers (and their straight counterparts) include: Queer Eyeís Fab Five Rosie OíDonnell Oscar Wilde Columnist Dan Savage k.d. lang Tom Cruise Whitney Houston and more!', NULL, NULL, NULL, 1, '9781592331208', '978-1-59233-120-8'),
	(175, 'book', 'Hollywoodn\'t: a queer postcard book', 'Laurence Jaugey-Paget', NULL, NULL, NULL, NULL, 1, '9780044409496', '978-0-04-440949-6'),
	(176, 'book', 'You know you\'re gay when...: those unforgettable moments that make us who we are', 'Joseph Cohen', NULL, NULL, NULL, NULL, 1, '9780809233205', '978-0-8092-3320-5'),
	(177, 'book', 'You\'re Different and That\'s Super', 'Carson Kressley, Jared Lee (Illustrator)', 'The spring that Trumpet was born, there were so many new foals in the pasture that almost no one notices there was a sassy little colt who didn\'t belong to any of the mares....Fab Five phenom Carson Kressley brings us the story of a one-of-a-kind pony who learns that it\'s our differences that make us super. Whimsical black-and-white illustrations from renowned equine artist Jared Lee corral both humor and charm in this warm tale of a unicorn struggling to find his identity and place in the world.', NULL, NULL, NULL, 1, '9781416900702', '978-1-4169-0070-2'),
	(178, 'book', 'Invasion of the Dykes to Watch Out For (Dykes to Watch Out for)', 'Alison Bechdel, ', 'Book 11 in the Dykes to Watch Out For series finds Alison Bechdel\'s beloved cast of characters discovering that nothing but change is constant in our multihued terror alert system world. Mo is working her way through library school by shelving bestsellers at Bounders Books and Muzak, Sparrow and Stuart face parenthood with a mixture of ambivalence and zeal, Clarice and Toni clash over the gay marriage debate while their 10-year-old son Raffi jacks cars and slaps hos on his best friend\'s computer, Sydney\'s mammogram yields very bad news, Ginger\'s love life is finally looking up, and is a Hello Kitty thong really the best gift Lois can give 13-year-old Jonas when s/he becomes Janis? Alison Bechdel is legendary for her ability to examine global politics through the prism of dinner conversations, and this book is no exception. Sydney\'s doctor insists in likening her malignant cells to terrorists, Stuart\'s home improvement project develops disturbing parallels to Operation Iraqi Freedom, and Ginger\'s best student turns out to be her most conservative, resulting in an ideology-busting clash of red- and blue-state values. Alison Bechdel has been writing and illustrating Dykes to Watch Out For since 1982. Her books have won multiple awards, and she was most recently a finalist for the Ferro-Grumley Award and two Eisner awards. She lives in Vermont and is working on a graphic memoir about her queer father.', NULL, NULL, NULL, 1, '9781555838331', '978-1-55583-833-1'),
	(179, 'book', 'Love Makes A Family: Living in Lesbian and Gay Families (Family Diversity Projects)', 'Family Diversity Projects, Inc.', 'A family is a bunch of people, or not so many, who love each other," explains seven year old Liza, who lives with her two moms. Liza\'s family is one among twenty families represented in this exhibit of diverse families with gay and lesbian dads or moms, grandparents, and/or young adults. In this exhibit, photographs by Gigi Kaeser are accompanied by text edited from interviews with each family member. Together, the words and the images in <b><i>Love Makes A Family</i></b> show in a visible and positive way, the existence, the love, and the power of these families.\r\n\r\nThe silence about lesbian and gay lives that pervades our schools and our society is not only the companion of abuse, but the foundation. In particular, the consequences of homophobia for lesbian, gay, and bisexual youth are profound. The Massachusetts Governor\'s Commission on Gay and Lesbian Youth concludes, "Reluctance on the part of the vast majority of schools is to encourage discussions of lesbian and gay issues perpetuates the isolation and loneliness of lesbian and gay teens that so often drives them to attempt suicide." In order to help create safe schools for gay and lesbian youth and children of gays and lesbians, Family Diversity Projects distributes a special version of <b><i>Love Makes A Family</i></b> designed especially for elementary school students (K-6) with age appropriate text. The regular exhibit text is suitable for older students and adults.\r\n\r\nAt the most basic level, <b><i>Love Makes A Family</i></b> combats homophobia by breaking silence and making the invisible visible. By educating people of all ages &mdash; beginning in early childhood &mdash; to celebrate and appreciate diversity, this exhibit contributes to the progress of dismantling the destructive power of prejudice and intolerance, thereby making the world a safer place for all families.', NULL, NULL, NULL, 2, '', ''),
	(180, 'book', 'Of Many Colors: Portraits of Multiracial Families (Family Diversity Projects)', 'Family Diversity Projects, Inc.', 'This moving exhibit offers a powerful vision of the growing diversity of the American family. These families clearly have much to teach everyone about the most intimate form of integration: familial love.\r\n\r\n<b><i>Of Many Colors</i></b> includes photos by Gigi Kaeser and interviews with twenty diverse American families formed through interracial relationships or transracial adoption. In a world where race is considered by many to be a formidable barrier between people, the families in this exhibit are celebrated as twentieth century pioneers willing to risk disapproval and misunderstanding to find richness and value in diversity.\r\n\r\n<i>"When you are in a multiracial family, you start seeing beauty in all races. I feel like somebody took a blindfold out of my eyes and I started appreciating people for who they are, not because of what color they are,"</i> says Eileen Madison. Her twelve year old son, José, agrees. <i>"I\'m very lucky. I love my Puerto Rican culture, and I have relatives who tell me about their lives as African-Americans. I\'m learning a lot about another race besides my own."</i>\r\n\r\nPhotographs from this exhibit have been featured in a cover story in the <i>Boston Globe Sunday Magazine</i> and in <i>Teaching Tolerance Magazine</i>. The winner of a Multicultural Recognition Award from the Regional Laboratory for Educational Improvement, <b><i>Of Many Colors</i></b> has been touring since 1993.', NULL, NULL, NULL, 1, '', ''),
	(181, 'book', 'State of the States 2004: A Policy Analysis of Lesbian, Gay, Bisexual and Transgender (LGBT) Safer School Issues', 'GLSEN', 'The State of the States 2004 report summarizes the laws affecting students, particularly, lesbian, gay, bisexual and transgender (LGBT) students in the 50 states and the District of Columbia. The Gay, Lesbian, and Straight Education Network (GLSEN) prepared this State of the States 2004 report to continue the comprehensive collection of data and information related to education issues affecting lesbian, gay, bisexual, and transgender students, teachers, and staff.\r\n\r\nResults from the 2003 GLSEN National School Climate Survey indicate a great need to enact and fully implement statewide laws in order to effectively confront harassment and discrimination in schools. More than 84% of lesbian, gay, bisexual and transgender (LGBT) students report being verbally harassed because of their sexual orientation and nearly 40% report being physically harassed. Additionally, more than 90% of LGBT students regularly hear anti-LGBT comments at school. Transgender youth are disproportionately likely to face harassment. In the survey, 55% of transgender students reported being physically harassed because of their gender, gender expression, or sexual orientation. And while all of this is occurring nearly 85% of LGBT students report that faculty or staff never intervened or intervened only some of the time when present and homophobic remarks were made.\r\n\r\nFor the purposes of this report safe school laws are statewide anti-harassment and/or non-discrimination laws that are inclusive of the categories of sexual orientation and/or gender identity/expression and safe schools policies are those passed by a local educational agency (LEA) governing authority, generally a school board.', NULL, NULL, NULL, 2, '', ''),
	(182, 'book', 'You Can\'t Shave in a Minimart Bathroom', 'Shauna Marie O\'Toole, ', 'A more light-hearted look at what happens when you transition from your birth-sex to your true gender. And, no, you can\'t shave in a minimart bathroom - the soap isn\'t slippery enough!', NULL, NULL, NULL, 2, '9780557075553', '978-0-557-07555-3'),
	(183, 'book', 'Brother to Brother: New Writings by Black Gay Men', 'Essex Hemphill, ', NULL, NULL, NULL, NULL, 1, '9781555831462', '978-1-55583-146-2'),
	(184, 'book', 'Shirt of Flame: The Secret Gay Art of War', 'Ko Imani, ', NULL, NULL, NULL, NULL, 2, '9780974126500', '978-0-9741265-0-0'),
	(185, 'book', 'Biological exuberance: animal homosexuality and natural diversity', 'Bruce Bagemihl; illustrated by John Megahan', '"Drawing upon a rich body of zoological research spanning more than two centuries, Bruce Bagemihl shows that animals engage in all types of nonreproductive sexual behavior. Sexual and gender expression in the animal world displays exuberant variety, including same-sex courtship, pair-bonding, sex, and co-parenting - even instances of lifelong homosexual bonding in species that do not have lifelong heterosexual bonding."--BOOK JACKET.', NULL, NULL, NULL, 1, '9780312253776', '978-0-312-25377-6'),
	(186, 'book', 'The Naked Centurion', 'J.A. Kichi, ', '"A novel wherein the Gay Disciple learns the nature of his true Master."', NULL, NULL, NULL, 1, '9780974899206', '978-0-9748992-0-6'),
	(187, 'book', 'The other side of desire', 'by Paula Christian; with a special foreword by Joyce Bright', NULL, NULL, NULL, NULL, 1, '9780931328084', '978-0-931328-08-4'),
	(188, 'book', 'Cactus Love: A Collection of Short Stories', 'Lee Lynch, ', NULL, NULL, NULL, NULL, 1, '9781562800710', '978-1-56280-071-0'),
	(189, 'book', 'Lovers\' Choice', 'Birtha, ', NULL, NULL, NULL, NULL, 1, '9781878067418', '978-1-878067-41-8'),
	(190, 'book', 'Unspeakable ShaXXXspeares, Revised Edition: Queer Theory and American Kiddie Culture', 'Richard Burt, ', 'Unspeakable ShaXXXspeares is a savvy look at the wide range of adaptations, spin-offs, and citations of Shakespeare\'s plays in 1990s popular culture. What does it say about our culture when Shakespearean references turn up in television episodes of The Brady Bunch and Gilligan\'s Island, films such as In and Out and My Own Private Idaho, and hardcore porn adaptations of Hamlet and Romeo and Juliet? Burt reads the reception of these often quite bad replays in relation to contemporary youth culture and the "queering" of Shakespeare.', NULL, NULL, NULL, 1, '9780312226855', '978-0-312-22685-5'),
	(191, 'book', 'Gay haiku', 'Joel Derfner', NULL, NULL, NULL, NULL, 1, '9780767919845', '978-0-7679-1984-5'),
	(192, 'book', 'Conduct unbecoming: lesbians and gays in the U. S. military: Vietnam to the Persian Gulf', 'Randy Shilts', NULL, NULL, NULL, NULL, 1, '9780312092610', '978-0-312-09261-0'),
	(193, 'book', 'Twice blessed: on being lesbian or gay, and Jewish', 'edited by Christie Balka and Andy Rose', NULL, NULL, NULL, NULL, 1, '9780807079096', '978-0-8070-7909-6'),
	(194, 'book', 'Girls Will Be Boys Will Be Girls: A Coloring Book', 'Jacinta Bunnell, Irit Reinheimer, ', 'The parallel-universe version of the "Dick and Jane" coloring book, Girls Will Be Boys Will Be Girls is a comic deconstruction of traditional gender roles. This playful, provocative coloring book contains 32 original illustrations guaranteed to give even the most seasoned gender bender a good laugh. Captions like "Calvin, baking is fun and all, but we can make a rad drum set out of these pots and bowls" and "Don\'t let gender box you in" offer lighthearted, fun ways to rethink gender for both children and adults. Girls Will Be Boys Will Be Girls makes it OK to step outside the lines.', NULL, NULL, NULL, 1, '9781932360622', '978-1-932360-62-2'),
	(195, 'book', 'Everything you have is mine', 'Sandra Scoppettone', NULL, NULL, NULL, NULL, 1, '9780345376824', '978-0-345-37682-4'),
	(196, 'book', 'Death Understood', 'Claire McNab, ', NULL, NULL, NULL, NULL, 1, '9781562802646', '978-1-56280-264-6'),
	(197, 'book', 'Serious pleasure: lesbian erotic stories and poetry', 'edited by the Sheba Collective', NULL, NULL, NULL, NULL, 1, '9780939416455', '978-0-939416-45-5'),
	(198, 'book', 'From Balzac to Zola: 19th Century Short Stories (Bristol French Texts)', 'Richard Hobbs, ', 'French text with English introduction and notes.', NULL, NULL, NULL, 1, '9781853993312', '978-1-85399-331-2'),
	(199, 'book', 'Little Lavender Book: On the Love That Once Dared Not Speak its Name (Little Red Books)', 'Saeko Usukawa (Editor)', 'Historically revealing quotations tracing the evolution of gay and lesbian desire amid the myriad struggles for acceptance. I am the love that dare not speak its name. —Lord Alfred Douglas, Oscar Wilde\'s lover', NULL, NULL, NULL, 1, '9781551520049', '978-1-55152-004-9'),
	(200, 'book', 'Strapped for Cash : A History of American Hustler Culture', 'Mack Friedman, ', 'An inside view of American hustler culture from the 1600s to today. Male and transgendered hustlers have been a mostly -invisible part of American life for more than 400 years. Their invisibility leaves these men unprotected, victimized and at risk for sexually transmitted diseases, assault and murder. Strapped for Cash is a critical examination of sex work that serves as both a cultural analysis and a guide for those who conduct HIV/AIDS and other interventions among male and transgendered sex workers. Mack Friedman brings to this work an unusual perspective informed by both his former life as a hustler and his current work as a case manager targeting prostituted youth. Drawn from many sources, including 50 oral histories from current and former male and transgendered sex workers, details of the work of anti-vice committees, academic research and uncovered materials from archives around the country, Strapped for Cash presents the first complete picture of the evolution of hustler culture. Friedman reaches back to European hustler history to find the influences that informed male-male sex trade in the New World and carries this research forward to examine media representations, the development of male pornography and the larger social implications of male sex work. Frank and unapologetic, Strapped for Cash is a powerfully provocative study of this marginalized world. Features more than 70 black-and-white photographs.Mack Friedman is an HIV testing and prevention supervisor for the Pittsburgh AIDS Task Force. Formerly a youth advocate for Women Hurt in Systems of Prostitution -Engaged in Revolt (WHISPER), and the former vice president of Youth Services of Pittsburgh, he currently serves on the boards of both the Southwestern Pennsylvania AIDS Planning Coalition and Standing Against Global Exploitation (SAGE). A native of Chicago and a 2001 graduate of the University of Minnesota, he currently lives in Pittsburgh, Pennsylvania.', NULL, NULL, NULL, 1, '9781555837310', '978-1-55583-731-0'),
	(201, 'book', 'Future Imperfect', 'Shauna O\'Toole, ', 'Many authors paint the future as bright and shiny. Not every story will have a happy ending. Not everyone will have a bright future. Indeed, for many, the future will be quite imperfect. From the author of "You Can\'t Shave in a Minimart Bathroom."', NULL, NULL, NULL, 2, '9780557931415', '978-0-557-93141-5'),
	(202, 'book', 'Gay roots: twenty years of Gay sunshine: an anthology of gay history, sex, politics, and culture', 'edited by Winston Leyland', NULL, NULL, NULL, NULL, 1, '9780940567139', '978-0-940567-13-9'),
	(203, 'book', 'Outbursts!: A Gay and Lesbian Erotic Thesaurus', 'A.D. Peterkin, ', 'Erotic slang words from Great Britain, Canada, the United States, Australia, and other English-speaking nations number well into the tens of thousands. But the history of terms used to describe the sexual activities of gays and lesbians have opposing sources: one, the discreet networks of gay men and lesbians who sought to come up with a new terminology for the pleasures of their secret lives; and the other, those who found gay sexuality repellent, and created phrases that denigrated and insulted its proponents. The result? A coded language, for better or worse, that celebrates sexuality in all its queerness.A. D. Peterkin shows how euphemism, camp humor, rhyme, acronym, and secret code have all been recruited imaginatively by gay men, lesbians, and bisexuals to name what was thought to be unnamable.A. D. Peterkin is a Toronto psychiatrist and journalist.', NULL, NULL, NULL, 1, '9781551521510', '978-1-55152-151-0'),
	(204, 'book', 'Lesbian Short Stories', 'Victoria M. St Christopher, ', 'Humor, mystery, romance and erotica make up these eight lesbian short stories. Be delighted by soft romance as a home-spun southern belle is pursued by a female college professor. Savor the sweetness of a semi-truck driver whose dead love life is brought back to life by a passer-by. A young woman\'s career does a turn-about because of a chance meeting with the woman of her dreams. The Olympic Trials will never be the same to you after reading about a softball player who makes more than just the team. Ever fantasize about the neighborhood dyke mechanic? This story makes one come alive!', NULL, NULL, NULL, 1, '9781930693814', '978-1-930693-81-4'),
	(205, 'book', 'Lesbian Therapists and Their Therapy: From Both Sides of the Couch', 'Ellen Cole, Esther D Rothblum, Nancy C Davis, ', 'This is an important anthology for therapists who want to enhance their sensitivity and effectiveness in working with lesbians. In Lesbian Therapists and Their Therapy, the authors describe how being a lesbian has affected their own therapy--as clients and as therapists--to enable other practicing lesbian therapists to work more effectively with lesbian clients.This guidebook contains personal stories from lesbian therapist\'s own therapy, told to help other therapists deal with lesbian clients. Lesbian Therapists and Their Therapy enlightens the reader about the special factors in making therapy with lesbian clients more successful. The contributors accomplish this by highlighting personal details from experiences they had during their own therapy. In an excellent position to critique others’therapy with lesbian clients, these lesbian therapists can help others identify and avoid the pitfalls of doing therapy with this population.The personal material contained in this collection shows, in a very powerful way, the good and not-so-good handling of the special issues that all lesbians face in today\'s society. Lesbian Therapists and Their Therapy is unique because it presents insight from personal stories of therapists that is not available elsewhere in book form. Among the topics and concerns covered are:therapists who are homophobicpitfalls of doing therapy with lesbian clientsboundary issues between client and therapistattempts to change sexual orientation through therapysexual abuse in therapyThis book will appeal to all therapists who deal with or would like to treat lesbian clients. Lesbian Therapists and Their Therapy should also appeal to graduate students studying psychology and counseling who desire to be well-prepared for treating this population. It is a helpful collection for persons not in the counseling field who would like to learn about the problems lesbians face in therapy due to their sexual orientation.', NULL, NULL, NULL, 1, '9781560230823', '978-1-56023-082-3'),
	(206, 'book', 'What the Bible Really Says About Homosexuality', 'Daniel A. Helminiak', NULL, NULL, NULL, NULL, 1, '9781886360099', '978-1-886360-09-9'),
	(207, 'book', 'Creation of Sex? Gender?', 'Jelena Poštić, Svetlana Ɖurković, and Amir Hodžić', 'Welcome!\r\n\r\nEverything that you live your life by, the values that you hold, these are all theories. They don\'t feel like theories because they\'re part of the dominant discourse, they feel normal. But in fact they are specific sets of assertions, about what bodies and sex and so on means. I\'m just expressing and looking at it a different way. There are people who believe in male and female. That\'s a theory. It just doesn\'t feel like a theory, it feels like an essential truth.', NULL, NULL, NULL, 1, '', ''),
	(208, 'book', 'Merriam-Webster\'s collegiate dictionary', 'Publisher information: Springfield, Mass. : Merriam-Webster, Inc., c2003.', NULL, NULL, NULL, NULL, 1, '9780877798088', '978-0-87779-808-8'),
	(209, 'book', 'Voices for a New Tomorrow: An Assessment of the Lesbian, Gay, Bisexual and Transgender Community of Allegheny County', 'Sandra Crouse Quinn, Ph.D.; Deborah Aaron, Ph.D.; Katrina Schwarz, MPH; Tammy Thomas, MSW, MPH', 'In fall 2001, Betty Hill, Executive Director of Persad Center, Inc. (Persad) and Dr. Deborah Aaron, representing the board of the Gay and Lesbian Community Center (GLCC) approached Dr. Sandra Quinn of the Graduate School of Public Health, University of Pittsburgh. As Ms. Hill took the reins of leadership at Persad, she believed that a community assessment was necessary to help determine new directions for the organization. Ms. Hill, Drs. Aaron and Quinn conceptualized a process that would provide a comprehensive picture of the lesbian, gay, bisexual and transgender (LGBT) community in Allegheny County. Persad, the GLCC and the Seven Project (Seven) saw the merits of this and the three organizations jointly proposed this community assessment to the Maurice Falk Medical Fund and the William J. Copeland Fund. In July 2002, the two foundations funded the project.\r\nThis community assessment is based on the Community Diagnosis model utilized by Guy Steuart:\r\n\r\n"Members of the community have expertise concerning their community, culture, and health problems, and they know what solutions are likely to work. The health professional and the client community are equal partners in the development, implementation and evaluation process" (Steckler, Dawson, Israel, & Eng, 1993, p. S8).\r\n\r\nCalling Community Diagnosis a critical part of program planning, Steuart stated that in contrast to a needs assessment:\r\n\r\n"Diagnosis is much broader and aims to understand how many facets of a community including culture, values and norms, leadership and power structure, means of communication, helping patterns, important community institutions, and history. A good diagnosis suggests what it is like to live in a community, what the important health problems are, what interventions are most likely to be efficacious, and how the problem would be best evaluated" (Steckler et. al, p.S9-10).\r\n\r\nAccording to Eng and Blanchard (1991), "the action-oriented community diagnosis... not only identifies needs, but initiates resolution as well by building on existing linkages and ties to the community" (p. 107).\r\nThis community assessment began in the summer of 2002 with primary data collection occurring in October, November and December. This final report was prepared in December 2002/January 2003 and will be presented at a community forum in late January 2003 as part of Persad\'s 30th anniversary series.', NULL, NULL, NULL, 3, '', ''),
	(210, 'book', 'Gay Male, Lesbian, Bisexual, and Transgender (GLBT) Students\' Perceptions of Homophobia at the University of Pittsburgh', 'Michael L. Strong', 'This paper investigates the University of Pittsburgh campus community and its environments for pervasiveness of homophobia. Eight self-identified GLBT students active in Rainbow Alliance, the campus GLBT-activist group, were interviewed and their perceptions recorded. Results showed that the campus environment and its constituents tended to have low perceived levels of homophobia overall, though males and male-dominated environments (like fraternities) exhibited more negative attitudes. All students felt comfortable in their residential environments and with Residence Life staff, in academic environments and with professors and teaching assistants, and in student affairs and with student affairs professionals. However, all respondents felt that administrators and the University of Pittsburgh in general did not do enough to support the needs of GLBT students.', NULL, NULL, NULL, 1, '', ''),
	(211, 'book', 'OutGiving Outlines: A compilation of educational reference materials from the National Gay and Lesbian Task Force\'s 11th Annual Creating Change Conference.', 'Mickey MacIntyre, Julie R. Enszer', 'November 11-15, 1998\r\nWilliam Penn Westin Hotel\r\n530 William Penn Place\r\nPittsburgh, Pennsylvania', NULL, NULL, NULL, 1, '', ''),
	(212, 'book', '101 Ways to Combat Prejudice', 'Barnes & Noble and the Anti-Defamation League', 'BARNES & NOBLE and THE ANTI-DEFAMATION LEAGUE (ADL) first launched the Close the Book on Hate campaign in September 2000. This continuing and unprecedented joint effort provides children and their parents, caregivers, teachers and civic leaders with various resources and programs to help end prejudice and discrimination in America. In addition to this free informational pamphlet, the Close the Book on Hate campaign features ADL\'s groundbreaking <i>Hate Hurts</i>, co-authored by Caryl Stern-LaRosa and Ellen Hofheimer Bettman, special recommended reading displays in Barnes & Noble stores across the country, and in-store educational programs and events. Barnes & Noble.com (www.bn.com) also prominently features this campaign.', NULL, NULL, NULL, 1, '', ''),
	(213, 'book', 'Gayellow Pages, Greater Northeast Edition: 1996-97 (Annual)', 'Publisher information: Renaissance House', NULL, NULL, NULL, NULL, 1, '9781885404084', '978-1-885404-08-4'),
	(214, 'book', 'Faultline: a novel', 'by Sheila Ortiz Taylor', NULL, NULL, NULL, NULL, 1, '9780930044244', '978-0-930044-24-4'),
	(215, 'book', 'A critical introduction to queer theory', 'Nikki Sullivan', NULL, NULL, NULL, NULL, 1, '9780814798416', '978-0-8147-9841-6'),
	(216, 'book', 'Queer London: perils and pleasures in the sexual metropolis, 1918-1957', 'Matt Houlbrook', NULL, NULL, NULL, NULL, 1, '9780226354620', '978-0-226-35462-0'),
	(217, 'book', 'Out of the Shadows', 'Ryan Olson and David Freeburg', 'Who are we, and what are we here for? This is the question we attempt to answer in the pages that follow. We are the students, teachers, and friends of Gonzaga University. This compilation, sponsored by HERO, tells our stories.\r\nIn some cases, society defines us before we get a chance to do it ourselves: we fall victim to stereotypes drawn too tightly. This collection is our attempt to redefine ourselves, to claim a place of our own choosing. We want our lives back. We want our school, our homes, and our families back.\r\nIn pursuit of this purpose, the editors of this collection have chosen a variety of submissions that reflect the lives of a wide variety of sexual minorities. Though some of us may share a common culture, all too often we share simple misunderstanding instead. But a thread unites us, and we invite you to follow this thread where it leads, through stories of fiction, of fact, of poetry and of memory.\r\nWhether real or created, each of them is true in the sense that they describe our realities.\r\n\r\nThese are our stories.', NULL, NULL, NULL, 1, '', ''),
	(218, 'book', 'My Invisible Kingdom: Letters from the Secret lives of Teens', 'Scott Fried', 'Who stands beside us in life\'s unbridgeable moments? Who will help us transcend as the internal voices echo, "I was there...when you were picked last for the team...when you waited by the phone for the call that never came...when you felt alone and unloved."\r\nEach of us inhabits an invisible kingdom. It is the secret life landscaped by loneliness and neglect. It is the home of our resident ghosts and our indwelling wounds. It is the cryptic code etched inside a heart of sorrow that no one else can see.\r\nThis is an entryway into that world, inspired by letters and emails of teens who have written to me in an attempt to share their pain and doubt on such diverse topics as eating disorders, self-mutilation, drug and alcohol abuse, sex, death and broken hearts. One-part love-letter, one-part guide, this book bears witness to each of our secret lives.\r\nAs one teen describes, "I want to steal the sun from the sky so everyone else can feel this darkness." In the words of another, "I wish I could realign the stars so I can change who I am and be something more for others to love." And finally, "Dear Scott, please write back, \'cause I have nowhere else to turn."\r\n\r\nHere is my reply.', NULL, NULL, NULL, 1, '', ''),
	(219, 'book', 'The 2005 National School Climate Survey: The Experiences of Lesbian, Gay, Bisexual and Transgender Youth in Our Nation\'s Schools', 'GLSEN', 'GLSEN\'s National School Climate Survey is the only national survey to document the experiences of students who identify as lesbian, gay, bisexual and transgender (LGBT) in America\'s secondary schools. Conducted biennially since 1999, the National School Climate Survey (NSCS) fills a crucial void in our collective understanding of the contemporary high school experience. The results of this survey are intended to inform educators, policymakers and the public at large, as part of GLSEN\'s ongoing effort to ensure that all schools are places where students are free to learn, regardless of sexual orientation or gender identity/expression.\r\nThe 2005 NSCS results summarized here continue to track the endemic problem of name-calling, harassment and violence directed at LGBT students, while offering information about the impact of these experiences on academic performance and the effect of interventions designed to address the underlying problem. In particular, the 2005 survey data allowed us to examine the role that state education legislation has in creating (or not creating) safer schools for all students, including LGBT students. The 2005 NSCS paints a disturbing picture of the school experiences of LGBT students. However, it also provides further insight into the solutions for creating safer schools for all students.', NULL, NULL, NULL, 2, '', ''),
	(220, 'book', '2002-2003 Transformations: Student Organizing Yearbook', 'GLSEN', 'Letter from the Editor\r\n\r\nThis year I was fortunate enough to be able to intern with the Student Organizing Department of the Gay, Lesbian & Straight Education Network (GLSEN). As a high school senior, being able to spend one day a week in such a positive and supportive environment was probably the best transitional step I could make from childhood to adulthood.\r\nOriginally I saw the work of being the Student Organizing Yearbook Assistant as simply reviewing the submissions. It took very little time before I realized it was much more than that. The articles gave me the chance as a gay-straight alliance (GSA) leader and as a student to learn from my peers. Reviewing the articles introduced me to new people and their work towards equality. Reading these articles let me put myself into the mind of a transsexual boy and learn about his frustrations, a student who made all the difference by aiding a teacher training in her school, a young man who made the hybrid of his two passions (art and activism), and even a GSA leader who\'s in the closet. Being able to meet these students through their articles and artwork gave me the wonderful opportunity to get to know amazing student leaders who, I\'m sure, will far surpass the work done by generations before.\r\nI hope this year\'s Student Organizing Yearbook, "Transformations," will be able to bring all to [sic] of you the same inspiration and feeling of friendship it has to me. I hope next year\'s submissions will be just as diverse, if not more, and representative of the highly motivated and inspirational students country-wide. Most of all, I hope that you motivated and inspirational students across the country continue with your work and make the world that much better.\r\n\r\nLaurie Dalzell\r\n2002-2003 Student Organizing Yearbook Editor', NULL, NULL, NULL, 2, '', ''),
	(221, 'book', 'Fighting for Resources: A Direct Action Organizing Guide for Winning Lesbian, Gay, Bisexual and Transgender Resource Centers on College Campuses', 'LGBT Student Empowerment Project, USSA Foundation', 'The United States Student Association is the country\'s oldest and largest national student organization, representing over a million students as colleges and universities around the United States. Since 1947, USSA has been representing the student voice on Capitol Hill, in the White House and in the Department of Education. For years USSA has been working with student organizers in winning concrete victories that improve people\'s lives on campuses.\r\n<b>In 2001, the Lesbian, Gay, Bisexual, Transgender (LGBT) Student Empowerment Project was created <i>by</i> students to provide technical support <i>for</i> students, organizing to increase resources for LGBT students in higher education.</b> Through this project, the United States Student Association Foundation provides students with informational materials, direct action organizing trainings, and on going advice and assistance on issues of retention of LGBT students in colleges and universities across the country.\r\nOf the thousands of universities in this country, only about 400 have nondiscrimination policies that include sexual orientation and just a handful include gender identity. The number of LGBT Retention/Resource programs that exist on campuses is extremely limited. Most universities in this country ignore the issues that LGBT students face in our schools, leaving queer students struggling in their efforts to get a degree.\r\n<b>This struggle is ours.</b> This struggle should not be left up to our administrators, faculty and alumni. These problems can not always be solved by our national organizations. <b>As student organizers, we must not sit back and allow our universities to constantly deny LGBT people the resources necessary to participate fully in our colleges and universities.</b> As hate crimes plague our campus communities, homophobia frequents our classrooms and curriculum, and discrimination lives on in our admissions, we must identify and use the power that we have over these situations. We must count ourselves among the movement builders and organizers for justice and equality.', NULL, NULL, NULL, 1, '', ''),
	(222, 'book', 'Transgender Equality: A Handbook for Activists and Policymakers', 'Paisley Currah and Shannon Minter', 'The Policy Institute of the National Gay and Lesbian Task Force is honored to publish this handbook to provide activists and policymakers with the tools they need to pass transgender-inclusive non-discrimination and anti-violence legislation. Written by three of the gay, lesbian, bisexual and transgender movement\'s brightest transgender scholar-activists, <i>Transgender Equality</i> is an invaluable resource guide, providing an introduction to transgender issues, model language for legislation, talking points, responses to frequently asked questions, and a comprehensive resource listing and bibliography. We trust it will strengthen efforts to legislate equality for transgender people at the local, state and national level.', NULL, NULL, NULL, 1, '', ''),
	(223, 'book', 'International Directory of Bisexual Groups', 'Robyn Ochs', 'A service of the Bisexual Resource Center', NULL, NULL, NULL, 1, '', ''),
	(224, 'magazine', 'OUT', '', NULL, '2009', 12, NULL, 2, '', ''),
	(225, 'magazine', 'OUT', '', NULL, '2010', 02, NULL, 2, '', ''),
	(226, 'magazine', 'OUT', '', NULL, '2010', 09, NULL, 1, '', ''),
	(227, 'magazine', 'OUT', '', NULL, '2010', 04, NULL, 1, '', ''),
	(228, 'magazine', 'OUT', '', NULL, '2010', 05, NULL, 1, '', ''),
	(229, 'magazine', 'OUT', '', NULL, '2010', 06, NULL, 1, '', ''),
	(230, 'magazine', 'OUT', '', NULL, '2010', 08, NULL, 1, '', ''),
	(231, 'magazine', 'Out', '', NULL, '2007', 03, NULL, 1, '', ''),
	(232, 'magazine', 'OUT', '', NULL, '2011', 11, NULL, 1, '', ''),
	(233, 'magazine', 'OUT', '', NULL, '2007', 09, NULL, 1, '', ''),
	(234, 'magazine', 'Out', '', NULL, '2007', 10, NULL, 1, '', ''),
	(235, 'magazine', 'OUT', '', NULL, '2010', 03, NULL, 1, '', ''),
	(236, 'magazine', 'Out', '', NULL, '2007', 12, NULL, 1, '', ''),
	(237, 'magazine', 'Out', '', NULL, '2008', 02, NULL, 1, '', ''),
	(238, 'magazine', 'Out', '', NULL, '2008', 03, NULL, 1, '', ''),
	(239, 'magazine', 'Out', '', NULL, '2008', 04, NULL, 1, '', ''),
	(240, 'magazine', 'OUT', '', NULL, '2011', 12, NULL, 1, '', ''),
	(241, 'magazine', 'Out', '', NULL, '2008', 05, NULL, 1, '', ''),
	(242, 'magazine', 'OUT', '', NULL, '2012', 12, NULL, 1, '', ''),
	(243, 'magazine', 'Out', '', NULL, '2008', 06, NULL, 1, '', ''),
	(244, 'magazine', 'Out', '', NULL, '2008', 09, NULL, 1, '', ''),
	(245, 'magazine', 'Out', '', NULL, '2008', 10, NULL, 1, '', ''),
	(246, 'magazine', 'Out', '', NULL, '2008', 12, NULL, 1, '', ''),
	(247, 'magazine', 'Out', '', NULL, '2009', 02, NULL, 1, '', ''),
	(248, 'magazine', 'Out', '', NULL, '2009', 05, NULL, 1, '', ''),
	(249, 'magazine', 'Out', '', NULL, '2009', 06, NULL, 1, '', ''),
	(250, 'magazine', 'Out', '', NULL, '2009', 08, NULL, 1, '', ''),
	(251, 'magazine', 'OUT', '', NULL, '2009', 09, NULL, 1, '', ''),
	(252, 'magazine', 'OUT', '', NULL, '2009', 10, NULL, 1, '', ''),
	(253, 'magazine', 'OUT', '', NULL, '2009', 11, NULL, 1, '', ''),
	(254, 'magazine', 'THE OUT TRAVELER', '', NULL, '2008', 15, NULL, 1, '', ''),
	(255, 'magazine', 'THE OUT TRAVELER', '', NULL, '2008', 13, NULL, 1, '', ''),
	(256, 'magazine', 'THE OUT TRAVELER', '', NULL, '2008', 16, NULL, 1, '', ''),
	(257, 'magazine', 'THE OUT TRAVELER', '', NULL, '2008', 14, NULL, 1, '', ''),
	(258, 'magazine', 'THE OUT TRAVELER', '', NULL, '2007', 14, NULL, 1, '', ''),
	(259, 'magazine', 'THE OUT TRAVELER', '', NULL, '2007', 13, NULL, 1, '', ''),
	(260, 'magazine', 'curve', '', NULL, '2010', 01, NULL, 1, '', ''),
	(261, 'magazine', 'curve', '', NULL, '2009', 12, NULL, 1, '', ''),
	(262, 'magazine', 'curve', '', NULL, '2009', 11, NULL, 1, '', ''),
	(263, 'magazine', 'curve', '', NULL, '2010', 04, NULL, 1, '', ''),
	(264, 'magazine', 'curve', '', NULL, '2010', 07, NULL, 1, '', ''),
	(265, 'magazine', 'curve', '', NULL, '2010', 05, NULL, 1, '', ''),
	(266, 'magazine', 'curve', '', NULL, '2010', 03, NULL, 1, '', ''),
	(267, 'magazine', 'curve', '', NULL, '2009', 10, NULL, 1, '', ''),
	(268, 'magazine', 'curve', '', NULL, '2009', 09, NULL, 1, '', ''),
	(269, 'magazine', 'curve', '', NULL, '2009', 11, NULL, 1, '', ''),
	(270, 'magazine', 'curve', '', NULL, '2009', 07, NULL, 1, '', ''),
	(271, 'magazine', 'curve', '', NULL, '2009', 06, NULL, 1, '', ''),
	(272, 'magazine', 'cue pittsburgh', '', NULL, '2009', 12, NULL, 1, '', ''),
	(273, 'magazine', 'cue pittsburgh', '', NULL, '2010', 01, NULL, 1, '', '');
/*!40000 ALTER TABLE `libraryItems` ENABLE KEYS */;


-- Dumping structure for table deitloff_rainbow.locations
CREATE TABLE IF NOT EXISTS `locations` (
  `locationID` int(100) unsigned NOT NULL AUTO_INCREMENT,
  `building` int(10) unsigned NOT NULL,
  `room` varchar(40) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `capacity` int(10) unsigned NOT NULL,
  PRIMARY KEY (`locationID`)
) ENGINE=MyISAM AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

-- Dumping data for table deitloff_rainbow.locations: 11 rows
/*!40000 ALTER TABLE `locations` DISABLE KEYS */;
INSERT INTO `locations` (`locationID`, `building`, `room`, `capacity`) VALUES
	(1, 1, 'Room 837', 0),
	(2, 1, 'Kurtzman Room', 0),
	(3, 1, 'Ballroom', 0),
	(4, 3, 'Ballroom', 0),
	(5, 3, 'Dining Room', 0),
	(6, 5, 'Basketball Court', 0),
	(7, 1, 'Room 611', 0),
	(8, 1, 'Room 630', 0),
	(9, 1, 'Room 548', 0),
	(10, 1, 'Dining Room A', 0),
	(11, 1, 'Room 650', 0);
/*!40000 ALTER TABLE `locations` ENABLE KEYS */;


-- Dumping structure for table deitloff_rainbow.meetingFeedback
CREATE TABLE IF NOT EXISTS `meetingFeedback` (
  `feedbackID` int(100) NOT NULL AUTO_INCREMENT,
  `meeting` int(100) NOT NULL,
  `member` int(100) NOT NULL,
  `rating` int(1) NOT NULL,
  `comment` text CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `lastModified` datetime NOT NULL,
  `agrees` int(10) NOT NULL,
  `disagrees` int(10) NOT NULL,
  PRIMARY KEY (`feedbackID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Dumping data for table deitloff_rainbow.meetingFeedback: 0 rows
/*!40000 ALTER TABLE `meetingFeedback` DISABLE KEYS */;
/*!40000 ALTER TABLE `meetingFeedback` ENABLE KEYS */;


-- Dumping structure for table deitloff_rainbow.meetingFeedbackRatings
CREATE TABLE IF NOT EXISTS `meetingFeedbackRatings` (
  `ratingID` int(200) NOT NULL AUTO_INCREMENT,
  `feedback` int(100) NOT NULL,
  `member` int(100) NOT NULL,
  `rating` enum('agree','disagree') NOT NULL,
  PRIMARY KEY (`ratingID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Dumping data for table deitloff_rainbow.meetingFeedbackRatings: 0 rows
/*!40000 ALTER TABLE `meetingFeedbackRatings` DISABLE KEYS */;
/*!40000 ALTER TABLE `meetingFeedbackRatings` ENABLE KEYS */;


-- Dumping structure for table deitloff_rainbow.meetings
CREATE TABLE IF NOT EXISTS `meetings` (
  `meetingID` int(100) unsigned NOT NULL AUTO_INCREMENT,
  `name` text CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `date` date NOT NULL,
  `schoolYear` year(4) NOT NULL,
  `time` time NOT NULL,
  `type` int(10) unsigned NOT NULL,
  `location` int(10) unsigned NOT NULL,
  `description` text CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`meetingID`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- Dumping data for table deitloff_rainbow.meetings: 5 rows
/*!40000 ALTER TABLE `meetings` DISABLE KEYS */;
/*!40000 ALTER TABLE `meetings` ENABLE KEYS */;


-- Dumping structure for table deitloff_rainbow.meetingTypes
CREATE TABLE IF NOT EXISTS `meetingTypes` (
  `typeID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `handle` varchar(20) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `isLounge` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`typeID`),
  UNIQUE KEY `handle` (`handle`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

-- Dumping data for table deitloff_rainbow.meetingTypes: 9 rows
/*!40000 ALTER TABLE `meetingTypes` DISABLE KEYS */;
INSERT INTO `meetingTypes` (`typeID`, `handle`, `isLounge`) VALUES
	(1, 'Speaker', 0),
	(2, 'Workshop', 0),
	(3, 'Panel', 0),
	(4, 'Movie (non-speaker)', 0),
	(5, 'Game night', 1),
	(6, 'Officer elections', 0),
	(7, 'Arts and crafts', 1),
	(8, 'Park visit', 1),
	(9, 'Steering Committee', 0);
/*!40000 ALTER TABLE `meetingTypes` ENABLE KEYS */;


-- Dumping structure for table deitloff_rainbow.members
CREATE TABLE IF NOT EXISTS `members` (
  `memberID` int(100) unsigned NOT NULL AUTO_INCREMENT,
  `firstName` varchar(30) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `lastName` varchar(50) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `displayName` varchar(80) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `university` int(10) NOT NULL,
  `universityHandle` varchar(10) NOT NULL,
  `dateCreated` datetime NOT NULL,
  `password` varchar(32) NOT NULL DEFAULT 'c21f969b5f03d33d43e04f8f136e7682',
  `passwordResetHash` varchar(23) DEFAULT NULL,
  `passwordResetRequest` datetime DEFAULT NULL,
  `mobile` varchar(10) DEFAULT NULL,
  `mobileCarrier` enum('verizon','sprint','att','virgin','comcast','boost','nextel') DEFAULT NULL,
  PRIMARY KEY (`memberID`)
) ENGINE=MyISAM AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;

-- Dumping data for table deitloff_rainbow.members: 15 rows
/*!40000 ALTER TABLE `members` DISABLE KEYS */;
/*!40000 ALTER TABLE `members` ENABLE KEYS */;


-- Dumping structure for table deitloff_rainbow.officeHolidays
CREATE TABLE IF NOT EXISTS `officeHolidays` (
  `holidayID` int(100) NOT NULL AUTO_INCREMENT,
  `day` date NOT NULL,
  PRIMARY KEY (`holidayID`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- Dumping data for table deitloff_rainbow.officeHolidays: 5 rows
/*!40000 ALTER TABLE `officeHolidays` DISABLE KEYS */;
/*!40000 ALTER TABLE `officeHolidays` ENABLE KEYS */;


-- Dumping structure for table deitloff_rainbow.officeHourAttendance
CREATE TABLE IF NOT EXISTS `officeHourAttendance` (
  `attendanceID` int(255) NOT NULL AUTO_INCREMENT,
  `officeHour` int(100) NOT NULL,
  `member` int(100) NOT NULL,
  `date` date NOT NULL,
  `attended` tinyint(1) NOT NULL DEFAULT '0',
  `signedIn` datetime DEFAULT NULL,
  PRIMARY KEY (`attendanceID`)
) ENGINE=MyISAM AUTO_INCREMENT=1096 DEFAULT CHARSET=latin1;

-- Dumping data for table deitloff_rainbow.officeHourAttendance: 1,094 rows
/*!40000 ALTER TABLE `officeHourAttendance` DISABLE KEYS */;
/*!40000 ALTER TABLE `officeHourAttendance` ENABLE KEYS */;


-- Dumping structure for table deitloff_rainbow.officeHours
CREATE TABLE IF NOT EXISTS `officeHours` (
  `hourID` int(200) NOT NULL AUTO_INCREMENT,
  `member` int(100) NOT NULL,
  `semester` enum('fall','spring') NOT NULL,
  `schoolYear` year(4) NOT NULL,
  `day` enum('m','t','w','h','f') NOT NULL,
  `hour` int(2) unsigned zerofill NOT NULL,
  `minutes` int(2) unsigned zerofill NOT NULL DEFAULT '00',
  `isHalfHour` tinyint(1) NOT NULL DEFAULT '0',
  `effective` date DEFAULT NULL,
  `terminates` date DEFAULT NULL,
  PRIMARY KEY (`hourID`)
) ENGINE=MyISAM AUTO_INCREMENT=101 DEFAULT CHARSET=latin1;

-- Dumping data for table deitloff_rainbow.officeHours: 100 rows
/*!40000 ALTER TABLE `officeHours` DISABLE KEYS */;
/*!40000 ALTER TABLE `officeHours` ENABLE KEYS */;


-- Dumping structure for table deitloff_rainbow.officers
CREATE TABLE IF NOT EXISTS `officers` (
  `officerID` int(100) unsigned NOT NULL AUTO_INCREMENT,
  `member` int(100) unsigned NOT NULL,
  `position` int(10) unsigned NOT NULL,
  `schoolYear` year(4) NOT NULL,
  `semester` enum('fall','spring') NOT NULL,
  `isPerpetual` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`officerID`)
) ENGINE=MyISAM AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

-- Dumping data for table deitloff_rainbow.officers: 12 rows
/*!40000 ALTER TABLE `officers` DISABLE KEYS */;
/*!40000 ALTER TABLE `officers` ENABLE KEYS */;


-- Dumping structure for table deitloff_rainbow.pages
CREATE TABLE IF NOT EXISTS `pages` (
  `handle` varchar(30) NOT NULL,
  `title` varchar(100) NOT NULL,
  `fileName` varchar(35) NOT NULL,
  `permissions` enum('officer','public','president') NOT NULL DEFAULT 'officer',
  `parentHandle` varchar(30) DEFAULT NULL,
  `breadcrumbLink` varchar(60) DEFAULT NULL,
  `javascriptFile` varchar(30) NOT NULL,
  `traversalLeft` int(100) NOT NULL,
  `traversalRight` int(100) NOT NULL,
  `traversalDepth` int(10) NOT NULL,
  PRIMARY KEY (`handle`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Dumping data for table deitloff_rainbow.pages: 29 rows
/*!40000 ALTER TABLE `pages` DISABLE KEYS */;
INSERT INTO `pages` (`handle`, `title`, `fileName`, `permissions`, `parentHandle`, `breadcrumbLink`, `javascriptFile`, `traversalLeft`, `traversalRight`, `traversalDepth`) VALUES
	('login', 'Login', 'login.php', 'public', NULL, NULL, '', 1, 4, 0),
	('logout', 'Logout', 'logout.php', 'officer', NULL, NULL, '', 5, 6, 0),
	('error', 'Error', 'error.php', 'public', NULL, NULL, '', 7, 8, 0),
	('index', 'Home', 'index.php', 'officer', NULL, NULL, '', 9, 10, 0),
	('change-password', 'Change password', 'change-password.php', 'officer', NULL, NULL, '', 11, 12, 0),
	('browse-members', 'Browse members', 'browse-members.php', 'officer', NULL, NULL, '', 13, 18, 0),
	('member', 'Member profile', 'member.php', 'officer', 'browse-members', NULL, '', 14, 15, 1),
	('add-member', 'Add new member', 'add-member.php', 'officer', 'browse-members', NULL, 'addMember.js', 16, 17, 1),
	('create-meeting', 'Create new meeting', 'create-meeting.php', 'officer', 'browse-meetings', NULL, 'createMeeting.js', 20, 21, 1),
	('meeting', 'Meeting profile', 'meeting.php', 'officer', 'browse-meetings', 'meeting/{{ARG}}', '', 22, 33, 1),
	('browse-meetings', 'Browse meetings', 'browse-meetings.php', 'officer', NULL, NULL, '', 19, 34, 0),
	('forgot-password', 'Reset your password', 'forgot-password.php', 'public', 'login', NULL, '', 2, 3, 1),
	('add-library-item', 'Add new library material', 'add-library-item.php', 'officer', 'browse-library', NULL, 'addLibraryItem.js', 36, 37, 1),
	('browse-library', 'Browse library', 'browse-library.php', 'officer', NULL, NULL, '', 35, 52, 0),
	('library-item', 'Library item profile', 'library-item.php', 'officer', 'browse-library', 'library-item/{{ARG}}', '', 38, 45, 1),
	('checked-out-items', 'Checked out materials list', 'checked-out-items.php', 'officer', 'browse-library', NULL, '', 46, 49, 1),
	('library-checkin', 'Check-in materials', 'library-checkin.php', 'officer', 'checked-out-items', NULL, '', 47, 48, 2),
	('library-checkout', 'Check-out materials', 'library-checkout.php', 'officer', 'browse-library', NULL, '', 50, 51, 1),
	('library-checkout-express', 'Express check-out', 'library-checkout-express.php', 'officer', 'library-item', NULL, '', 39, 40, 2),
	('meeting-attendance', 'Manage meeting attendance', 'meeting-attendance.php', 'officer', 'meeting', 'meeting-attendance/{{ARG}}', '', 23, 24, 2),
	('meeting-description', 'Modify meeting description', 'meeting-description.php', 'officer', 'meeting', 'meeting-description/{{ARG}}', '', 25, 26, 2),
	('delete-meeting', 'Delete meeting', 'delete-meeting.php', 'president', 'meeting', 'delete-meeting/{{ARG}}', '', 27, 28, 2),
	('edit-meeting', 'Edit meeting information', 'edit-meeting.php', 'officer', 'meeting', 'edit-meeting/{{ARG}}', '', 29, 30, 2),
	('meeting-feedback', 'Meeting feedback', 'meeting-feedback.php', 'officer', 'meeting', 'meeting-feedback/{{ARG}}', '', 31, 32, 2),
	('office-hours', 'View office hours', 'office-hours.php', 'officer', NULL, NULL, '', 53, 58, 0),
	('office-hour-signin', 'OFFICE_HOUR_SIGNIN_PAGE', 'office-hour-signin.php', 'officer', 'office-hours', NULL, '', 54, 55, 1),
	('edit-library-item', 'Edit library item', 'edit-library-item.php', 'officer', 'library-item', 'edit-library-item/{{ARG}}', '', 41, 42, 2),
	('library-item-description', 'Edit library item description', 'library-item-description.php', 'officer', 'library-item', 'library-item-description/{{ARG}}', '', 43, 44, 2),
	('assign-office-hour', 'Assign office hour', 'assign-office-hour.php', 'officer', 'office-hours', NULL, '', 56, 57, 1);
/*!40000 ALTER TABLE `pages` ENABLE KEYS */;


-- Dumping structure for procedure deitloff_rainbow.pageTreewalk
DELIMITER //
//
DELIMITER ;


-- Dumping structure for procedure deitloff_rainbow.pageTreewalk_piece
DELIMITER //
//
DELIMITER ;


-- Dumping structure for table deitloff_rainbow.partnerGroups
CREATE TABLE IF NOT EXISTS `partnerGroups` (
  `groupID` int(100) unsigned NOT NULL AUTO_INCREMENT,
  `name` text CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `isUniversityGroup` tinyint(1) NOT NULL DEFAULT '1',
  `university` int(10) unsigned NOT NULL,
  PRIMARY KEY (`groupID`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- Dumping data for table deitloff_rainbow.partnerGroups: 3 rows
/*!40000 ALTER TABLE `partnerGroups` DISABLE KEYS */;
INSERT INTO `partnerGroups` (`groupID`, `name`, `isUniversityGroup`, `university`) VALUES
	(1, '(CWO) Campus Women\'s Organization', 1, 1),
	(2, '(BAS) Black Action Society', 1, 1),
	(3, '(PATF) Pittsburgh Aids Task Force', 0, 0);
/*!40000 ALTER TABLE `partnerGroups` ENABLE KEYS */;


-- Dumping structure for table deitloff_rainbow.partners
CREATE TABLE IF NOT EXISTS `partners` (
  `pairingID` int(100) unsigned NOT NULL AUTO_INCREMENT,
  `meeting` int(100) unsigned NOT NULL,
  `group` int(10) unsigned NOT NULL,
  PRIMARY KEY (`pairingID`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- Dumping data for table deitloff_rainbow.partners: 5 rows
/*!40000 ALTER TABLE `partners` DISABLE KEYS */;
INSERT INTO `partners` (`pairingID`, `meeting`, `group`) VALUES
	(1, 8, 1),
	(2, 8, 2),
	(3, 9, 1),
	(4, 9, 2),
	(5, 10, 2);
/*!40000 ALTER TABLE `partners` ENABLE KEYS */;


-- Dumping structure for table deitloff_rainbow.positions
CREATE TABLE IF NOT EXISTS `positions` (
  `positionID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `handle` varchar(20) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `name` varchar(50) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `term` enum('year','semester','perpetual') NOT NULL,
  `email` varchar(60) DEFAULT NULL,
  `emailPassword` varchar(20) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `permissionLevel` enum('officer','president') NOT NULL DEFAULT 'officer',
  `officeHourRequirements` int(1) NOT NULL,
  PRIMARY KEY (`positionID`),
  UNIQUE KEY `handle` (`handle`)
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

-- Dumping data for table deitloff_rainbow.positions: 10 rows
/*!40000 ALTER TABLE `positions` DISABLE KEYS */;
INSERT INTO `positions` (`positionID`, `handle`, `name`, `term`, `email`, `emailPassword`, `permissionLevel`, `officeHourRequirements`) VALUES
	(1, 'president', 'President', 'year', 'rainbow.president@gmail.com', NULL, 'president', 6),
	(2, 'vicepresident', 'Vice President', 'year', 'rainbow.vicepres@gmail.com', NULL, 'officer', 6),
	(3, 'businessmanager', 'Business Manager', 'year', 'rainbow.businessman@gmail.com', NULL, 'officer', 6),
	(4, 'officeadmin', 'Office Administrator', 'year', 'rainbowofficeadmin@gmail.com', NULL, 'officer', 6),
	(5, 'politicalaction', 'Political Action Chair', 'semester', 'rainbow.political@gmail.com', NULL, 'officer', 5),
	(6, 'communityoutreach', 'Community Outreach Chair', 'semester', 'rainbow.commoutreach@gmail.com', NULL, 'officer', 5),
	(7, 'publicity', 'Publicity Chair', 'semester', 'rainbow.publicity@gmail.com', NULL, 'officer', 5),
	(8, 'executiveassistant', 'Executive Assistant', 'semester', 'rainbow.execassistant@gmail.com', NULL, 'officer', 5),
	(9, 'commassistant', 'Communications Assistant', 'semester', 'rainbow.commassistant@gmail.com', NULL, 'officer', 5),
	(10, 'webmaster', 'Webmaster', 'perpetual', NULL, NULL, 'president', 0);
/*!40000 ALTER TABLE `positions` ENABLE KEYS */;


-- Dumping structure for table deitloff_rainbow.rainbowDashes
CREATE TABLE IF NOT EXISTS `rainbowDashes` (
  `rainbowDashID` int(10) NOT NULL AUTO_INCREMENT,
  `mood` enum('snarky','worried','happy','shy','sir','oops','roger','haay','confused','babytalk','tired','brite') NOT NULL,
  `side` enum('left','right') NOT NULL,
  `quote` text NOT NULL,
  PRIMARY KEY (`rainbowDashID`)
) ENGINE=MyISAM AUTO_INCREMENT=21 DEFAULT CHARSET=latin1;

-- Dumping data for table deitloff_rainbow.rainbowDashes: 20 rows
/*!40000 ALTER TABLE `rainbowDashes` DISABLE KEYS */;
INSERT INTO `rainbowDashes` (`rainbowDashID`, `mood`, `side`, `quote`) VALUES
	(1, 'snarky', 'left', 'Remember that thing you were suppose to do?<br /><br />No you don\'t.'),
	(2, 'sir', 'left', 'lol i\'m a pony<br>lol wut is happen<br>halp'),
	(3, 'oops', 'right', 'I just realized there is no escape valve.<br>Sorry.<br>My bad.'),
	(4, 'sir', 'right', 'lol how do i work this?'),
	(5, 'sir', 'left', 'how does a pony make a life?'),
	(6, 'roger', 'right', 'I shall <i>make</i> it my duty, then, to find more cat pictures.'),
	(7, 'haay', 'left', 'Haay gurl haay.'),
	(8, 'worried', 'right', 'I think I just uploaded porn.<br>How do I delete?'),
	(9, 'happy', 'left', 'I was so popular!'),
	(10, 'happy', 'left', 'I will take you and break you in two, slice open your skull, and clean the sockets with bleach before I sip Limeade from your cranium.'),
	(11, 'shy', 'right', 'Um, I think this should work, at least.'),
	(12, 'snarky', 'left', 'Oh, ha ha. I bet you expected to get something done here, didn\'t you?'),
	(13, 'worried', 'right', 'I think I need an adult.'),
	(14, 'snarky', 'left', 'No adults allowed!'),
	(15, 'sir', 'left', 'I can count to two on my own two hooves!'),
	(16, 'brite', 'right', 'I\'m still relevant!'),
	(17, 'tired', 'right', 'Ohhhhh, fuck, <i>you</i>.'),
	(18, 'confused', 'left', 'So wait, <i>what</i> happens when a man loves a woman?'),
	(19, 'snarky', 'left', '<strong>Did you know?</strong> I\'m not a gay Ponyta.'),
	(20, 'babytalk', 'right', 'Whuzacyutewiddlewainboeovvizer. Whuuzzyeww.');
/*!40000 ALTER TABLE `rainbowDashes` ENABLE KEYS */;


-- Dumping structure for table deitloff_rainbow.semesters
CREATE TABLE IF NOT EXISTS `semesters` (
  `semesterID` int(100) NOT NULL AUTO_INCREMENT,
  `semester` enum('fall','spring') NOT NULL,
  `schoolYear` year(4) NOT NULL,
  `starts` date NOT NULL,
  `ends` date NOT NULL,
  `officeHoursStart` date NOT NULL,
  `officeHoursEnd` date NOT NULL,
  PRIMARY KEY (`semesterID`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

-- Dumping data for table deitloff_rainbow.semesters: 8 rows
/*!40000 ALTER TABLE `semesters` DISABLE KEYS */;
INSERT INTO `semesters` (`semesterID`, `semester`, `schoolYear`, `starts`, `ends`, `officeHoursStart`, `officeHoursEnd`) VALUES
	(1, 'fall', '2012', '2012-08-27', '2012-12-15', '2012-09-03', '2012-12-07'),
	(2, 'spring', '2012', '2013-01-07', '2013-04-27', '2013-01-14', '2013-04-20'),
	(3, 'fall', '2013', '2013-08-26', '2013-12-14', '2013-09-02', '2013-12-07'),
	(4, 'spring', '2013', '2014-01-06', '2014-04-26', '2014-01-13', '2014-04-19'),
	(5, 'fall', '2014', '2014-08-25', '2014-12-13', '2014-09-01', '2014-12-06'),
	(6, 'spring', '2014', '2015-01-05', '2015-04-25', '2015-01-12', '2015-04-18'),
	(7, 'fall', '2015', '2015-08-31', '2015-12-19', '2015-09-07', '2015-12-12'),
	(8, 'spring', '2015', '2016-01-06', '2016-04-30', '2016-01-11', '2016-04-23');
/*!40000 ALTER TABLE `semesters` ENABLE KEYS */;


-- Dumping structure for table deitloff_rainbow.universities
CREATE TABLE IF NOT EXISTS `universities` (
  `universityID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`universityID`)
) ENGINE=MyISAM AUTO_INCREMENT=21 DEFAULT CHARSET=latin1;

-- Dumping data for table deitloff_rainbow.universities: 20 rows
/*!40000 ALTER TABLE `universities` DISABLE KEYS */;
INSERT INTO `universities` (`universityID`, `name`) VALUES
	(1, 'University of Pittsburgh'),
	(2, 'Carnegie Mellon University'),
	(3, 'Duquesne University'),
	(4, 'Chatham University'),
	(5, 'Carlow University'),
	(6, 'Allegheny College'),
	(7, 'Geneva College'),
	(8, 'La Roche College'),
	(9, 'Point Park University'),
	(10, 'Robert Morris University'),
	(11, 'St. Vincent College'),
	(12, 'Seton Hill University'),
	(13, 'Washington and Jefferson College'),
	(14, 'Art Institute of Pittsburgh'),
	(15, 'Pennsylvania Culinary Institute'),
	(16, 'Pittsburgh Filmmakers\' School of Film, Photography, and Digital Media'),
	(17, 'Pittsburgh Technical Institute'),
	(18, 'Pittsburgh Institute of Mortuary Science'),
	(19, 'Community College of Allegheny County'),
	(20, 'University of Pennsylvania');
/*!40000 ALTER TABLE `universities` ENABLE KEYS */;


-- Dumping structure for table deitloff_rainbow.weekEvents
CREATE TABLE IF NOT EXISTS `weekEvents` (
  `eventID` int(100) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `semester` enum('fall','spring') NOT NULL,
  `schoolYear` year(4) NOT NULL,
  PRIMARY KEY (`eventID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Dumping data for table deitloff_rainbow.weekEvents: 0 rows
/*!40000 ALTER TABLE `weekEvents` DISABLE KEYS */;
/*!40000 ALTER TABLE `weekEvents` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
