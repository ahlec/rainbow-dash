<?php
require_once ("/home/deitloff/public_html/rainbow-dash/lib/config.inc.php");
$session = session();
$database = database();
$report = report();

if ($database == null)
{
    define("FATAL_ERROR_MESSAGE", "We have encountered a problem while attempting to connect to the database, and we can't connect.");
    require_once (DOCUMENT_ROOT . "/lib/index-majorError.php");
    exit();
}
?>