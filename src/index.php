<?php
/* setup */
require_once ("lib/config.inc.php");
$session = session();
$database = database();
$report = report();

if ($database == null)
{
    define("FATAL_ERROR_MESSAGE", "We have encountered a problem while attempting to connect to the database, and we can't connect.");
    require_once (DOCUMENT_ROOT . "/lib/index-majorError.php");
    exit();
}

/* determine page */
$path_pieces = explode("/", (isset($_GET["path"]) ? trim($_GET["path"], "/\t\n \r") : ""));
if (count($path_pieces) == 0 || mb_strlen($path_pieces[0]) == 0 || ctype_space($path_pieces[0]))
{
    $path_pieces = array("index");
}
else
{
    $path_pieces[0] = mb_strtolower($path_pieces[0]);
}
$base_page = $database->querySingle("SELECT permissions FROM pages WHERE handle='" .
    $database->escapeString($path_pieces[0]) . "' LIMIT 1", true);
if ($base_page === false)
{
    array_unshift($path_pieces, "error", "404");
}
else if ($session == null && $base_page["permissions"] != "public")
{
    array_unshift($path_pieces, "error", "401");
}
else if (!in_array($base_page["permissions"], ($session != null ? $session->permissions() : array("public"))))
{
    array_unshift($path_pieces, "error", "403");
}

$pageInfo = $database->querySingle("SELECT fileName, title, javascriptFile, traversalLeft, traversalRight FROM pages WHERE handle='" .
    $path_pieces[0] . "' LIMIT 1", true);
if (dirname(DOCUMENT_ROOT . "/pages/" . $pageInfo["fileName"]) != DOCUMENT_ROOT . "/pages")
{
    array_unshift($path_pieces, "error", "500");
    $pageInfo = $database->querySingle("SELECT fileName, title, javascriptFile, traversalLeft, traversalRight FROM pages WHERE handle='" .
        $path_pieces[0] . "' LIMIT 1", true);
}
if ($session != null && $session->resettingPassword())
{
    $pageInfo = $database->querySingle("SELECT fileName, title, javascriptFile, traversalLeft, traversalRight FROM pages WHERE " .
        "handle='change-password' LIMIT 1", true);
    array_unshift($path_pieces, "change-password");
}
$pageName = array_shift($path_pieces);


/* load page */
$args = arguments($pageName, (isset($_GET["path"]) ? trim($_GET["path"], "\t\n \r") : ""));
require_once (DOCUMENT_ROOT . "/pages/" . $pageInfo["fileName"]);
$page = new Page();
$page->preRender($database, $session, $path_pieces);

/* get a new Rainbow Dash */
$rainbowDash = $database->querySingle("SELECT mood, side, quote FROM rainbowDashes" . (isset($_SESSION[RAINBOW_DASH_HEADER_SESSION]) ?
    " WHERE mood<>'" . $database->escapeString($_SESSION[RAINBOW_DASH_HEADER_SESSION]) . "'" : "") . " ORDER BY RAND() LIMIT 1", true);
$_SESSION[RAINBOW_DASH_HEADER_SESSION] = $rainbowDash["mood"];
?>
<!DOCTYPE html>
<html>
  <head>
    <title>Rainbow Dash</title>
    <meta charset="UTF-8">
    <meta http-equiv="content-type" content="text/html; charset=utf-8">
    <link rel="stylesheet" href="<?php echo WEB_ROOT; ?>/style/stylesheet.css" />
	<link rel="stylesheet" href="<?php echo WEB_ROOT; ?>/style/right-sidebar.css" />
    <link rel="stylesheet" href="<?php echo WEB_ROOT; ?>/style/content.css" />
    <link rel="stylesheet" href="<?php echo WEB_ROOT; ?>/style/datepicker.css" />
    <script src="<?php echo WEB_ROOT; ?>/style/popups.js"></script>
    <script src="<?php echo WEB_ROOT; ?>/style/datepicker.js"></script>
    <script> var WEB_ROOT = '<?php echo WEB_ROOT; ?>';</script>
    <?php
        if ($pageInfo["javascriptFile"] != "")
        {
            echo "    <script src=\"" . WEB_ROOT . "/style/" . $pageInfo["javascriptFile"] . "\"></script>\n";
        }
    ?>
	<base href="<?php echo WEB_ROOT; ?>/"/>
  </head>
  <body>
	<div class="rainbowdash <?php echo $rainbowDash["side"]; ?>">
		<a href="<?php echo WEB_ROOT; ?>/<?php
		if ($pageName != "index")
		{
			echo $pageName . "/";
		}
		if (count($path_pieces) > 0)
		{
			echo implode("/", $path_pieces);
			echo "/";
		}
		?>"><img src="style/rainbow_dashes/<?php echo $rainbowDash["mood"]; ?>.png" /></a>
		<div class="speech"><span><?php echo $rainbowDash["quote"]; ?></span></div>
	</div>
	<div class="container">
		<div class="topbar">
			<a href="<?php echo WEB_ROOT; ?>/">Rainbow.Dash</a>
		</div>
        <?php if($session != null && !$session->resettingPassword()) : ?>
		<div class="sidebar left">
			<div class="block">
				<h1>Meetings</h1>
                <?php
                    pageLink("browse-meetings", "Browse meetings", $pageName);
                    pageLink("create-meeting", "Create new meeting", $pageName);
                    pageLink("search-meetings", "Search meetings", $pageName);
                ?>
			</div>
			<div class="block">
				<h1>Members</h1>
                <?php
                    pageLink("browse-members", "Browse members", $pageName);
                    pageLink("add-member", "Add new member", $pageName);
                    pageLink("search-members", "Search members", $pageName);
                    pageLink("merge-members", "Merge two members", $pageName);
                ?>
			</div>
			<div class="block">
				<h1>Library</h1>
                <?php
                    pageLink("browse-library", "Browse library", $pageName);
                    //pageLink("library-checkout", "Check-out materials", $pageName);
                    pageLink("library-checkin", "Check-in materials", $pageName);
                    pageLink("search-library", "Search library", $pageName);
                    pageLink("checked-out-items", "Check-outs list", $pageName);
                    pageLink("add-library-item", "Add new material", $pageName);
                ?>
			</div>
			<div class="block">
				<h1>Office hours</h1>
                <?php
                    pageLink("office-hours", "View office hours", $pageName);
                    pageLink("assign-office-hour", "Assign office hour", $pageName);
                    pageLink("modify-office-hours", "Modify office hours", $pageName);
                ?>
			</div>
		</div>
		<div class="sidebar right">
			<div class="block user">
				<h1><span><?php
                    echo $session->displayName();
                ?></span></h1>
                <a href="<?php echo WEB_ROOT; ?>/member/<?php echo $session->memberID(); ?>/">View profile</a>
                <?php
                    if ($session->gmail() != null)
                    {
                        echo "<a href=\"http://www.gmail.com/\" target=\"_blank\">View Gmail</a>";
                    }
                    pageLink("change-password", "Change password", $pageName);
                ?>
				<a href="<?php echo WEB_ROOT; ?>/logout/">Logout</a>
			</div>
			<div class="block">
				<h1>Rainbow Office</h1>
                <?php
                    $today = time();
                    $officeOpen = (date("w", $today) != 0 && date("w", $today) != 6);
                    if ($officeOpen && (date("G", $today) < OFFICE_HOURS_BEGIN || date("G", $today) > OFFICE_HOURS_FINISH))
                    {
                        $officeOpen = false;
                    }
                    if ($officeOpen && $database->querySingle("SELECT count(*) FROM semesters WHERE officeHoursStart <= '" .
                        date("Y-m-d", $today) . "' AND officeHoursEnd >= '" . date("Y-m-d", $today) . "'") == 0)
                    {
                        $officeOpen = false;
                    }
                    if ($officeOpen && $database->querySingle("SELECT count(*) FROM officeHolidays WHERE day='" .
                        date("Y-m-d", $today) . "'") > 0)
                    {
                        $officeOpen = false;
                    }
                    echo "<div class=\"status ";
                    echo ($officeOpen ? "open" : "closed");
                    echo "\">The office is <span>";
                    echo ($officeOpen ? "open" : "closed");
                    echo "</span>.</div>\n";
                    
                    if ($officeOpen)
                    {
                        $semesterInfo = getSemester($today);
						$dayEnum = getDayEnum($today);
                        $officeHoursNow = $database->query("SELECT officeHours.member, members.displayName, " .
                            "attended FROM officeHours JOIN members ON members.memberID = officeHours.member LEFT JOIN " .
                            "officeHourAttendance ON officeHours.hourID = officeHourAttendance.officeHour WHERE officeHours.day='" .
                            $dayEnum . "' AND semester='" . $semesterInfo[0] . "' AND schoolYear='" . $semesterInfo[1] .
                            "' AND (effective IS NULL OR effective <= '" . date("Y-m-d", $today) . "') AND (terminates IS NULL OR " .
                            "terminates >= '" . date("Y-m-d", $today) . "') AND officeHours.hour='" . date("G", $today) .
                            "' GROUP BY hourID ORDER BY members.displayName ASC");
                        if ($officeHoursNow->numberRows() > 0)
                        {
                            echo "<div class=\"label\">Scheduled officers:</div>\n";
                            echo "<ul class=\"officeHoursAttendance\">\n";
                            while ($officeHour = $officeHoursNow->fetchArray())
                            {
                                echo "<li class=\"" . ($officeHour["attended"] == "1" ? "attended" : "absent") .
                                    "\"><a href=\"/member/" . $officeHour["member"] . "/\">" . $officeHour["displayName"] . "</a></li>\n";
                            }
                            echo "</ul>\n";
                        }
                        else
                        {
                            echo "<div class=\"label\">No office hours are scheduled for right now.</div>\n";
                        }
						
						$officeHourSignins = $database->query("SELECT hourID, hour, minutes, attended FROM officeHours LEFT JOIN " .
							"officeHourAttendance ON officeHours.hourID = officeHourAttendance.officeHour AND `date`='" .
							date("Y-m-d", $today) . "' WHERE officeHours.member='" .
							$session->memberID() . "' AND day='" . $dayEnum . "' AND hour >= '" . date("H",
							strtotime("+30 minutes", $today)) . "' AND hour <= '" . date("H", strtotime("+3 hours 30 minutes", $today)) .
							"' AND (effective IS NULL OR effective <= '" . date("Y-m-d", $today) . "') AND (terminates IS NULL OR terminates >= '" .
							date("Y-m-d", $today) . "') AND semester='" . $semesterInfo[0] . "' AND schoolYear='" . $semesterInfo[1] .
							"' ORDER BY hour ASC, minutes ASC");
						if ($officeHourSignins !== false && $officeHourSignins->numberRows() > 0)
						{
							$officeHourSignin_StartHour = null;
							$officeHourSignin_StartMinutes = null;
							$officeHourSignin_EndHour = null;
							$officeHourSignin_EndMinutes = null;
							$officeHourSigninHourIDs = array();
							while ($signinHour = $officeHourSignins->fetchArray())
							{
								if ($signinHour["attended"] == 1)
								{
									break;
								}
								if ($officeHourSignin_StartHour == null)
								{
									if ($signinHour["hour"] > date("H", strtotime("+30 minutes", $today)))
									{
										break;
									}
									$officeHourSignin_StartHour = $signinHour["hour"];
									$officeHourSignin_StartMinutes = $signinHour["minutes"];
									$officeHourSignin_EndHour = $signinHour["hour"] + 1;
									$officeHourSignin_EndMinutes = $signinHour["minutes"];
									$officeHourSigninHourIDs[] = $signinHour["hourID"];
								}
								else if ($officeHourSignin_EndHour >= $signinHour["hour"])
								{
									$officeHourSignin_EndHour = $signinHour["hour"] + 1;
									$officeHourSignin_EndMinutes = $signinHour["minutes"];
									$officeHourSigninHourIDs[] = $signinHour["hourID"];
								}
								else
								{
									break;
								}
							}
							
							if (count($officeHourSigninHourIDs) > 0)
							{
								$officeHourSigninHourIDs = array_unique($officeHourSigninHourIDs);
								
								echo "<a href=\"/office-hour-signin/" . implode("/", $officeHourSigninHourIDs) . "/\" class=\"signinButton\">" .
									"Sign-in for<span>";
								$startTwelveMod = $officeHourSignin_StartHour % 12;
								$startPM = ($startTwelveMod != $officeHourSignin_StartHour);
								if ($startTwelveMod == 0)
								{
									echo "Noon";
								}
								else
								{
									echo $startTwelveMod . ":" . $officeHourSignin_StartMinutes . " " . ($startPM ? "PM" : "AM");
								}
								if (count($officeHourSigninHourIDs) > 1)
								{
									echo " &ndash; ";
									$endTwelveMod = $officeHourSignin_EndHour % 12;
									$endPM = ($endTwelveMod != $officeHourSignin_EndHour);
									if ($endTwelveMod == 0)
									{
										echo "Noon";
									}
									else
									{
										echo $endTwelveMod . ":" . $officeHourSignin_EndMinutes . " " . ($endPM ? "PM" : "AM");
									}
								}
								echo "</span></a>\n";
							}
						}
                    }
                ?>
			</div>
			<div class="block">
				<h1>Calendar</h1>
				<table class="calendar"><?php
                    echo "<tr>\n";
                    echo "\t<th colspan=\"7\">";
                    echo date("F Y", $today);
                    echo "</th>\n";
                    echo "</tr>\n";
                    $dateInfo = getdate(mktime(0, 0, 0, date("n", $today), 1, date("Y", $today)));
                    $todaysDay = date("j", $today);
                    $monthLength = date("t", $today);
                    
                    echo "<tr>\n";
                    $cellsOnRow = 0;
                    if ($dateInfo["wday"] > 0)
                    {
                        echo "\t<td class=\"empty\"" . ($dateInfo["wday"] > 1 ? " colspan=\"" . $dateInfo["wday"] .
                            "\"" : "") . "></td>\n";
                        $cellsOnRow = $dateInfo["wday"];
                    }
                    for ($day = 1; $day <= $monthLength; $day++)
                    {
                        if ($cellsOnRow == 7)
                        {
                            echo "</tr>\n";
                            echo "<tr>\n";
                            $cellsOnRow = 0;
                        }
                        echo "\t<td";
                        if ($day == $todaysDay)
                        {
                            echo " class=\"today\"";
                        }
                        echo ">";
                        echo $day;
                        echo "</td>\n";
                        $cellsOnRow++;
                    }
                    
                    if ($cellsOnRow > 0 && $cellsOnRow < 7)
                    {
                        echo "\t<td class=\"empty\"" . (7 - $cellsOnRow > 0 ? " colspan=\"" . (7 - $cellsOnRow) . "\"" : "") .
                            "></td>\n";
                    }
                    
                    echo "</tr>\n";
                ?></table>
			</div>
		</div>
        <?php endif; ?>
		<div class="center">
        <?php
        
            $breadcrumbs = $database->query("SELECT handle, title, breadcrumbLink FROM pages WHERE traversalLeft <= '" . $pageInfo["traversalLeft"] .
                "' AND traversalRight >= '" . $pageInfo["traversalRight"] . "' ORDER BY traversalDepth ASC");
            echo "<div class=\"breadcrumbs\"><a href=\"";
            echo WEB_ROOT;
            echo "/\">[Rainbow.Dash]</a>";
            $breadcrumbArg = "";
            if (count($path_pieces) > 0)
            {
                $breadcrumbArg = $path_pieces[0];
            }
            while ($breadcrumb = $breadcrumbs->fetchArray())
            {
                echo " &rsaquo; <a href=\"";
                echo WEB_ROOT;
                echo "/";
                if ($breadcrumb["breadcrumbLink"] != "")
                {
                    echo str_replace("{{ARG}}", $breadcrumbArg, $breadcrumb["breadcrumbLink"]);
                }
                else
                {
                    echo $breadcrumb["handle"];
                }
                echo "/\">" . $breadcrumb["title"] . "</a>";
            }
            echo "</div>\n";
            if ($report != null && $report->issetArg("error"))
            {
                error($report->arg("error"));
            }
            if ($report != null && $report->issetArg("success"))
            {
                success($report->arg("success"));
            }
			if ($report != null && $report->issetArg("notice"))
			{
				notice($report->arg("notice"));
			}
            
            
            echo "<h1>" . $pageInfo["title"] . "</h1>\n";
            $page->output($session, $database, $path_pieces);
		?></div>
	</div>
    <div class="footer">Designed by <a href="http://jacob.deitloff.com" target="_blank">Jacob Deitloff</a> for the <a href="http://www.pitt.edu/"
        target="_blank">University of Pittsburgh's</a> <a href="http://www.pitt.edu/~sorc/rainbow/" target="_blank">Rainbow Alliance</a>. &copy;2013, aforementioned groups.
        Rainbow Dash (the character) is trademarked and copywritten 2012 by <a href="http://www.hasbro.com/" target="_blank">Hasbro</a>. All rights reserved to them. Individually
        drawn pictures of Rainbow Dash are public contributions of artists on <a href="http://www.deviantart.com/">deviantArt</a>. Pictures used without express permission.</div>
  </body>
</html>
