<?php
if (!isset($_GET["meetingID"]))
{
    exit ("You must provide the meeting ID.");
}

/* setup */
require_once ("/home/deitloff/public_html/rainbow-dash/lib/config.inc.php");
$session = session();
$database = database();
if (mb_strlen($_GET["username"]) == 0)
{
    exit ("You must provide a valid username in order to log in.");
}

if ($database->querySingle("SELECT count(*) FROM members WHERE universityHandle='" .
    $database->escapeString(mb_strtolower($_GET["username"])) . "'") == 0)
{
    exit ("notRegistered");
}

exit ("success");
?>