<?php
/* setup */
require_once ("/home/deitloff/public_html/rainbow-dash/lib/config.inc.php");
$session = session();
$database = database();

if ($database == null)
{
    define("FATAL_ERROR_MESSAGE", "We have encountered a problem while attempting to connect to the database, and we can't connect.");
    require_once (DOCUMENT_ROOT . "/lib/index-majorError.php");
    exit();
}

// logged in officer?
if ($session == null)
{
    header("Location: " . WEB_ROOT . "/error/401/");
    exit();
}

// Determine page
$path_pieces = explode("/", (isset($_GET["path"]) ? trim($_GET["path"], "/\t\n \r") : ""));
if (count($path_pieces) == 0 || mb_strlen($path_pieces[0]) == 0 || ctype_space($path_pieces[0]))
{
    header("Location: " . WEB_ROOT . "/error/403/");
    exit();
}
else
{
    $path_pieces[0] = mb_strtolower($path_pieces[0]);
}
if ($database->querySingle("SELECT count(*) FROM kioskPages WHERE handle LIKE '" .
    $database->escapeString($path_pieces[0]) . "'") == 0)
{
    header("Location: " . WEB_ROOT . "/error/404/");
    exit();
}
$pageInfo = $database->querySingle("SELECT fileName FROM kioskPages WHERE handle='" . $path_pieces[0] .
    "' LIMIT 1", true);
if (dirname(DOCUMENT_ROOT . "/kiosk/" . $pageInfo["fileName"]) != DOCUMENT_ROOT . "/kiosk")
{
    header("Location: " . WEB_ROOT . "/error/500/");
    exit();
}

// Open and run page
require_once (DOCUMENT_ROOT . "/kiosk/" . $pageInfo["fileName"]);
$page = new Page();

if (!$page->preRender($database, $path_pieces))
{
    exit ("error!");
}

echo "<html>";
echo "<head>";
echo "<script> var WEB_ROOT = '" . WEB_ROOT . "';</script>\n";
echo "<script src=\"" . WEB_ROOT . "/style/kiosk.js\"></script>\n";
echo "<link rel=\"stylesheet\" href=\"" . WEB_ROOT . "/style/kiosk.css\">\n";
echo "</head>";
echo "<body>\n";
echo "<div id=\"kiosk\" onclick=\"requestFullScreen();\">\n";
$page->output($database, $path_pieces);
echo "</div>\n";
echo "</body>";
echo "</html>";
?>