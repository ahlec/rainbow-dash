<?php
//phpinfo();
//exit();
error_reporting(E_ALL);
ini_set("display_errors", "1");
ini_set("default_charset", "utf-8");
ini_set("date.timezone", "America/New_York");

define("DOCUMENT_ROOT", dirname(__dir__));
define("WEB_ROOT", "http://www.deitloff.com/rainbow-dash");
define("DATE_FORMAT", "j F Y");
define("WEEKDAY_DATE_FORMAT", "l, j F Y");
define("TIME_FORMAT", "g:i A");
define("DATETIME_FORMAT", "l, j F Y \a\\t g:i A");
define("DAYMONTH_FORMAT", "j F");

require_once ("datetime.inc.php");

define("OFFICE_HOURS_PERCENT_REQUIRED", 75);
define("OFFICE_HOURS_BEGIN", 9);
define("OFFICE_HOURS_FINISH", 18);

define("ISBNDB_ACCESS_KEY", "TJ85PJLN");

/* database */
require_once ("database.inc.php");
function database()
{
    exit ("config.inc.php has not been fully filled out (database())");
    $database = new VersatileDatabase(/* SUPPLY THIS UPON YOUR OWN INSTALLATION */);
    if ($database->valid())
    {
        return $database;
    }
    return null;
}

/* session */
@session_start();
define("AUTHENTICATION_SESSION", "com.deitloff.dash-rainbow.rainbow-authentication");
require_once ("session.inc.php");
function session()
{
    if (!isset($_SESSION[AUTHENTICATION_SESSION]))
    {
        return null;
    }
    $session = new Session(database(), $_SESSION[AUTHENTICATION_SESSION]);
    if ($session->memberID() == null)
    {
        return null;
    }
    return $session;
}

/* Rainbow Dash (header) session */
define ("RAINBOW_DASH_HEADER_SESSION", "com.deitloff.dash-rainbow.rainbow-rainbow-dash");

/* page report */
define("REPORT_SESSION", "com.deitloff.dash-rainbow.rainbow-transaction");
require_once ("pageReport.inc.php");
function report()
{
    return Report::Get();
}

/* page arguments */
require_once ("pageArguments.inc.php");
function arguments($pageName = null, $fullPagePath = null)
{
    return Arguments::Get($pageName, $fullPagePath);
}

/* page elements */
function pageLink($destinationLink, $text, $currentPage)
{
    echo "<a href=\"";
    echo WEB_ROOT;
    echo "/";
    echo $destinationLink;
    echo "/\"";
    if (strpos($destinationLink, $currentPage) === 0)
    {
        echo " class=\"current\"";
    }
    echo ">";
    echo $text;
    echo "</a>";
}
function error($text)
{
    echo "<div class=\"error\" onclick=\"this.style.display='none';\">" . $text . "<span>[ Click to close ]</span></div>\n";
}
function success($text)
{
    echo "<div class=\"success\" onclick=\"this.style.display='none';\">" . $text . "<span>[ Click to close ]</span></div>\n";
}
function notice($text)
{
    echo "<div class=\"notice\" onclick=\"this.style.display='none';\">" . $text . "<span>[ Click to close ]</span></div>\n";
}
?>