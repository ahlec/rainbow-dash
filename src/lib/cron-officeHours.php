<?php
require_once ("config.inc.php");
$database = database();

$today = time();

$todayEnum = null;
switch (date("w", $today))
{
    case 1:
    {
        $todayEnum = "m";
        break;
    }
    case 2:
    {
        $todayEnum = "t";
        break;
    }
    case 3:
    {
        $todayEnum = "w";
        break;
    }
    case 4:
    {
        $todayEnum = "h";
        break;
    }
    case 5:
    {
        $todayEnum = "f";
        break;
    }
}
if ($todayEnum == null)
{
    exit ("Not a weekday (M-F), and so this script doesn't need to run.\n");
}

$rightNow = date("Y-m-d", $today);

$currentSemester = $database->querySingle("SELECT semesterID, semester, schoolYear FROM semesters WHERE officeHoursStart <= '" .
    $rightNow . "' AND officeHoursEnd >= '" . $rightNow . "' LIMIT 1", true);
if ($currentSemester == null || $currentSemester === false)
{
    exit ("Not during a valid semester (or office hours aren't currently recorded), and so this script doesn't need to run.\n");
}

$officeHoursToday = $database->query("SELECT hourID, member FROM officeHours WHERE semester='" . $currentSemester["semester"] .
    "' AND schoolYear='" . $currentSemester["schoolYear"] . "' AND day='" . $todayEnum . "'");

while ($officeHour = $officeHoursToday->fetchArray())
{
    if ($database->querySingle("SELECT count(*) FROM officeHourAttendance WHERE officeHour='" . $officeHour["hourID"] .
        "' AND `date`='" . $rightNow . "'") == 0)
    {
        if (!$database->exec("INSERT INTO officeHourAttendance(officeHour, member, `date`, attended, signedIn) VALUES('" .
            $officeHour["hourID"] . "','" . $officeHour["member"] . "','" . $rightNow . "','0','" .
			date("Y-m-d H:i:s", $today) . "')"))
        {
            trigger_error("Couldn't add an absent officeHourAttendance record for officeHours '" . $officeHour["hourID"] .
                "' for date '" . $rightNow . "'");
        }
    }
}

exit("Finished cron.\n");
?>