<?php
require_once ("config.inc.php");
$database = database();

$today = time();

if (date("w", $today) == 0 || date("w", $today) == 6)
{
    exit("The office isn't open&mdash;it's the weekend! Go have some fun somewhere else!");
}
if (date("G", $today) < OFFICE_HOURS_BEGIN || date("G", $today) > OFFICE_HOURS_FINISH)
{
    exit("The office isn't open right now. You should come back during normal office hours.");
}
if ($database->querySingle("SELECT count(*) FROM semesters WHERE officeHoursStart <= '" .
    date("Y-m-d", $today) . "' AND officeHoursEnd >= '" . date("Y-m-d", $today) . "'") == 0)
{
    exit("Office hours don't begin yet. You don't need to worry about signing in right now.");
}
if ($database->querySingle("SELECT count(*) FROM officeHolidays WHERE day='" . date("Y-m-d", $today) . "'") > 0)
{
    exit("Today is a holiday! The office is closed. So, you don't have your office hours today.");
}

$rightNow = date("Y-m-d", $today);

$currentSemester = $database->querySingle("SELECT semesterID, semester, schoolYear FROM semesters WHERE officeHoursStart <= '" .
    $rightNow . "' AND officeHoursEnd >= '" . $rightNow . "' LIMIT 1", true);
if ($currentSemester == null || $currentSemester === false)
{
    exit ("Not during a valid semester (or office hours aren't currently recorded), and so this script doesn't need to run.\n");
}

$dayEnum = getDayEnum($today);     
$notSignedIn = $database->query("SELECT firstName, displayName, mobile, mobileCarrier, attended, hour, minutes FROM officeHours LEFT JOIN officeHourAttendance " .
    "ON officeHours.hourID = officeHourAttendance.officeHour AND `date`='" . $rightNow . "' JOIN members ON officeHours.member = " .
    "members.memberID WHERE attended IS NULL AND day='" . $database->escapeString($dayEnum) . "' AND semester='" .
    $database->escapeString($currentSemester["semester"]) . "' AND schoolYear='" . $database->escapeString($currentSemester["schoolYear"]) .
    "' AND (effective IS NULL OR effective <='" . $rightNow . "') AND (terminates IS NULL OR terminates >= '" . $rightNow . "') AND hour='" .
    date("H", $today) . "'");

$headers = "From: Rainbow Dash <donotreply@rainbow.deitloff.com>\r\n";
while ($absentee = $notSignedIn->fetchArray())
{
    if ($absentee["mobile"] == null || $absentee["mobileCarrier"] == null)
    {
        continue;
    }
    
    $sms = $absentee["mobile"] . "@";
    $carrierFound = true;
    switch ($absentee["mobileCarrier"])
    {
        case "verizon":
        {
            $sms .= "vtext.com";
            break;
        }
        case "sprint":
        {
            $sms .= "messaging.sprintpcs.com";
            break;
        }
        case "att":
        {
            $sms .= "txt.att.net";
            break;
        }
        case "virgin":
        {
            $sms .= "vmobl.com";
            break;
        }
        case "comcast":
        {
            $sms .= "comcastpcs.textmsg.com";
            break;
        }
        case "boost":
        {
            $sms .= "myboostmobile.com";
            break;
        }
        case "nextel":
        {
            $sms .= "messaging.nextel.com";
            break;
        }
        default:
        {
            $carrierFound = false;
            break;
        }
    }
    
    if (!$carrierFound)
    {
        continue;
    }
    
    $modTwelveHour = $absentee["hour"] % 12;
    if ($modTwelveHour == 0)
    {
        $modTwelveHour = "12";
    }
    
    if (mail($sms, "", "Hey " . $absentee["firstName"] . "! Don't forget to sign in for your " . $modTwelveHour .
        ":00 office hour now on [Rainbow.Dash]!", $headers, "-fdonotreply@rainbow.deitloff.com"))
    {
        echo $absentee["displayName"] . "\n";
    }
}

exit("Finished cron.\n");
?>