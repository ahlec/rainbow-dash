<?php
function getNumberDays($from, $to)
{
    $fromDate = strtotime(date("Y-m-d", $from));
    $toDate = strtotime(date("Y-m-d", $to));
    $difference = $fromDate - $toDate;
    return (date("z", $difference) + 1);
}

function getSemester($time)
{
    $schoolYear = date("Y", $time);
    $semester = "fall";
    if (date("n", $time) < 5)
    {
        $schoolYear--;
        $semester = "spring";
    }
    return array($semester, $schoolYear);
}
function getDayEnum($time)
{
    switch (date("w", $time))
    {
        case 1: return "m";
        case 2: return "t";
        case 3: return "w";
        case 4: return "h";
        case 5: return "f";
        default:
        {
            trigger_error("Day provided does not evaluate to a weekday, and therefore isn't defined in this enum.");
            break;
        }
    }
}
?>