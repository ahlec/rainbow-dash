<!DOCTYPE html>
<html>
  <head>
    <title>Rainbow Dash</title>
    <meta charset="UTF-8">
    <meta http-equiv="content-type" content="text/html; charset=utf-8">
    <link rel="stylesheet" href="<?php echo WEB_ROOT; ?>/style/stylesheet.css" />
    <link rel="stylesheet" href="<?php echo WEB_ROOT; ?>/style/content.css" />
    <script> var WEB_ROOT = '<?php echo WEB_ROOT; ?>';</script>
	<base href="<?php echo WEB_ROOT; ?>/"/>
  </head>
  <body>
	<div class="rainbowdash right">
		<a href="/"><img src="style/rainbow_dashes/shy.png" /></a>
		<div class="speech"><span>Oh dear, I hope I haven't caused this.</span></div>
	</div>
	<div class="container">
		<div class="topbar">
			<a href="/">Rainbow.Dash</a>
		</div>
		<div class="center">
        <?php
            error(FATAL_ERROR_MESSAGE);
		?>
        <h1>Will to live... DELETED</h1>
        <div class="block left">
        [Rainbow.Dash] has encountered a fatal error and absolutely cannot proceed from here. Refreshing the page repeatedly isn't likely
        to resolve <i>this</i> issue. Instead, contacting Jacob and letting him know what's going on has the best chance of solving this
        problem.<br /><br />
        Our apologies!</div>
        <div class="block right">&mdash; [Rainbow.Dash]</div>
        </div>
	</div>
    <div class="footer">Designed by <a href="http://jacob.deitloff.com" target="_blank">Jacob Deitloff</a> for the <a href="http://www.pitt.edu/"
        target="_blank">University of Pittsburgh's</a> <a href="http://www.pitt.edu/~sorc/rainbow/" target="_blank">Rainbow Alliance</a>. &copy;2013, aforementioned groups.
        Rainbow Dash (the character) is trademarked and copywritten 2012 by <a href="http://www.hasbro.com/" target="_blank">Hasbro</a>. All rights reserved to them. Individually
        drawn pictures of Rainbow Dash are public contributions of artists on <a href="http://www.deviantart.com/">deviantArt</a>. Pictures used without express permission.</div>
  </body>
</html>
