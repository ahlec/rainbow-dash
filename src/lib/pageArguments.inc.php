<?php
class Arguments
{
    function __construct($pageName, $fullPagePath)
    {
        global $_SESSION;
        if (isset($_SESSION[REPORT_SESSION]))
        {
            unset($_SESSION[REPORT_SESSION]);
        }
        
        $_SESSION[REPORT_SESSION] = array();
        $_SESSION[REPORT_SESSION]["pageName"] = $pageName;
		$_SESSION[REPORT_SESSION]["pagePath"] = $fullPagePath;
        $_SESSION[REPORT_SESSION]["pageURL"] = "http://" . $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"];
        $_SESSION[REPORT_SESSION]["arguments"] = array();
    }
    
    function setArg($argumentName, $value)
    {
        $_SESSION[REPORT_SESSION]["arguments"][$argumentName] = $value;
    }
    
    static function Get($pageName, $fullPagePath)
    {
        static $saved = null;
        
        if ($saved == null)
        {
            if ($pageName == null)
            {
                trigger_error("Unable to create a new Argument session without a valid pageName.");
                die();
            }
            $saved = new Arguments($pageName, $fullPagePath);
        }
        
        return $saved;
    }
}
?>