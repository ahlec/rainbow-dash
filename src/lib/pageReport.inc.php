<?php
class Report
{
    private $_pageName = null;
	private $_pagePath = null;
    private $_pageURL = null;
    private $_arguments = null;
    
    function __construct()
    {
        global $_SESSION;
        
        if (isset($_SESSION[REPORT_SESSION]))
        {
            $this->_pageName = $_SESSION[REPORT_SESSION]["pageName"];
			$this->_pagePath = $_SESSION[REPORT_SESSION]["pagePath"];
            $this->_pageURL = $_SESSION[REPORT_SESSION]["pageURL"];
            $this->_arguments = $_SESSION[REPORT_SESSION]["arguments"];
            unset ($_SESSION[REPORT_SESSION]);
        }
    }
    
    function pageName()
    {
        return $this->_pageName;
    }
	function pagePath()
	{
		return $this->_pagePath;
	}
    function pageURL()
    {
        return $this->_pageURL;
    }
    function issetArg($argumentName)
    {
        return array_key_exists($argumentName, $this->_arguments);
    }
    function arg($argumentName)
    {
        if (array_key_exists($argumentName, $this->_arguments))
        {
            return $this->_arguments[$argumentName];
        }
        
        trigger_error("Fatal error. Attempted to index an argument that is not set.");
        die();
    }
    
    static function Get()
    {
        static $saved = null;
        static $hasInitialRead = false;
        
        if (!$hasInitialRead)
        {
            $hasInitialRead = true;
            $saved = new Report();
            if ($saved->pageName() == null)
            {
                $saved = null;
            }
        }
        
        return $saved;
    }
}
?>