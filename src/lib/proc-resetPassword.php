<?php
if (!isset($argv) || count($argv) !== 2)
{
    exit ("This script requires one (1) command line argument.\n");
}

require_once ("config.inc.php");
$database = database();

$username = $database->escapeString($argv[1]);
$memberInfo = $database->querySingle("SELECT memberID FROM members WHERE universityHandle='" . $username . "' LIMIT 1");
if ($memberInfo === false)
{
    exit ("Passed a university handle that doesn't exist within the database, or who isn't an officer currently.\n");
}

$loginPermission = new Session($database, $memberInfo);
if ($loginPermission == null || $loginPermission->memberID() == null)
{
    exit ("Passed a university handle that doesn't exist within the database, or who isn't an officer currently.\n");
}

$passwordResetHash = uniqid('', true);
if (!$database->exec("UPDATE members SET passwordResetHash='" . $database->escapeString($passwordResetHash) .
    "', passwordResetRequest='" . date("Y-m-d H:i:s") . "' WHERE memberID='" . $memberInfo . "'"))
{
    exit ("Could not create the password reset hash within the database.\n");
}

$headers = "From: donotreply@rainbow.deitloff.com\r\n" .
    "Reply-To: jacob@deitloff.com\r\n" .
    "X-Mailer: PHP/" . phpversion() . "\r\n" .
    "MIME-Version: 1.0\r\n" .
    "Content-Type: text/html; charset=utf-8\r\n";

$message = "Hey <b>" . $username . "</b>!<br /><br />A request to reset your password has been made from <a href=\"" .
    WEB_ROOT . "\" target=\"_blank\">[Rainbow.Dash]</a>. Hopefully, this was done by you. If so, go to <a href=\"" .
    WEB_ROOT . "/forgot-password/" . $username . "/" . $passwordResetHash . "/\" target=\"_blank\">" . WEB_ROOT .
    "/forgot-password/" . $username . "/" . $passwordResetHash . "/</a> to reset your password.<br /><br />If this request " .
    "wasn't made by you, simply ignore this e-mail, as the next time you log in with your current password, this change " .
    "request will be nullified. It is highly recommended, though, that you change your password when you login again.<br />" .
    "<br />Best,<br />Jacob";
    
mail($username . "@pitt.edu", "[Rainbow.Dash] Reset your password", $message, $headers);
?>