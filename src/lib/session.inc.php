<?php
class Session
{
    private $_memberID = null;
    private $_permissionLevel = null;
    private $_firstName = null;
    private $_lastName = null;
    private $_displayName = null;
    private $_resettingPassword = false;
    private $_positionHandle = null;
    private $_positionEmail = null;
    
    function __construct($database, $memberID)
    {
        if ($memberID == null || ctype_space($memberID))
        {
            return;
        }
        
        $schoolYear = gmdate("Y");
        $semester = "fall";
        if (gmdate("n") < 5)
        {
            $schoolYear--;
            $semester = "spring";
        }
        
        $info = $database->querySingle("SELECT memberID, firstName, lastName, displayName, permissionLevel, email, passwordResetHash FROM " .
            "members JOIN officers ON members.memberID = officers.member JOIN positions ON positions.positionID = officers.position WHERE " .
            "memberID = '" . $database->escapeString($memberID) . "' AND (isPerpetual = '1' OR schoolYear = '" .
            $schoolYear . "' AND semester='" . $semester . "') LIMIT 1", true);
        if ($info === false)
        {
            return;
        }
        
        $this->_memberID = $info["memberID"];
        $this->_firstName = $info["firstName"];
        $this->_lastName = $info["lastName"];
        $this->_displayName = $info["displayName"];
        $this->_permissionLevel = $info["permissionLevel"];
        $this->_email = $info["email"];
        $this->_resettingPassword = ($info["passwordResetHash"] != "");
    }
    
    function memberID()
    {
        return $this->_memberID;
    }
    function permissionLevel()
    {
        return $this->_permissionLevel;
    }
    function firstName()
    {
        return $this->_firstName;
    }
    function lastName()
    {
        return $this->_lastName;
    }
    function displayName()
    {
        return $this->_displayName;
    }
    function resettingPassword()
    {
        return $this->_resettingPassword;
    }
    function gmail()
    {
        return $this->_email;
    }
    function permissions()
    {
        switch ($this->_permissionLevel)
        {
            case "officer":
            {
                return array("officer", "public");
            }
            case "president":
            {
                return array("officer", "public", "president");
            }
        }
        trigger_error("Invalid permission level");
        die();
    }
}
?>