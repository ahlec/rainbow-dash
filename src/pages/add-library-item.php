<?php
class Page
{
    private $_creationAttempted = false;
    private $_failedMessage = "";
    function preRender($database, $session, $arguments)
    {
        global $_POST;
        global $args;
        
        if (isset($_POST["type"]))
        {
            $this->_creationAttempted = true;
            
            $type = $database->escapeString($_POST["type"]);
            
            if (strlen($_POST["isbn"]) > 0 && $_POST["bookInputMode"] == "isbn" && $type == "book")
            {
                $isbn = $database->escapeString($_POST["isbn"]);
				
				if (strlen($isbn) < 10)
				{
					$this->_failedMessage = "Provided ISBN is not long enough to be a valid ISBN. Please recheck and try again.";
					return;
				}
                
                /*require_once (DOCUMENT_ROOT . "/lib/isbn.php");
                if (isbntest13($isbn))
                {
                    $isbnHyphenated = $database->escapeString(addHyphens13($isbn));
                    $isbn = $database->escapeString(str_replace("-", "", $isbn));
                }
                else if (isbntest($isbn))
                {
                    $isbn = $database->escapeString(str_replace("-", "", convertToISBN13($isbn)));
                    $isbnHyphenated = $database->escapeString(addHyphens13($isbn));
                }
                else
                {
                    $this->_failedMessage = "Provided ISBN does not appear to be valid. Please recheck and try again.";
                    return;
                }*/
                
                $xisdb_raw_results = file_get_contents("http://xisbn.worldcat.org/webservices/xid/isbn/" .
                    $isbn . "?method=getMetadata&fl=*&format=json");
                $xisdb_results = json_decode($xisdb_raw_results, true);
                
                if (!is_array($xisdb_results) || $xisdb_results["stat"] != "ok")
                {
                    if ($xisdb_results["stat"] == "invalidId")
                    {
                        $this->_failedMessage = "Provided ISBN does not appear to be valid. Please recheck and try again.";
                        return;
                    }
                    else
                    {
                        $this->_failedMessage = "Provided ISBN has caused problems when attempting to retrieve information. Please recheck and try again.";
                        return;
                    }
                }
                
                if (count($xisdb_results["list"]) != 1)
                {
                    $this->_failedMessage = "Provided ISBN has returned too many results to determine the appropriate book. Please recheck and try again.";
                    return;
                }
                
                $isbnInfo = $xisdb_results["list"][0];
                
                $name = $database->escapeString($isbnInfo["title"]);
                if (array_key_exists("author", $isbnInfo))
                {
                    $author = $database->escapeString($isbnInfo["author"]);
                }
                else if (array_key_exists("publisher", $isbnInfo))
                {
                    $author = $database->escapeString("Published by " . $isbnInfo["publisher"]);
                }
                else
                {
                    $author = null;
                }
                $yearPublished = $database->escapeString($isbnInfo["year"]);
                
                $xisbn_raw_to13 = file_get_contents("http://xisbn.worldcat.org/webservices/xid/isbn/" . $isbn .
                    "?method=to13&format=json");
                $xisbn_to13 = json_decode($xisbn_raw_to13, true);
                
                if (!is_array($xisbn_to13) || $xisbn_to13["stat"] != "ok")
                {
                    $this->_failedMessage = "Provided ISBN failed to evaluate to the uniform storage format. Please try again.";
                    return;
                }
                
                $isbn = $database->escapeString(str_replace("-", "", $xisbn_to13["list"][0]["isbn"][0]));
                
                //var_dump($xisbn_to13["list"]);
                //var_dump($isbn);
                
                $xisbn_raw_hyphen = file_get_contents("http://xisbn.worldcat.org/webservices/xid/isbn/" . $isbn .
                    "?method=hyphen&format=json");
                $xisbn_hyphen = json_decode($xisbn_raw_hyphen, true);
                
                if (!is_array($xisbn_hyphen) || $xisbn_hyphen["stat"] != "ok")
                {
                    $this->_failedMessage = "Provided ISBN failed to evaluate to the uniform display format. Please try again.";
                    return;
                }
                
                $isbnHyphenated = $database->escapeString($xisbn_hyphen["list"][0]["isbn"][0]);
                
				if ($database->querySingle("SELECT count(*) FROM libraryItems WHERE isbn='" . $isbn .
                    "' OR isbnHyphenated='" . $isbnHyphenated . "'") > 0)
				{
					$this->_failedMessage = "The book with this ISBN is <a href=\"/library-item/" . 
						$database->querySingle("SELECT itemID FROM libraryItems WHERE isbn='" . $isbn .
                        "' OR isbnHyphenated='" . $isbnHyphenated . "' LIMIT 1") .
						"/\">already in the database</a>.";
					return;
				}
                
                $monthPublished = null;
				$description = null;
                    
                $isbndb_results = file_get_contents("http://isbndb.com/api/books.xml?access_key=" . ISBNDB_ACCESS_KEY .
                    "&results=texts&index1=isbn&value1=" . $_POST["isbn"]);
                if ($isbndb_results !== false)
                {
                    $xml_parser = xml_parser_create();
                    $isbndb = array();
                    if (xml_parse_into_struct($xml_parser, $isbndb_results, $isbndb) !== 0)
                    {
                    
                        foreach ($isbndb as $tag)
                        {
                            if ($tag["tag"] == "BOOKLIST" && ($tag["type"] == "open" || $tag["type"] == "complete"))
                            {
                                if (!array_key_exists("SHOWN_RESULTS", $tag["attributes"]))
                                {
                                    break;
                                }
                                if ($tag["attributes"]["SHOWN_RESULTS"] != 1)
                                {
                                    break;
                                }
                            }
                            /*else if ($tag["tag"] == "TITLE" && !$titleLong)
                            {
                                $name = $database->escapeString($tag["value"]);
                            }
                            else if ($tag["tag"] == "TITLELONG" && array_key_exists("value", $tag) && strlen($tag["value"]) > 0)
                            {
                                $name = $database->escapeString($tag["value"]);
                                $titleLong = true;
                            }
                            else if ($tag["tag"] == "AUTHORSTEXT" && array_key_exists("value", $tag) && strlen($tag["value"]) > 0)
                            {
                                $author = $database->escapeString($tag["value"]);
                            }
                            else if ($tag["tag"] == "PUBLISHERTEXT" && array_key_exists("value", $tag) && $author == null)
                            {
                                $author = $database->escapeString("Publisher information: " . $tag["value"]);
                            }*/
                            else if ($tag["tag"] == "SUMMARY" && array_key_exists("value", $tag))
                            {
                                $description = $database->escapeString($tag["value"]);
                            }
                        }
                    }
                    xml_parser_free($xml_parser);
                }
                
                /*if ($name == null || $author == null)
                {
                    $this->_failedMessage = "ISBN database results were in an improper format. Please try again.";
                    return;
                }*/
            }
            else
            {
                $name = $database->escapeString($_POST["name"]);
                if ($type == "book")
                {
                    $author = $database->escapeString($_POST["authors"]);
                }
                else
                {
                    $author = "";
                }
                if ($type == "magazine")
                {
                    $yearPublished = $database->escapeString($_POST["yearPublished"]);
                    $monthPublished = $database->escapeString($_POST["monthPublished"]);
                }
                else
                {
                    $yearPublished = null;
                    $monthPublished = null;
                }
                $description = $database->escapeString($_POST["description"]);
				$isbn = "";
                $isbnHyphenated = "";
            }
            $dateAcquired = $database->escapeString($_POST["dateAcquired"]);
            $copies = $database->escapeString($_POST["copies"]);
            
            if ($type != "book" && $type != "DVD" && $type != "VHS" && $type != "magazine" && $type != "CD")
            {
                $this->_failedMessage = "Provided media type is not a proper vaue. Did you even use the form?";
                return;
            }
            
            if (strlen($dateAcquired) > 0)
            {
                $date = strtotime($dateAcquired);
                if ($date == -1 || $date === false)
                {
                    $this->_failedMessage = "Date acquired does not produce a valid date. Did you even use the form?";
                    return;
                }
                $dateAcquired = "'" . date("Y-m-d", $date) . "'";
            }
            else
            {
                $dateAcquired = "NULL";
            }
            
            if (mb_strlen($name) == 0 || ctype_space($name) || $name == null)
            {
                $this->_failedMessage = "Name of the item must be at least one character in length.";
                return;
            }
            
            if ($type == "book" && (mb_strlen($author) == 0 || ctype_space($author) || $author == null))
            {
                $this->_failedMessage = "Authors of this book must be at least one character in length.";
                return;
            }
            
            if (ctype_space($description) || strlen($description) == 0)
            {
                $description = null;
            }
            
            if (!ctype_digit($copies) || $copies <= 0)
            {
                $this->_failedMessage = "Number of copies must be a nonzero, positive integer value.";
                return;
            }
            
            if ($monthPublished != null)
            {
                if (strlen($monthPublished) != 2 || !ctype_digit($monthPublished) || $monthPublished < 1 || $monthPublished > 16)
                {
                    $this->_failedMessage = "Specified publication month must be a valid month. Did you even use the form?";
                    return;
                }
            }
            
            if ($yearPublished != null)
            {
                if (strlen($yearPublished) != 4 || !ctype_digit($yearPublished) || $yearPublished < 1970 || $yearPublished > 2030)
                {
                    $this->_failedMessage = "Specified publication year is not a valid numeric value between 1970&ndash;2030. Please try again.";
                    return;
                }
            }
            
            if ($database->exec("INSERT INTO libraryItems(media, title, authors, description, yearPublished, monthPublished, " .
                "dateAcquired, copies, isbn, isbnHyphenated) VALUES('" . $type . "','" . $name . "','" . $author . "'," .
                ($description == null ? "NULL" : "'" . $description . "'") . "," . ($yearPublished == null ? "NULL" :
                "'" . $yearPublished . "'") . "," . ($monthPublished == null ? "NULL" : "'" . $monthPublished . "'") . "," .
                $dateAcquired . ",'" . $copies . "','" . $isbn . "','" . $isbnHyphenated . "')"))
            {
                $itemID = $database->getLastAutoInc();
                
                $args->setArg("success", "Library item was added successfully. Thank you!");
                header ("Location: " . WEB_ROOT . "/library-item/" . $itemID . "/");
                exit();
            }
            else
            {
                $this->_failedMessage = "Error adding the library item in the database. Please try again.";
            }
        }
    }
    function output($session, $database, $arguments)
    {
        if ($this->_creationAttempted)
        {
            error($this->_failedMessage);
        }
        echo "<hr />\n";
        echo "<form method=\"POST\" action=\"/add-library-item/\">\n";
        echo "\t<label for=\"type\">Media type:</label> <select id=\"type\" name=\"type\" onchange=\"onMediaTypeChanged();\" " .
            "autofocus=\"autofocus\"><option value=\"book\">Book</option>" .
            "<option value=\"DVD\">DVD</option><option value=\"VHS\">VHS tape</option><option value=\"magazine\">Magazine/" .
            "Periodical</option><option value=\"CD\">CD (Audio)</option></select><br />\n";
        echo "<div id=\"bookInputMode-block\" class=\"togetherBlock\"><label for=\"bookInputMode-isbn\">Input method:</label> " .
            "<div class=\"togetherBlock\"><input type=\"radio\" " .
            "id=\"bookInputMode-isbn\" name=\"bookInputMode\" value=\"isbn\" checked=\"checked\" onchange=\"onBookInputModeChanged();\" " .
            "/> <label for=\"bookInputMode-isbn\">ISBN</label><br /><input type=\"radio\" id=\"bookInputMode-manual\" name=\"bookInputMode\" " .
            " value=\"manual\" onchange=\"onBookInputModeChanged();\" /> <label for=\"bookInputMode-manual\">Manual</label></div></div>\n";
        echo "<div id=\"manual-block\" style=\"display:none;\">\n";
        echo "\t<label for=\"name\">Name:</label> <input type=\"text\" id=\"name\" name=\"name\" " .
            "autofocus=\"autofocus\" /><br />\n";
        echo "\t<div id=\"authors-block\" style=\"display:none;\"><label for=\"authors\">Authors:</label> " .
            "<input type=\"text\" id=\"authors\" name=\"authors\" /></div>\n";
        echo "\t<div class=\"textareaBlock\" id=\"description-block\"><label for=\"description\">Description:</label> " .
            "<textarea rows=\"5\" id=\"description\" name=\"description\"></textarea></div>\n";
        echo "</div>\n";
        echo "<div id=\"publication-block\" style=\"display:none;\">\n";
        echo "\t<label for=\"monthPublished\">Publication month:</label> <select id=\"monthPublished\" name=\"monthPublished\"><option value=\"01\">" .
            "January</option><option value=\"02\">February</option><option value=\"03\">March</option><option value=\"04\">April</option>" .
            "<option value=\"05\">May</option><option value=\"06\">June</option><option value=\"07\">July</option><option value=\"08\">" .
            "August</option><option value=\"09\">September</option><option value=\"10\">October</option><option value=\"11\">November" .
            "</option><option value=\"12\">December</option><option value=\"13\">Spring</option><option value=\"14\">Summer" .
            "</option><option value=\"15\">Fall</option><option value=\"16\">Winter</option></select><br />\n";
        echo "\t<label for=\"yearPublished\">Publication year:</label> <input type=\"number\" id=\"yearPublished\" name=\"yearPublished\" " .
            "min=\"1970\" max=\"2030\" /><br />\n";
        echo "</div>\n";
        echo "<div id=\"isbn-block\">\n";
        echo "<label for=\"isbn\">ISBN:</label> <input type=\"text\" id=\"isbn\" name=\"isbn\" /><br />\n";
        echo "</div>\n";
        echo "\t<label for=\"dateAcquired\">Date acquired:</label> " .
            "<input type=\"text\" class=\"dateformat-j-sp-F-sp-Y\" id=\"dateAcquired\" name=\"dateAcquired\" /><br />\n";
        echo "\t<label for=\"copies\">Number of copies:</label> <input type=\"number\" min=\"1\" id=\"copies\" name=\"copies\" value=\"1\" /><br />\n";
        echo "\t<input type=\"submit\" value=\"Add new item\" />\n";
        echo "</form>\n";
    }
}
?>