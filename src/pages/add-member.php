<?php
class Page
{
    private $_creationAttempted = false;
    private $_failedMessage = "";
    function preRender($database, $session, $arguments)
    {
        global $_POST;
        global $args;
        
        if (isset($_POST["displayName"]))
        {
            $this->_creationAttempted = true;
            $university = $database->escapeString($_POST["university"]);
            $universityHandle = $database->escapeString($_POST["universityHandle"]);
            $firstName = $database->escapeString($_POST["firstName"]);
            $lastName = $database->escapeString($_POST["lastName"]);
            $displayName = $database->escapeString($_POST["displayName"]);
            
            if (!ctype_digit($university) || $university != 0 &&
                $database->querySingle("SELECT count(*) FROM universities WHERE universityID='" . $university . "'") == 0)
            {
                $this->_failedMessage = "Provided university is of an invalid value. Did you even use the form?";
                return;
            }
            
            if ($university == 1 && $database->querySingle("SELECT count(*) FROM members WHERE universityHandle='" .
                $universityHandle . "'") > 0)
            {
                $otherMember = $database->querySingle("SELECT memberID FROM members WHERE universityHandle='" .
                    $database->escapeString($universityHandle) . "' LIMIT 1");
                $this->_failedMessage = "Provided UPitt university handle is already registered to <a href=\"/member/" .
                    $otherMember . "/\" target=\"_blank\">another member</a> in the database.";
                return;
            }
            else if ($university != 1)
            {
                $universityHandle = null;
            }
            
            if (mb_strlen($displayName) == 0 || $database->querySingle("SELECT count(*) FROM members WHERE displayName='" .
                $displayName . "'") > 0)
            {
                $this->_failedMessage = "The dispaly name must contain at least one character, and must be unique among " .
                    "all display names in the database.";
                return;
            }
            
            if ($database->exec("INSERT INTO members(firstName, lastName, displayName, university, universityHandle, " .
                "dateCreated) VALUES('" . $firstName . "','" . $lastName . "','" . $displayName . "','" .
                $university . "','" . $universityHandle . "','" . date("Y-m-d H:i:s") . "')"))
            {
                $memberID = $database->getLastAutoInc();
                $args->setArg("success", "Member was added successfully. Thank you!");
                header ("Location: " . WEB_ROOT . "/member/" . $memberID . "/");
                exit();
            }
            else
            {
                $this->_failedMessage = "Error adding the member to the database. Please try again.";
            }
        }
    }
    function output($session, $database, $arguments)
    {
        if ($this->_creationAttempted)
        {
            error($this->_failedMessage);
        }
        echo "<hr />\n";
        echo "<form method=\"POST\" action=\"add-member\">\n";
        echo "\t<label for=\"university\">Attending university:</label> <select id=\"university\" name=\"university\" " .
            "onchange=\"addMemberUniversitySwitched();\" autofocus=\"autofocus\" class=\"followed\">";
        $universities = $database->query("SELECT universityID, name FROM universities");
        while ($university = $universities->fetchArray())
        {
            echo "<option value=\"" . $university["universityID"] . "\">" . $university["name"] . "</option>";
        }
        echo "<option value=\"0\">None</option>";
        echo "</select><input type=\"button\" value=\"Add\" class=\"addButton\" onclick=\"addUniversity();\" /><br />\n";
        echo "\t<div id=\"universityHandle-block\"><label for=\"universityHandle\">UPitt handle:</label> " .
            "<input type=\"text\" id=\"universityHandle\" name=\"universityHandle\" /><br /></div>\n";
        echo "\t<label for=\"firstName\">First name:</label> <input type=\"text\" id=\"firstName\" name=\"firstName\" /><br />\n";
        echo "\t<label for=\"lastName\">Last name:</label> <input type=\"text\" id=\"lastName\" name=\"lastName\" /><br />\n";
        echo "\t<label for=\"displayName\">Display name:</label> <input type=\"text\" id=\"displayName\" name=\"displayName\" /><br />\n";
        echo "\t<input type=\"submit\" value=\"Add member\" />\n";
        echo "</form>\n";
    }
}
?>