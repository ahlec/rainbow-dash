<?php
class Page
{
    private $_creationAttempted = false;
    private $_failedMessage = "";
    
    private $_partTwo = false;
    private $_effective = null;
    private $_terminates = null;
    
    function preRender($database, $session, $arguments)
    {
        global $_POST;
        global $args;
        
        if (isset($_POST["effective"]))
        {
            $this->_effective = (strlen($_POST["effective"]) > 0 ? strtotime($_POST["effective"]) : null);
            $this->_terminates = (strlen($_POST["terminates"]) > 0 ? strtotime($_POST["terminates"]) : null);
            $this->_partTwo = true;
            
            if (isset($_POST["officeHourBlock"]) && strlen($_POST["officeHourBlock"]) > 0)
            {
                $this->_creationAttempted = true;
                
                $member = $database->escapeString($_POST["member"]);
                list($day, $hour) = explode("-", $_POST["officeHourBlock"]);
                $day = $database->escapeString($day);
                $hour = $database->escapeString($hour);
                $semesterInfo = getSemester(time());
                $semester = $database->escapeString($semesterInfo[0]);
                $schoolYear = $database->escapeString($semesterInfo[1]);
                
                if ($day != "m" && $day != "t" && $day != "w" && $day != "h" && $day != "f")
                {
                    $this->_failedMessage = "Day provided is not a valid weekday value. Now I <i>know</i> you didn't use the form.";
                    return;
                }
                
                if (!ctype_digit($hour) || strlen($hour) == 0 || $hour < 9 || $hour > 17)
                {
                    $this->_failedMessage = "Hour provided is not a valid value. Now I <i>know</i> you didn't use the form.";
                    return;
                }
                
                if ($database->querySingle("SELECT count(*) FROM officers JOIN positions ON officers.position = positions.positionID AND " .
                    "officeHourRequirements > '0' WHERE member='" . $member . "' AND semester='" .
                    $semester . "' AND schoolYear='" . $schoolYear . "'") == 0)
                {
                    $this->_failedMessage = "Provided member does not exist, is not an officer for the current semester, or doesn't serve office hours. Now I <i>know</i> you didn't use the form.";
                    return;
                }
                
                if ($database->querySingle("SELECT count(*) FROM officeHours WHERE day='" . $day . "' AND hour='" . $hour .
                    "' AND (terminates IS NULL" . ($this->_effective != null ? " OR terminates >= '" . date("Y-m-d",
                    $this->_effective) . "'" : "") . ") AND (effective IS NULL" . ($this->_terminates != null ?
                    " OR effective <= '" . date("Y-m-d", $this->_terminates) . "'" : "") . ") AND semester='" .
                    $semester . "' AND schoolYear='" . $schoolYear . "'") >= 2)
                {
                    $this->_failedMessage = "There are already two officers assigned to this office hour. No more office hours can be assigned for this time.";
                    return;
                }
                
                if ($database->querySingle("SELECT count(*) FROM officeHours WHERE day='" . $day . "' AND hour='" . $hour .
                    "' AND (terminates IS NULL" . ($this->_effective != null ? " OR terminates >= '" . date("Y-m-d",
                    $this->_effective) . "'" : "") . ") AND (effective IS NULL" . ($this->_terminates != null ?
                    " OR effective <= '" . date("Y-m-d", $this->_terminates) . "'" : "") . ") AND member='" . $member .
                    "' AND semester='" . $semester . "' AND schoolYear='" . $schoolYear . "'") > 0)
                {
                    $this->_failedMessage = "This officer is already assigned for this office hour. Cannot reassign an officer to their own office hour.";
                    return;
                }
                
                if ($database->exec("INSERT INTO officeHours(member, semester, schoolYear, day, hour, effective, terminates) " .
                    "VALUES('" . $member . "','" . $semester . "','" . $schoolYear .
                    "','" . $day . "','" . $hour . "'," . ($this->_effective != null ? "'" . date("Y-m-d", $this->_effective) .
                    "'" : "NULL") . "," . ($this->_terminates != null ? "'" . date("Y-m-d", $this->_terminates) . "'" : "NULL") .
                    ")"))
                {
                    $hourID = $database->getLastAutoInc();
                    $args->setArg("success", "Office hour was successfully assigned. Thanks!");
                    header ("Location: " . WEB_ROOT . "/office-hour/" . $hourID . "/");
                    exit();
                }
                else
                {
                    $this->_failedMessage = "Error assigning the office hour in the database. Please try again.";
                }
            }
        }
    }
    function output($session, $database, $arguments)
    {
        if ($this->_creationAttempted)
        {
            error($this->_failedMessage);
        }
        echo "<hr />\n";
        echo "<form method=\"POST\" action=\"" . WEB_ROOT . "/assign-office-hour/\">\n";
                    
        if ($this->_partTwo)
        {
            echo "\t<label>Effective:</label> <input type=\"hidden\" name=\"effective\" value=\"" . 
                ($this->_effective != null ? date("Y-m-d", $this->_effective) : "") . "\" />" .
                ($this->_effective != null ? date(DATE_FORMAT, $this->_effective) : "Semester start") . "<br />\n";
            echo "\t<label>Ceases:</label> <input type=\"hidden\" name=\"terminates\" value=\"" . 
                ($this->_terminates != null ? date("Y-m-d", $this->_terminates) : "") . "\" />" .
                ($this->_terminates != null ? date(DATE_FORMAT, $this->_terminates) : "Semester end") . "<br />\n";
            echo "\t<label for=\"member\">Officer:</label> <select id=\"member\" name=\"member\">";
            $semesterInfo = getSemester(time());
            $officers = $database->query("SELECT displayName, memberID FROM officers JOIN members ON officers.member = " .
                "members.memberID AND semester='" . $semesterInfo[0] . "' AND schoolYear='" . $semesterInfo[1] .
                "' JOIN positions ON positions.positionID = officers.position AND officeHourRequirements > '0' ORDER BY displayName ASC");
            while ($officer = $officers->fetchArray())
            {
                echo "<option value=\"" . $officer["memberID"] . "\">" . $officer["displayName"] . "</option>\n";
            }
            echo "</select><br />\n";
            
            echo "\t<label>Office hour block:</label><br />\n";
            
            $semester = getSemester(time());
            $officeHourTotals = $database->query("SELECT day, hour, count(*) AS \"assignments\" FROM officeHours WHERE semester='" .
                $database->escapeString($semester[0]) . "' AND schoolYear='" . $database->escapeString($semester[1]) .
                "' AND (terminates IS NULL" . ($this->_effective != null ? " OR terminates >= '" . date("Y-m-d",
                    $this->_effective) . "'" : "") . ") AND (effective IS NULL" . ($this->_terminates != null ?
                    " OR effective <= '" . date("Y-m-d", $this->_terminates) . "'" : "") . ") GROUP BY day, hour ORDER BY hour ASC, day ASC");
            $officeHourTotal = $officeHourTotals->fetchArray();
            
            echo "<table class=\"officeHoursChart small\">\n";
            
            echo "<tr class=\"header\">\n";
            echo "<td class=\"empty\"></td>\n";
            echo "<th>Monday</th>\n";
            echo "<th>Tuesday</th>\n";
            echo "<th>Wednesday</th>\n";
            echo "<th>Thursday</th>\n";
            echo "<th>Friday</th>\n";
            echo "</tr>\n";
            
            for ($hour = 9; $hour <= 17; $hour++)
            {
                echo "<tr>\n";
                
                echo "  <th>";
                $twelveMod = $hour % 12;
                if ($twelveMod == 0)
                {
                    $twelveMod = "Noon";
                }
                else
                {
                    $twelveMod .= ":00";
                }
                $upperMod = ($hour + 1) % 12;
                if ($upperMod == 0)
                {
                    $upperMod = "Noon";
                }
                else
                {
                    $upperMod .= ":00";
                }
                echo $twelveMod;
                echo " &ndash; ";
                echo $upperMod;
                echo "</th>\n";
                
                echo "<td>";
                if ($officeHourTotal !== false && $officeHourTotal["day"] == "m" && $officeHourTotal["hour"] == $hour)
                {
                    if ($officeHourTotal["assignments"] < 2)
                    {
                        echo "<input type=\"radio\" name=\"officeHourBlock\" value=\"m-" . $hour . "\" />";
                    }
                    $officeHourTotal = $officeHourTotals->fetchArray();
                }
                else
                {
                    echo "<input type=\"radio\" name=\"officeHourBlock\" value=\"m-" . $hour . "\" />";
                }
                echo "</td>";
                echo "<td>";
                if ($officeHourTotal !== false && $officeHourTotal["day"] == "t" && $officeHourTotal["hour"] == $hour)
                {
                    if ($officeHourTotal["assignments"] < 2)
                    {
                        echo "<input type=\"radio\" name=\"officeHourBlock\" value=\"t-" . $hour . "\" />";
                    }
                    $officeHourTotal = $officeHourTotals->fetchArray();
                }
                else
                {
                    echo "<input type=\"radio\" name=\"officeHourBlock\" value=\"t-" . $hour . "\" />";
                }
                echo "</td>";
                echo "<td>";
                if ($officeHourTotal !== false && $officeHourTotal["day"] == "w" && $officeHourTotal["hour"] == $hour)
                {
                    if ($officeHourTotal["assignments"] < 2)
                    {
                        echo "<input type=\"radio\" name=\"officeHourBlock\" value=\"w-" . $hour . "\" />";
                    }
                    $officeHourTotal = $officeHourTotals->fetchArray();
                }
                else
                {
                    echo "<input type=\"radio\" name=\"officeHourBlock\" value=\"w-" . $hour . "\" />";
                }
                echo "</td>";
                echo "<td>";
                if ($officeHourTotal !== false && $officeHourTotal["day"] == "h" && $officeHourTotal["hour"] == $hour)
                {
                    if ($officeHourTotal["assignments"] < 2)
                    {
                        echo "<input type=\"radio\" name=\"officeHourBlock\" value=\"h-" . $hour . "\" />";
                    }
                    $officeHourTotal = $officeHourTotals->fetchArray();
                }
                else
                {
                    echo "<input type=\"radio\" name=\"officeHourBlock\" value=\"h-" . $hour . "\" />";
                }
                echo "</td>";
                echo "<td>";
                if ($officeHourTotal !== false && $officeHourTotal["day"] == "f" && $officeHourTotal["hour"] == $hour)
                {
                    if ($officeHourTotal["assignments"] < 2)
                    {
                        echo "<input type=\"radio\" name=\"officeHourBlock\" value=\"f-" . $hour . "\" />";
                    }
                    $officeHourTotal = $officeHourTotals->fetchArray();
                }
                else
                {
                    echo "<input type=\"radio\" name=\"officeHourBlock\" value=\"f-" . $hour . "\" />";
                }
                echo "</td>";
                
                echo "</tr>\n";
            }
            echo "</table>\n";
            
            echo "\t<input type=\"submit\" value=\"Assign office hour\" />\n";
        }
        else
        {
            echo "<div class=\"togetherBlock\"><label for=\"effective\">Effective:</label> <div class=\"togetherBlock\">" .
                "<input type=\"text\" class=\"dateformat-j-sp-F-sp-Y\" id=\"effective\" name=\"effective\" /><br /><i>" .
                "Leave blank to indicate the beginning of the semester</i></div></div>\n";
            echo "<div class=\"togetherBlock\"><label for=\"terminates\">Ceases:</label> <div class=\"togetherBlock\">" .
                "<input type=\"text\" class=\"dateformat-j-sp-F-sp-Y\" id=\"terminates\" name=\"terminates\" /><br /><i>" .
                "Leave blank to indicate the end of the semester</i></div></div>\n";
            echo "\t<input type=\"submit\" value=\"Proceed\" />\n";
        }
        echo "</form>\n";
    }
}
?>