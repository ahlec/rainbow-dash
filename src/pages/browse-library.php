<?php
class Page
{
    private $_mediaTypes = array("book" => "Books", "DVD" => "DVDs", "VHS" => "VHS tapes", "magazine" => "Magazines",
        "CD" => "CDs");
    private $_currentType = "book";
    
    function preRender($database, $session, $arguments)
    {
        if (count($arguments) > 0 && array_key_exists($arguments[0], $this->_mediaTypes))
        {
            $this->_currentType = $arguments[0];
        }
    }
    function output($session, $database, $arguments)
    {
        echo "<hr />\n";
        echo "\t<div class=\"block\">Select the media type to browse.<br />\n";
        $previousType = null;
        $nextType = null;
        $reachedCurrent = false;
        $justReachedCurrent = false;
        foreach ($this->_mediaTypes as $handle => $mediaType)
        {
            if (!$reachedCurrent && $handle == $this->_currentType)
            {
                $reachedCurrent = true;
                $justReachedCurrent = true;
            }
            else if (!$reachedCurrent)
            {
                $previousType = $handle;
            }
            else if ($justReachedCurrent)
            {
                $nextType = $handle;
                $justReachedCurrent = false;
            }
            
            echo "<a href=\"" . WEB_ROOT . "/browse-library/" . $handle . "/\" class=\"browseLibrary";
            if ($handle == $this->_currentType)
            {
                echo " current";
            }
            echo "\">" . $mediaType . "</a>\n";
        }
        echo "</div>\n";
        echo "<script>
        function navigateLibrary(e)
        {
            if (!e)
            {
                var e = window.event;
            }
            ";
            if ($nextType != null)
            {
                echo "if (e.keyCode == 39)
                {
                    window.location = '" . WEB_ROOT . "/browse-library/" . $nextType . "/';
                }";
            }
            if ($previousType != null)
            {
                echo "if (e.keyCode == 37)
                {
                    window.location = '" . WEB_ROOT . "/browse-library/" . $previousType . "/';
                }";
            }
        echo "}
        document.body.onkeydown = navigateLibrary;
        </script>\n";
        echo "<hr />\n";
        
        $libraryItems = $database->query("SELECT itemID, title, copies FROM libraryItems WHERE media='" . $this->_currentType .
            "' ORDER BY title ASC");
        if ($libraryItems === false || $libraryItems->numberRows() == 0)
        {
            echo "<div class=\"block\">No " . $this->_mediaTypes[$this->_currentType] . " have been added to the library.</div>";
            return;
        }
        
        echo "<div class=\"block left\">The following <strong>" . $libraryItems->numberRows() .
            "</strong> titles have been found:</div>\n";
        
        echo "<ul>\n";
        while ($item = $libraryItems->fetchArray())
        {
            echo "\t<li><a href=\"" . WEB_ROOT . "/library-item/" . $item["itemID"] . "/\">" . $item["title"] . "</a>";
            if ($item["copies"] > 1)
            {
                echo " (" . $item["copies"] . ")";
            }
            echo "</li>\n";
        }
        echo "</ul>\n";
    }
}
?>