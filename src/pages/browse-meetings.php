<?php
class Page
{
    function preRender($database, $session, $arguments)
    {
    }
    
    function output($session, $database, $arguments)
    {
        echo "<hr />\n";
        echo "\t<div class=\"block\">Select the school year of the meeting to browse.<br />\n";
        $schoolYears = $database->query("SELECT DISTINCT schoolYear FROM meetings ORDER BY schoolYear ASC");
        $browseYear = date("Y");
        if (date("m") <= 6)
        {
            $browseYear--;
        }
        if (count($arguments) > 0 && strlen($arguments[0]) == 4 && ctype_digit($arguments[0]) &&
            $database->querySingle("SELECT count(*) FROM meetings WHERE schoolYear='" . $database->escapeString($arguments[0]) .
            "'") > 0)
        {
            $browseYear = $database->escapeString($arguments[0]);
        }
        $previousYear = null;
        $nextYear = null;
        $foundCurrentYear = false;
        $justFoundCurrentYear = false;
        
        while ($schoolYear = $schoolYears->fetchArray())
        {
            echo "<a href=\"" . WEB_ROOT . "/browse-meetings/" . $schoolYear["schoolYear"] . "/\" class=\"browseYear";
            if ($schoolYear["schoolYear"] == $browseYear)
            {
                echo " current";
                $foundCurrentYear = true;
                $justFoundCurrentYear = true;
            }
            else if (!$foundCurrentYear || $justFoundCurrentYear)
            {
                if (!$foundCurrentYear)
                {
                    $previousYear = $schoolYear["schoolYear"];
                }
                else if ($justFoundCurrentYear)
                {
                    $nextYear = $schoolYear["schoolYear"];
                    $justFoundCurrentYear = false;
                }
            }
            echo "\">" . $schoolYear["schoolYear"] . "&ndash;" . ($schoolYear["schoolYear"] + 1) . "</a>\n";
        }
        echo "</div>\n";
        echo "<script>
        function navigateYears(e)
        {
            if (!e)
            {
                var e = window.event;
            }
            ";
            if ($nextYear != null)
            {
                echo "if (e.keyCode == 39)
                {
                    window.location = '" . WEB_ROOT . "/browse-meetings/" . $nextYear . "/';
                }";
            }
            if ($previousYear != null)
            {
                echo "if (e.keyCode == 37)
                {
                    window.location = '" . WEB_ROOT . "/browse-meetings/" . $previousYear . "/';
                }";
            }
        echo "}
        document.body.onkeydown = navigateYears;
        </script>\n";
        echo "<hr />\n";
        
        $fallMeetings = $database->query("SELECT meetingID, name, date, time FROM meetings WHERE schoolYear='" .
            $browseYear . "' AND date LIKE '" . $browseYear . "%' ORDER BY date DESC");
        echo "<h3>Fall semester</h3>\n";
        
        if ($fallMeetings === false || $fallMeetings->numberRows() == 0)
        {
            echo "<div class=\"block\">No meetings were found for the fall semester.</div>";
        }
        else
        {
            echo "<div class=\"block left\">The following <strong>" . $fallMeetings->numberRows() . "</strong> meeting" .
                ($fallMeetings->numberRows() != 1 ? "s" : "") . " have been found:</div>\n";
            
            echo "<ul>\n";
            while ($meeting = $fallMeetings->fetchArray())
            {
                echo "\t<li><a href=\"" . WEB_ROOT . "/meeting/" . $meeting["meetingID"] . "/\">" . $meeting["name"] . " <span class=\"browseDate\">" .
                    date(DATE_FORMAT, strtotime($meeting["date"])) . "</span></a></li>\n";
            }
            echo "</ul>\n";
        }
        
        $springMeetings = $database->query("SELECT meetingID, name, date, time FROM meetings WHERE schoolYear='" .
            $browseYear . "' AND date LIKE '" . ($browseYear + 1) . "%' ORDER BY date DESC");
        echo "<h3>Spring semester</h3>\n";
        
        if ($springMeetings === false || $springMeetings->numberRows() == 0)
        {
            echo "<div class=\"block\">No meetings were found for the spring semester.</div>";
        }
        else
        {
            echo "<div class=\"block left\">The following <strong>" . $springMeetings->numberRows() . "</strong> meeting" .
                ($springMeetings->numberRows() != 1 ? "s" : "") . " have been found:</div>\n";
            
            echo "<ul>\n";
            while ($meeting = $springMeetings->fetchArray())
            {
                echo "\t<li><a href=\"" . WEB_ROOT . "/meeting/" . $meeting["meetingID"] . "/\">" . $meeting["name"] . " <span class=\"browseDate\">" .
                    date(DATE_FORMAT, strtotime($meeting["date"])) . "</span></a></li>\n";
            }
            echo "</ul>\n";
        }
    }
}
?>