<?php
class Page
{
    function preRender($database, $session, $arguments)
    {
    }
    function output($session, $database, $arguments)
    {
        echo "<hr />\n";
        echo "\t<div class=\"block\">Select the first letter of the aliases to browse.<br />\n";
        $firstLetter = "A";
        if (count($arguments) > 0 && strlen($arguments[0]) == 1)
        {
            $firstLetter = strtoupper($arguments[0]);
        }
        //$previousLetter = null;
        //$nextLetter = null;
        for ($character = 65; $character <= 90; $character++)
        {
            $char = chr($character);
            echo "<a href=\"" . WEB_ROOT . "/browse-members/" . $char . "/\" class=\"browseLetter";
            if ($char == $firstLetter)
            {
                echo " current";
                /*if ($character > 65)
                {
                    $previousLetter = chr($character - 1);
                }
                if ($character < 90)
                {
                    $nextLetter = chr($character + 1);
                }*/
            }
            echo "\">" . $char . "</a>\n";
        }
        echo "</div>\n";
        echo "<script>
		var currentDestination = " . ord($firstLetter) . ";
        function navigateLetters(e)
        {
            if (!e)
            {
                var e = window.event;
            }
			if (e.keyCode == 39)
			{
				currentDestination++;
				if (currentDestination > 90)
				{
					currentDestination -= 26;
				}
				window.location = '" . WEB_ROOT . "/browse-members/' + String.fromCharCode(currentDestination).toUpperCase() + '/';
			}
			else if (e.keyCode == 37)
			{
				currentDestination--;
				if (currentDestination < 65)
				{
					currentDestination += 26;
				}
				window.location = '" . WEB_ROOT . "/browse-members/' + String.fromCharCode(currentDestination).toUpperCase() + '/';
			}
			else if (e.keyCode >= 65 && e.keyCode <= 90)
			{
				window.location = '" . WEB_ROOT . "/browse-members/' + String.fromCharCode(e.keyCode).toUpperCase() + '/';
			}";
        echo "}
        document.body.onkeydown = navigateLetters;
        </script>\n";
        echo "<hr />\n";
        
        $members = $database->query("SELECT memberID, displayName FROM members WHERE displayName like '" . $firstLetter .
            "%' ORDER BY displayName ASC");
        if ($members === false || $members->numberRows() == 0)
        {
            echo "<div class=\"block\">No members were found that begin with the selected letter.</div>";
            return;
        }
        
        echo "<div class=\"block left\">The following <strong>" . $members->numberRows() . "</strong> member" .
            ($members->numberRows() != 1 ? "s" : "") . " have been found:</div>\n";
        
        echo "<ul>\n";
        while ($member = $members->fetchArray())
        {
            echo "\t<li><a href=\"" . WEB_ROOT . "/member/" . $member["memberID"] . "/\">" . $member["displayName"] . "</a></li>\n";
        }
        echo "</ul>\n";
    }
}
?>