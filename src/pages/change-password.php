<?php
class Page
{
    private $_changeAttempted = false;
    private $_failedMessage = "";
    function preRender($database, $session, $arguments)
    {
        global $_POST;
        global $args;
        
        if ((isset($_POST["currentPassword"]) || $session->resettingPassword()) && isset($_POST["newPassword"]) &&
            isset($_POST["confirmPassword"]))
        {
            $this->_changeAttempted = true;
            $currentPassword = ($session->resettingPassword() ? false : $_POST["currentPassword"]);
            $newPassword = $_POST["newPassword"];
            $confirmPassword = $_POST["confirmPassword"];
            
            if (mb_strlen($newPassword) >= 6 && preg_match("/(.*)([0-9]+)(.*)/", $newPassword) === 1 &&
                preg_match("/(.*)([a-zA-Z]+)(.*)/", $newPassword) === 1)
            {
                if ($newPassword == $confirmPassword)
                {
                    if (!$session->resettingPassword())
                    {
                        $memberInfo = $database->querySingle("SELECT count(*) FROM members WHERE memberID='" .
                            $session->memberID() . "' AND password='" . $database->escapeString(md5($currentPassword)) .
                            "' LIMIT 1");
                    }
                    else
                    {
                        $memberInfo = 1;
                    }
                    if ($memberInfo == 1)
                    {
                        if ($database->exec("UPDATE members SET password='" . $database->escapeString(md5($newPassword)) .
                            "', passwordResetHash = NULL, passwordResetRequest = NULL WHERE memberID='" . $session->memberID() . "'") === true)
                        {
                            $args->setArg("success", "Your password has been successfully changed. Congratulations!");
                            header ("Location: " . WEB_ROOT . "/");
                            exit();
                        }
                        else
                        {
                            $this->_failedMessage = "Could not change password within the database. Please try again.";
                        }
                    }
                    else
                    {
                        $this->_failedMessage = "The supplied current password does not match your current password.";
                    }
                }
                else
                {
                    $this->_failedMessage = "The confirmation password does not match the new password.";
                }
            }
            else
            {
                $this->_failedMessage = "Passwords must be at least six (6) characters in length, with a minimum " .
                    "of one numeric digit and one alphabetical character.";
            }
        }
    }
    function output($session)
    {
        if ($this->_changeAttempted)
        {
            error($this->_failedMessage);
        }
        echo "<hr />\n";
        echo "<div class=\"block left\">\n";
        echo "Passwords must comply with the following rules:\n";
        echo "<ul>\n";
        echo "\t<li>Be at least six (6) characters in length (though preferably more).</li>\n";
        echo "\t<li>Contain (at least) one numeric digit.</li>\n";
        echo "\t<li>Contain (at least) one alphabetic character.</li>\n";
        echo "</ul>\n";
        echo "</div>\n";
        echo "<hr />\n";
        echo "<form method=\"POST\" action=\"change-password\">\n";
        if (!$session->resettingPassword())
        {
            echo "\t<label for=\"currentPassword\">Current password:</label> <input type=\"password\" id=\"currentPassword\" " .
                "name=\"currentPassword\" /><br />\n";
            echo "\t<hr />\n";
        }
        echo "\t<label for=\"newPassword\">New password:</label> <input type=\"password\" id=\"newPassword\" " .
            "name=\"newPassword\" /><br />\n";
        echo "\t<label for=\"confirmPassword\">Confirm:</label> <input type=\"password\" id=\"confirmPassword\" " .
            "name=\"confirmPassword\" /><br />\n";
        echo "\t<input type=\"submit\" value=\"Change password\" />\n";
        echo "</form>\n";
    }
}
?>