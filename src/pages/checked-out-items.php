<?php
class Page
{

    function preRender($database, $session, $arguments)
    {
    }
    function output($session, $database, $arguments)
    {
        $checkedOut = $database->query("SELECT itemID, media, title, displayName, member, checkedOut FROM checkouts JOIN libraryItems " .
            "ON checkouts.item = libraryItems.itemID JOIN members ON members.memberID = checkouts.member WHERE checkedIn IS NULL " .
            "ORDER BY displayName ASC, title ASC");
        if ($checkedOut === false || $checkedOut->numberRows() == 0)
        {
            echo "<div class=\"block\">There are currently no items checked out from the library.</div>\n";
            return;
        }
        
        $currentMember = null;
        while ($record = $checkedOut->fetchArray())
        {
            if ($currentMember !== $record["member"])
            {
                if ($currentMember != null)
                {
                    echo "</ul>\n";
                }
                echo "<h2><a href=\"" . WEB_ROOT . "/member/" . $record["member"] . "/\">" . $record["displayName"] . "</a></h2>\n";
                echo "<ul>\n";
                $currentMember = $record["member"];
            }
            
            echo "<li><a href=\"" . WEB_ROOT . "/library-item/" . $record["itemID"] . "/\">" . $record["title"] . "</a> (" .
                ucfirst($record["media"]) . ". Checked out: " . date(DATE_FORMAT, strtotime($record["checkedOut"])) . ")</li>\n";
        }
        echo "</ul>\n";
    }
}
?>