<?php
class Page
{
    private $_creationAttempted = false;
    private $_failedMessage = "";
    function preRender($database, $session, $arguments)
    {
        global $_POST;
        global $args;
        
        if (isset($_POST["name"]))
        {
            $this->_creationAttempted = true;
            
            $name = $database->escapeString($_POST["name"]);
            $date = $database->escapeString($_POST["date"]);
            $time = $database->escapeString($_POST["time"]);
            $type = $database->escapeString($_POST["type"]);
            $location = $database->escapeString($_POST["location"]);
            $description = $database->escapeString($_POST["description"]);
            $partners = array();
            
            foreach ($_POST as $key => $value)
            {
                if (substr($key, 0, 8) == "partner-")
                {
                    $partnerID = $database->escapeString(substr($key, 8));
                    if ($database->querySingle("SELECT count(*) FROM partnerGroups WHERE groupID='" .
                        $partnerID . "'") == 1)
                    {
                        $partners[] = $partnerID;
                    }
                    else
                    {
                        $this->_failedMessage = "One of the chosen partner groups is not a proper value. Did you even use the form?";
                        return;
                    }
                }
            }
            
            if (!ctype_digit($type) || $database->querySingle("SELECT count(*) FROM meetingTypes WHERE typeID='" .
                $type . "'") != 1)
            {
                $this->_failedMessage = "Provided meeting type is not a proper value. Did you even use the form?";
                return;
            }
            if (!ctype_digit($location) || $database->querySingle("SELECT count(*) FROM locations WHERE locationID='" .
                $location . "'") != 1)
            {
                $this->_failedMessage = "Provided location is not a proper value. Did you even use the form?";
                return;
            }
            
            $datetime = strtotime($time . " " . $date);
            if ($datetime == -1 || $datetime === false)
            {
                $this->_failedMessage = "Date and time provided do not produce a valid combination.";
                return;
            }
            
            if (mb_strlen($name) == 0 || ctype_space($name))
            {
                $this->_failedMessage = "Name of the meeting must be at least one character in length.";
                return;
            }
            
            if (mb_strlen($description) == 0 || ctype_space($description))
            {
                $this->_failedMessage = "Description of the event must be provided.";
                return;
            }
            
            $schoolYear = getSemester($datetime)[1];
            
            if ($database->exec("INSERT INTO meetings(name, date, schoolYear, time, type, location, description) VALUES('" . $name . "','" .
                date("Y-m-d", $datetime) . "','" . $schoolYear . "','" . date("H:i:s", $datetime) . "','" . $type .
                "','" . $location . "','" . $description . "')"))
            {
                $meetingID = $database->getLastAutoInc();
                
                foreach ($partners as $groupID)
                {
                    $database->exec("INSERT INTO partners(meeting, `group`) VALUES(" . $meetingID . "," .
                        $groupID . ")");
                }
                
                $args->setArg("success", "Meeting was created successfully. Thank you!");
                header ("Location: " . WEB_ROOT . "/meeting/" . $meetingID . "/");
                exit();
            }
            else
            {
                $this->_failedMessage = "Error creating the meeting in the database. Please try again.";
            }
        }
    }
    function output($session, $database, $arguments)
    {
        if ($this->_creationAttempted)
        {
            error($this->_failedMessage);
        }
        echo "<hr />\n";
        echo "<form method=\"POST\" action=\"create-meeting\">\n";
        echo "\t<label for=\"name\">Meeting name:</label> <input type=\"text\" id=\"name\" name=\"name\" " .
            "autofocus=\"autofocus\" /><br />\n";
        echo "\t<label for=\"partners\">Partner organizations:</label> <select id=\"partners\" onchange=\"partnerSelectSwitched();\">";
            echo "<option value=\"EMPTY\"></option>\n";
        $partnerGroups = $database->query("SELECT groupID, name FROM partnerGroups ORDER BY name ASC");
        while ($partner = $partnerGroups->fetchArray())
        {
            echo "\t<option value=\"" . $partner["groupID"] . "\">" . $partner["name"] . "</option>\n";
        }
        echo "</select><input type=\"button\" value=\"Add\" class=\"addButton\" onclick=\"addPartnerGroup();\" />" .
            "<div id=\"partners-block\" class=\"formBlock\"></div><br />\n";
        echo "\t<label for=\"date\">Date:</label> <input type=\"text\" class=\"dateformat-j-sp-F-sp-Y\" id=\"date\" name=\"date\" /><br />\n";
        echo "\t<label for=\"time\">Time:</label> <select id=\"time\" name=\"time\">";
        for ($hour = 0; $hour <= 23; $hour++)
        {
            $pm = ($hour >= 12);
            $modTwelveHour = $hour % 12;
            if ($modTwelveHour == 0)
            {
                $modTwelveHour = 12;
            }
            echo "<option>" . $modTwelveHour . ":00 " . ($pm ? "PM" : "AM") . "</option>";
            echo "<option>" . $modTwelveHour . ":15 " . ($pm ? "PM" : "AM") . "</option>";
            echo "<option>" . $modTwelveHour . ":30 " . ($pm ? "PM" : "AM") . "</option>";
            echo "<option" . ($modTwelveHour == 8 && $pm ? " selected=\"selected\"" : "") . ">" . $modTwelveHour . ":45 " .
                ($pm ? "PM" : "AM") . "</option>";
        }
        echo "</select><br />\n";
        echo "\t<label for=\"meetingType\">Meeting type:</label> <select id=\"meetingType\" name=\"type\">";
        $meeting_types = $database->query("SELECT typeID, handle, isLounge FROM meetingTypes ORDER BY isLounge ASC, handle ASC");
        while ($type = $meeting_types->fetchArray())
        {
            echo "<option value=\"" . $type["typeID"] . "\">";
            if ($type["isLounge"] == "1")
            {
                echo "[Lounge] ";
            }
            echo $type["handle"];
            echo "</option>";
        }
        echo "</select><input type=\"button\" value=\"Add\" class=\"addButton\" onclick=\"addMeetingType();\" /><br />\n";
        echo "\t<label for=\"location\">Location:</label> <select id=\"location\" name=\"location\">";
        $locations = $database->query("SELECT locationID, buildings.name, room, capacity FROM locations JOIN buildings " .
            "ON buildings.buildingID = locations.building ORDER BY room, buildings.name ASC");
        while ($location = $locations->fetchArray())
        {
            echo "<option value=\"" . $location["locationID"] . "\">" . $location["room"] . ", " . $location["name"] .
                "</option>";
        }
        echo "</select><input type=\"button\" value=\"Add\" class=\"addButton\" onclick=\"addLocation();\" /><br />\n";
        echo "\t<div class=\"textareaBlock\"><label for=\"description\">Description:</label> " .
            "<textarea rows=\"5\" id=\"description\" name=\"description\"></textarea></div>\n";
        echo "\t<input type=\"submit\" value=\"Create meeting\" />\n";
        echo "</form>\n";
    }
}
?>