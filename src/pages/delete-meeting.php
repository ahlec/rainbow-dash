<?php
class Page
{
    private $_attempted = false;
    private $_failedMessage = "";
    private $_storedDescription = null;
    
    function preRender($database, $session, $arguments)
    {
        global $args;
        
        if (count($arguments) == 0 || $database->querySingle("SELECT count(*) FROM meetings " .
            "WHERE meetingID='" . $database->escapeString($arguments[0]) . "'") != 1)
        {
            $args->setArg("error", "The specified meeting does not exist in our databases. I'm sorry.");
            header ("Location: " . WEB_ROOT . "/browse-meetings/");
            exit();
        }
        
        if (isset($_POST["delete"]))
        {
            $this->_attempted = true;
            
            if ($_POST["delete"] == "no")
            {
                $args->setArg("error", "The meeting was not deleted. False alarm!");
                header ("Location: " . WEB_ROOT . "/meeting/" . $arguments[0] . "/");
                exit();
            }
            
            if ($_POST["delete"] == "yes")
            {
                $meetingInfo = $database->querySingle("SELECT date FROM meetings WHERE meetingID='" .
                    $database->escapeString($arguments[0]) . "' LIMIT 1");
                $semesterInfo = getSemester(strtotime($meetingInfo));
                if ($database->exec("DELETE FROM meetings WHERE meetingID='" . $database->escapeString($arguments[0]) .
                    "'"))
                {
                    if ($database->exec("DELETE FROM attendance WHERE meeting='" . $database->escapeString($arguments[0]) .
                        "'"))
                    {
						$meetingFeedback = $database->query("SELECT feedbackID FROM meetingFeedback WHERE meeting='" .
							$database->escapeString($arguments[0]) . "'");
						while ($feedback = $meetingFeedback->fetchArray())
						{
							$database->exec("DELETE FROM meetingFeedback WHERE feedbackID='" .
								$feedback["feedbackID"] . "'");
							$database->exec("DELETE FROM meetingFeedbackRatings WHERE feedback='" .
								$feedback["feedbackID"] . "'");
						}
                        $args->setArg("success", "Meeting (and attendance records for it) have been deleted. Thanks for keeping me up-to-date!");
                        header ("Location: " . WEB_ROOT . "/browse-meetings/" . $semesterInfo[1] . "/");
                        exit();
                    }
                    else
                    {
                        $args->setArg("error", "Could not remove attendance records for the meeting, but deleted the meeting. Please notify the webmaster at your earliest convenience.");
                        header ("Location: " . WEB_ROOT . "/browse-meetings/" . $semesterInfo[1] . "/");
                        exit();
                    }
                }
                else
                {
                    $this->_failedMessage = "Could not delete the meeting in the database. Please try again.";
                    return;
                }
            }
            
            $this->_failedMessage = "There is no way that you used the form on this one. Try again. This time, with the form.";
            return;
        }
    }
    function output($session, $database, $arguments)
    {
        if ($this->_attempted)
        {
            error($this->_failedMessage);
        }
        
        $meetingInfo = $database->querySingle("SELECT name, date FROM meetings WHERE meetingID='" .
            $database->escapeString($arguments[0]) . "' LIMIT 1", true);
            
        echo "<form method=\"POST\" action=\"delete-meeting/" . $arguments[0] . "/\">\n";
        echo "<label>Meeting:</label> " . $meetingInfo["name"] . " (" . date(DATE_FORMAT, strtotime($meetingInfo["date"])) . ")<br />\n";
        echo "\t<div class=\"togetherBlock\"><label>Delete?</label><div class=\"togetherBlock\"><input type=\"radio\" name=\"delete\" " .
            "id=\"deleteNo\" value=\"no\" autofocus=\"autofocus\" checked=\"checked\" /> <label for=\"deleteNo\">No</label><br /><input type=\"radio\" name=\"delete\" " .
            "id=\"deleteYes\" value=\"yes\" /> <label for=\"deleteYes\">Yes</label></div></div>\n";
        echo "<input type=\"submit\" value=\"Process\" />\n";
        echo "</form>\n";
    }
}
?>