<?php
class Page
{
    private $_editAttempted = false;
    private $_failedMessage = "";
	
    function preRender($database, $session, $arguments)
    {
        global $args;
        
        if (count($arguments) == 0 || $database->querySingle("SELECT count(*) FROM libraryItems " .
            "WHERE itemID='" . $database->escapeString($arguments[0]) . "'") != 1)
        {
            $args->setArg("error", "The specified library item does not exist in our databases. I'm sorry.");
            header ("Location: " . WEB_ROOT . "/browse-library/");
            exit();
        }
        
        if (isset($_POST["title"]))
        {
            $this->_editAttempted = true;
            
            $title = $database->escapeString(htmlspecialchars_decode($_POST["title"]));
			$authors = $database->escapeString(htmlspecialchars_decode($_POST["authors"]));
			$dateAcquired = $database->escapeString($_POST["dateAcquired"]);
			$copies = $database->escapeString($_POST["copies"]);
			
            if (!ctype_digit($copies) || $copies < 1)
            {
                $this->_failedMessage = "Number of copies of this item must be a nonzero, positive integer.";
                return;
            }
			
			if ($copies < $database->querySingle("SELECT count(*) FROM checkouts WHERE item='" . 
				$database->escapeString($arguments[0]) . "' AND checkedIn IS NULL"))
			{
				$this->_failedMessage = "I'm sorry. We cannot reduce the number of copies of an item to a number less than " .
					"the number of copies currently checked out from the library. Documentation reasons. I hope you can understand.";
				return;
			}
            
			if (strlen($dateAcquired) > 0)
			{
				$datestamp = strtotime($date);
				if ($datestamp == -1 || $datestamp === false)
				{
					$this->_failedMessage = "Date of first acquisition is not a valid date. Please check your information.";
					return;
				}
				$dateAcquired = "'" . date("Y-m-d", $datestamp) . "'";
			}
			else
			{
				$dateAcquired = "NULL";
			}
            
            if (mb_strlen($title) == 0 || ctype_space($title))
            {
                $this->_failedMessage = "The title of a library item must be composed of at least one non-whitespace characters.";
                return;
            }
            
            if ($database->exec("UPDATE libraryItems SET title='" . $title . "', dateAcquired=" . $dateAcquired .
				", authors='" . $authors . "', copies='" . $copies . "' WHERE itemID='" .
				$database->escapeString($arguments[0]) . "'"))
            {
                $args->setArg("success", "Changes were successfully made in the database. Thanks!");
                header ("Location: " . WEB_ROOT . "/library-item/" . $arguments[0] . "/");
                exit();
            }
            else
            {
                $this->_failedMessage = "Could not update the item in the database. Please try again.";
            }
        }
    }
    function output($session, $database, $arguments)
    {
        if ($this->_editAttempted)
        {
            error($this->_failedMessage);
        }
        
        $itemInfo = $database->querySingle("SELECT media, title, authors, dateAcquired, copies, isbn, isbnHyphenated " .
			"FROM libraryItems WHERE itemID='" . $database->escapeString($arguments[0]) . "' LIMIT 1", true);
			
        echo "<form method=\"POST\" action=\"edit-library-item/" . $arguments[0] . "/\">\n";
		echo "\t<label>Media:</label> " . ucfirst($itemInfo["media"]) . "<br />\n";
        echo "\t<label for=\"title\">Title:</label> <input type=\"text\" id=\"title\" name=\"title\" " .
            "autofocus=\"autofocus\" value=\"" . htmlspecialchars($itemInfo["title"]) . "\" /><br />\n";
        echo "\t<label for=\"authors\">Authors:</label> <input type=\"text\" id=\"authors\" name=\"authors\" " .
            "value=\"" . htmlspecialchars($itemInfo["authors"]) . "\" /><br />\n";
        echo "\t<label for=\"dateAcquired\">Date first acquired:</label> <input type=\"text\" " .
			"class=\"dateformat-j-sp-F-sp-Y\" id=\"dateAcquired\" name=\"dateAcquired\" value=\"" .
			($itemInfo["dateAcquired"] != null ? date(DATE_FORMAT, strtotime($itemInfo["dateAcquired"])) : "") .
			"\" /><br />\n";
		echo "\t<label for=\"copies\">Number of copies:</label> <input type=\"number\" id=\"copies\" name=\"copies\" " .
			"value=\"" . $itemInfo["copies"] . "\" /><br />\n";
		echo "\t<input type=\"submit\" value=\"Edit " . $itemInfo["media"] . "\" />\n";
        echo "</form>\n";
    }
}
?>