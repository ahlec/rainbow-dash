<?php
class Page
{
    private $_editAttempted = false;
    private $_failedMessage = "";
    function preRender($database, $session, $arguments)
    {
        global $args;
        
        if (count($arguments) == 0 || $database->querySingle("SELECT count(*) FROM meetings " .
            "WHERE meetingID='" . $database->escapeString($arguments[0]) . "'") != 1)
        {
            $args->setArg("error", "The specified meeting does not exist in our databases. I'm sorry.");
            header ("Location: " . WEB_ROOT . "/browse-meetings/");
            exit();
        }
        
        if (isset($_POST["name"]))
        {
            $this->_editAttempted = true;
            
            $name = $database->escapeString(htmlspecialchars_decode($_POST["name"]));
            $date = $database->escapeString($_POST["date"]);
            $time = $database->escapeString($_POST["time"]);
            $type = $database->escapeString($_POST["type"]);
            $location = $database->escapeString($_POST["location"]);
            
            if (!ctype_digit($type) || $database->querySingle("SELECT count(*) FROM meetingTypes WHERE typeID='" .
                $type . "'") != 1)
            {
                $this->_failedMessage = "Provided meeting type is not a proper value. Did you even use the form?";
                return;
            }
            
            if (!ctype_digit($location) || $database->querySingle("SELECT count(*) FROM locations WHERE locationID='" .
                $location . "'") != 1)
            {
                $this->_failedMessage = "Provided location is not a proper value. Did you even use the form?";
                return;
            }
            
            $datetime = strtotime($time . " " . $date);
            if ($datetime == -1 || $datetime === false)
            {
                $this->_failedMessage = "Date and time provided do not produce a valid combination.";
                return;
            }
            
            if (mb_strlen($name) == 0 || ctype_space($name))
            {
                $this->_failedMessage = "Name of the meeting must be at least one character in length.";
                return;
            }
            
            $schoolYear = date("Y", $datetime);
            if (date("m") <= 6)
            {
                $schoolYear--;
            }
            
            if ($database->exec("UPDATE meetings SET name='" . $name . "', `date`='" . date("Y-m-d", $datetime) . "', schoolYear='" .
                $schoolYear . "', `time`='" . date("H:i:s", $datetime) . "', type='" . $type . "', location='" .
                $location . "' WHERE meetingID='" . $database->escapeString($arguments[0]) . "'"))
            {
                $args->setArg("success", "Changes were successfully made in the database. Thanks!");
                header ("Location: " . WEB_ROOT . "/meeting/" . $arguments[0] . "/");
                exit();
            }
            else
            {
                $this->_failedMessage = "Could not update the meeting in the database. Please try again.";
            }
        }
    }
    function output($session, $database, $arguments)
    {
        if ($this->_editAttempted)
        {
            error($this->_failedMessage);
        }
        
        $meetingInfo = $database->querySingle("SELECT name, date, time, type, location FROM meetings WHERE meetingID='" .
            $database->escapeString($arguments[0]) . "' LIMIT 1", true);
        
        echo "<form method=\"POST\" action=\"edit-meeting/" . $arguments[0] . "/\">\n";
        echo "\t<label for=\"name\">Meeting name:</label> <input type=\"text\" id=\"name\" name=\"name\" " .
            "autofocus=\"autofocus\" value=\"" . htmlspecialchars($meetingInfo["name"]) . "\" /><br />\n";
        echo "\t<label for=\"date\">Date:</label> <input type=\"text\" class=\"dateformat-j-sp-F-sp-Y\" id=\"date\" " .
            "name=\"date\" value=\"" . date(DATE_FORMAT, strtotime($meetingInfo["date"])) . "\" /><br />\n";
        echo "\t<label for=\"time\">Time:</label> <select id=\"time\" name=\"time\">";
        $meetingHour = substr($meetingInfo["time"], 0, 2);
        $meetingMinute = substr($meetingInfo["time"], 3, 2);
        for ($hour = 0; $hour <= 23; $hour++)
        {
            $pm = ($hour >= 12);
            $modTwelveHour = $hour % 12;
            if ($modTwelveHour == 0)
            {
                $modTwelveHour = 12;
            }
            echo "<option" . ($meetingHour == $hour && $meetingMinute == 00 ? " selected=\"selected\"" : "") . ">" . $modTwelveHour . ":00 " .
            ($pm ? "PM" : "AM") . "</option>";
            echo "<option" . ($meetingHour == $hour && $meetingMinute == 15 ? " selected=\"selected\"" : "") . ">" . $modTwelveHour . ":15 " .
            ($pm ? "PM" : "AM") . "</option>";
            echo "<option" . ($meetingHour == $hour && $meetingMinute == 30 ? " selected=\"selected\"" : "") . ">" . $modTwelveHour . ":30 " .
            ($pm ? "PM" : "AM") . "</option>";
            echo "<option" . ($meetingHour == $hour && $meetingMinute == 45 ? " selected=\"selected\"" : "") . ">" . $modTwelveHour . ":45 " .
                ($pm ? "PM" : "AM") . "</option>";
        }
        echo "</select><br />\n";
        echo "\t<label for=\"meetingType\">Meeting type:</label> <select id=\"meetingType\" name=\"type\">";
        $meeting_types = $database->query("SELECT typeID, handle, isLounge FROM meetingTypes ORDER BY isLounge ASC, handle ASC");
        while ($type = $meeting_types->fetchArray())
        {
            echo "<option value=\"" . $type["typeID"] . "\"";
            if ($type["typeID"] == $meetingInfo["type"])
            {
                echo " selected=\"selected\"";
            }
            echo ">";
            if ($type["isLounge"] == "1")
            {
                echo "[Lounge] ";
            }
            echo $type["handle"];
            echo "</option>";
        }
        echo "</select><input type=\"button\" value=\"Add\" class=\"addButton\" onclick=\"addMeetingType();\" /><br />\n";
        echo "\t<label for=\"location\">Location:</label> <select id=\"location\" name=\"location\">";
        $locations = $database->query("SELECT locationID, buildings.name, room, capacity FROM locations JOIN buildings " .
            "ON buildings.buildingID = locations.building ORDER BY room, buildings.name ASC");
        while ($location = $locations->fetchArray())
        {
            echo "<option value=\"" . $location["locationID"] . "\"";
            if ($location["locationID"] == $meetingInfo["location"])
            {
                echo " selected=\"selected\"";
            }
            echo ">";
            echo $location["room"];
            echo ", ";
            echo $location["name"];
            echo "</option>";
        }
        echo "</select><input type=\"button\" value=\"Add\" class=\"addButton\" onclick=\"addLocation();\" /><br />\n";
        echo "\t<input type=\"submit\" value=\"Edit meeting\" />\n";
        echo "</form>\n";
    }
}
?>