<?php
class Page
{
    private $_errorNumber;
    private $_path;
    
    function preRender($database, $session, $arguments)
    {
        global $args;
        
        if (count($arguments) == 0)
        {
            $this->_errorNumber = 200;
            $this->_path = array();
        }
        else
        {
            $this->_errorNumber = array_shift($arguments);
            $this->_path = $arguments;
        }
        
        if ($this->_errorNumber == 401)
        {
            $args->setArg("error", "You are not logged in. Please log in to access this page.");            
            header ("Location: " . WEB_ROOT . "/login/");
            exit();
        }
    }
    function output($session)
    {
        echo "ERROR " . $this->_errorNumber . "\n<br /><br />";
        echo "<pre>";
        print_r($this->_path);
        echo "</pre>";
    }
}
?>