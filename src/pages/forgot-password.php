<?php
class Page
{
    private $_resetAttempted = false;
    private $_failedMessage = "";
    function preRender($database, $session, $arguments)
    {
        global $_POST;
        global $args;
        if ($session != null)
        {
            header ("Location: " . WEB_ROOT . "/");
            exit();
        }
        
        if ($arguments != null && count($arguments) == 2)
        {
            $username = $database->escapeString($arguments[0]);
            $resetHash = $database->escapeString($arguments[1]);
            $memberInfo = $database->querySingle("SELECT memberID FROM members WHERE universityHandle='" . 
                $username . "' AND passwordResetHash='" . $resetHash . "' AND passwordResetHash IS NOT NULL LIMIT 1");
            if ($memberInfo !== false)
            {
                $loginPermission = new Session($database, $memberInfo);
                if ($loginPermission != null && $loginPermission->memberID() != null)
                {
                    $args->setArg("success", "Your password reset has been processed. You will need to change your password before you can access Rainbow.Dash.");
                    $_SESSION[AUTHENTICATION_SESSION] = $loginPermission->memberID();
                    header ("Location: " . WEB_ROOT . "/change-password/");
                    exit();
                }
                else
                {
                    $args->setArg("error", "The provided reset information provided is currently invalid.");
                    header ("Location: " . WEB_ROOT . "/login/");
                    exit();
                }
            }
            else
            {
                $args->setArg("error", "The provided reset information provided is currently invalid.");
                header ("Location: " . WEB_ROOT . "/login/");
                exit();
            }
        }
        
        if (isset($_POST["username"]))
        {
            $this->_resetAttempted = true;
            $username = $database->escapeString($_POST["username"]);
            $memberInfo = $database->querySingle("SELECT memberID FROM members WHERE universityHandle='" .
                $username . "' LIMIT 1");
            if ($memberInfo !== false)
            {
                $loginPermission = new Session($database, $memberInfo);
                if ($loginPermission != null && $loginPermission->memberID() != null)
                {
                    system("php " . DOCUMENT_ROOT . "/lib/proc-resetPassword.php " . $username .
                        " > /dev/null &");
                    $args->setArg("success", "An e-mail has been sent to your @pitt.edu account with information on resetting your password.");
                    header ("Location: " . WEB_ROOT . "/login/");
                    exit();
                }
            }
            
            $this->_failedMessage = "The UPitt username provided either does not exist in the database, or the member is " .
                "not currently an officer, and cannot log in.";
        }
    }
    function output($session)
    {
        global $report;
        if ($this->_resetAttempted)
        {
            error($this->_failedMessage);
        }
        echo "<form method=\"POST\" action=\"forgot-password\">\n";
        echo "\t<label for=\"username\">UPitt username:</label> <input type=\"text\" id=\"username\" " .
            "name=\"username\" autofocus=\"autofocus\" /><br />\n";
        echo "\t<input type=\"submit\" value=\"Reset password\" />\n";
        echo "</form>\n";
        echo "<div class=\"block left\">\n";
        echo "<a href=\"" . WEB_ROOT . "/login/\">Oh wait, I remember it now!</a>\n";
        echo "</div>\n";
    }
}
?>