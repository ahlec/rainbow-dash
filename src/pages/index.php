<?php
class Page
{
    function preRender($database, $session, $arguments)
    {
    }
    function output($session)
    {
        echo "<div class=\"block left\">\n";
        echo "<strong>Rainbow.Dash</strong> is a web-based application written entirely from scratch for the <a href=\"http://www.pitt.edu/\" " .
            "target=\"_blank\">University of Pittsburgh's</a> <a href=\"http://www.pitt.edu/~sorc/rainbow/\" target=\"_blank\">Rainbow " .
            "Alliance</a>. Capable of handling the various management needs of the club, it allows the current officers to perform their " .
            "jobs and make decisions with a greater ease, and allows the current team of officers to see records from the past, allowing " .
            "for better judgment calls and more informed decision-making.<br /><br />\n";
        echo "It's also based off of <a href=\"http://mlp.wikia.com/wiki/Rainbow_Dash\" target=\"_blank\">Rainbow Dash</a>, because that " .
            "was suggested to me, and I liked the idea.</div>\n";
    }
}
?>