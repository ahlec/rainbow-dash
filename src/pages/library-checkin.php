<?php
class Page
{
    private $_checkinAttempted = false;
    private $_failedMessage = "";
    function preRender($database, $session, $arguments)
    {
        global $args;
        if (count($_POST) > 0)
        {
            $this->_checkinAttempted = true;
            $checkoutIDs = array();
            foreach ($_POST as $key => $value)
            {
                if ($value !== "yes" || strpos($key, "checkout-") !== 0)
                {
                    continue;
                }
                
                $checkoutID = $database->escapeString(substr($key, 9));
                if ($database->querySingle("SELECT count(*) FROM checkouts WHERE checkoutID='" . $checkoutID .
                    "' AND checkedIn IS NULL") != 0)
                {
                    $checkoutIDs[] = $checkoutID;
                }
                else
                {
                    $this->_failedMessage = "One or more of the received checkout records does not exist, or has already been checked in.";
                    return;
                }
            }
            
            if (count($checkoutIDs) == 0)
            {
                $this->_failedMessage = "At least one item must be selected in order to check a title into the library.";
                return;
            }
            
            $updateWhereClause = "";
            $firstConditional = true;
            foreach ($checkoutIDs as $checkoutID)
            {
                if ($firstConditional)
                {
                    $firstConditional = false;
                }
                else
                {
                    $updateWhereClause .= " OR ";
                }
                $updateWhereClause .= "checkoutID='" . $checkoutID . "'";
            }
            
            if ($database->exec("UPDATE checkouts SET checkedIn = '" . date("Y-m-d") . "' WHERE " . $updateWhereClause))
            {
                $args->setArg("success", "Materials were checked in successfully. Thank you!");
                header ("Location: " . WEB_ROOT . "/checked-out-items/");
                exit();
            }
            else
            {
                $this->_failedMessage = "Checking-in the items in the database caused a problem. Please try again.";
            }
        }
    }
    function output($session, $database, $arguments)
    {
        if ($this->_checkinAttempted)
        {
            error($this->_failedMessage);
        }
        $checkedOut = $database->query("SELECT checkoutID, itemID, media, title, displayName, member, checkedOut FROM checkouts JOIN libraryItems " .
            "ON checkouts.item = libraryItems.itemID JOIN members ON members.memberID = checkouts.member WHERE checkedIn IS NULL " .
            "ORDER BY displayName ASC, title ASC");
        if ($checkedOut === false || $checkedOut->numberRows() == 0)
        {
            echo "<div class=\"block\">There are currently no items checked out from the library.</div>\n";
            return;
        }
        
        echo "<form method=\"POST\" action=\"library-checkin/\">\n";
        
        $currentMember = null;
        while ($record = $checkedOut->fetchArray())
        {
            if ($currentMember !== $record["member"])
            {
                echo "<h2>" . $record["displayName"] . "</h2>\n";
                $currentMember = $record["member"];
            }
            
            echo "<div class=\"checkin\"><input type=\"checkbox\" name=\"checkout-" . $record["checkoutID"] . "\" id=\"checkout-" .
                $record["checkoutID"] . "\" value=\"yes\" /> <label for=\"checkout-" . $record["checkoutID"] .
                "\">" . $record["title"] . " (" .
                ucfirst($record["media"]) . ". Checked out: " . date(DATE_FORMAT, strtotime($record["checkedOut"])) . ")</label></div>\n";
        }
        
        echo "<input type=\"submit\" name=\"checkin-button\" value=\"Check-in selected materials\" />\n";
        
        echo "</form>\n";
    }
}
?>