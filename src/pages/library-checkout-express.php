<?php
class Page
{
    private $_itemID;
    private $_checkoutAttempted = false;
    private $_failedMessage = "";
    function preRender($database, $session, $arguments)
    {
        global $args;
        
        if (count($arguments) == 0 || $database->querySingle("SELECT count(*) FROM libraryItems WHERE itemID='" .
            $database->escapeString($arguments[0]) . "'") == 0)
        {
            $args->setArg("error", "The provided library item does not exist in the database, or you didn't provide one. Did you even use the form?");
            header ("Location: " . WEB_ROOT . "/browse-library/");
            exit();
        }
        
        $this->_itemID = $database->escapeString($arguments[0]);
        $outstandingCheckouts = $database->querySingle("SELECT count(*) FROM checkouts JOIN libraryItems ON " .
            "checkouts.item = libraryItems.itemID WHERE itemID='" . $this->_itemID . "' AND checkedIn IS NULL");
        $numberCopies = $database->querySingle("SELECT copies FROM libraryItems WHERE itemID='" . $this->_itemID . "'");
        
        if ($outstandingCheckouts >= $numberCopies)
        {
            $args->setArg("error", "There are no copies of this item available for check-out at this time.");
            header ("Location: " . WEB_ROOT . "/library-item/" . $this->_itemID . "/");
            exit();
        }
        
        
        if (isset($_POST["itemID"]) && isset($_POST["member"]))
        {
            $this->_checkoutAttempted = true;
            $itemID = $database->escapeString($_POST["itemID"]);
            $member = $database->escapeString($_POST["member"]);
            
            if ($itemID != $this->_itemID)
            {
                $this->_failedMessage = "Mismatched library item. Now I <i>know</i> you didn't use the form for this one.";
                return;
            }
            
            if ($database->querySingle("SELECT count(*) FROM members WHERE memberID='" . $member . "'") == 0)
            {
                $this->_failedMessage = "The selected member does not exist within the database. Did you even use the form?";
                return;
            }
            
            if ($database->exec("INSERT INTO checkouts(item, member, checkedOut) VALUES('" . $itemID . "','" . $member . "','" .
                date("Y-m-d") . "')"))
            {
                $args->setArg("success", "Check-out was performed. Enjoy the " .
                    $database->querySingle("SELECT media FROM libraryItems WHERE itemID='" . $itemID . "' LIMIT 1") . ".");
                header ("Location: " . WEB_ROOT . "/library-item/" . $itemID . "/");
                exit();
            }
            else
            {
                $this->_failedMessage = "Could not check-out the item with the database. Please try again.";
            }
        }
    }
    function output($session, $database, $arguments)
    {
        if ($this->_checkoutAttempted)
        {
            error($this->_failedMessage);
        }
        
        $itemInfo = $database->querySingle("SELECT itemID, title, media, copies FROM libraryItems WHERE itemID='" .
            $this->_itemID . "' LIMIT 1", true);
        
        echo "<form method=\"POST\" action=\"library-checkout-express/" . $this->_itemID . "/\">\n";
        echo "<label>Item:</label> <input type=\"hidden\" name=\"itemID\" value=\"" . $this->_itemID . "\">" .
            $itemInfo["title"] . " (" . ucfirst($itemInfo["media"]) . ")<br />\n";
        echo "<label for=\"member\">Member:</label> <select autofocus=\"autofocus\" id=\"member\" name=\"member\">";
        $members = $database->query("SELECT memberID, displayName FROM members ORDER BY displayName ASC");
        while ($member = $members->fetchArray())
        {
            echo "<option value=\"" . $member["memberID"] . "\">" . $member["displayName"] . "</option>\n";
        }
        echo "</select><br />\n";
        echo "<input type=\"submit\" value=\"Check-out item\" />\n";
        echo "</form>\n";
    }
}
?>