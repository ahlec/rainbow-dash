<?php
class Page
{
    private $_attempted = false;
    private $_failedMessage = "";
    private $_storedDescription = null;
    
    function preRender($database, $session, $arguments)
    {
        global $args;
        
        if (count($arguments) == 0 || $database->querySingle("SELECT count(*) FROM libraryItems " .
            "WHERE itemID='" . $database->escapeString($arguments[0]) . "'") != 1)
        {
            $args->setArg("error", "The specified library item does not exist in our databases. I'm sorry.");
            header ("Location: " . WEB_ROOT . "/browse-library/");
            exit();
        }
        
        if (isset($_POST["description"]))
        {
            $this->_attempted = true;
            $description = $database->escapeString($_POST["description"]);
            
            if (ctype_space($description) || strlen($description) == 0)
            {
				$description = null;
            }
            
            if ($database->exec("UPDATE libraryItems SET description=" . ($description == null ? "NULL" : "'" .
				$description . "'") . " WHERE itemID='" . $database->escapeString($arguments[0]) . "'"))
            {
                $args->setArg("success", "The library item description was modified successfully. Good job!");
                header ("Location: " . WEB_ROOT . "/library-item/" . $arguments[0] . "/");
                exit();
            }
            else
            {
                $this->_storedDescription = $_POST["description"];
                $this->_failedMessage = "There was a problem updating the description in the database. Please try again.";
                return;
            }
        }
    }
    function output($session, $database, $arguments)
    {
        if ($this->_attempted)
        {
            error($this->_failedMessage);
        }
        
        $itemInfo = $database->querySingle("SELECT media, title, description FROM libraryItems WHERE itemID='" .
            $database->escapeString($arguments[0]) . "' LIMIT 1", true);
            
        echo "<form method=\"POST\" action=\"library-item-description/" . $arguments[0] . "/\">\n";
        echo "<label>Media:</label> " . ucfirst($itemInfo["media"]) . "<br />\n";
		echo "<label>Title:</label> " . $itemInfo["title"] . "<br />\n";
        echo "\t<div class=\"textareaBlock\"><label for=\"description\">Description:</label> " .
            "<textarea rows=\"10\" id=\"description\" name=\"description\">";
        if ($this->_storedDescription != null)
        {
            echo $this->_storedDescription;
        }
        else
        {
            echo $itemInfo["description"];
        }
        echo "</textarea></div>\n";
        echo "<input type=\"submit\" value=\"Modify description\" />\n";
        echo "</form>\n";
    }
}
?>