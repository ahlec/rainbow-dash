<?php
class Page
{
    function preRender($database, $session, $arguments)
    {
        global $args;
        
        if (count($arguments) == 0 || $database->querySingle("SELECT count(*) FROM libraryItems " .
            "WHERE itemID='" . $database->escapeString($arguments[0]) . "'") != 1)
        {
            $args->setArg("error", "The specified item does not exist in our databases. I'm sorry.");
            header ("Location: " . WEB_ROOT . "/browse-library/");
            exit();
        }
        
    }
    function output($session, $database, $arguments)
    {
        global $args;
        $itemInfo = $database->querySingle("SELECT itemID, media, title, authors, description, yearPublished, monthPublished, " .
            "dateAcquired, copies, isbnHyphenated FROM libraryItems WHERE itemID='" . $database->escapeString($arguments[0]) . "' LIMIT 1", true);
        $checkedOutCopies = $database->querySingle("SELECT count(*) FROM checkouts WHERE checkedIn IS NULL AND item='" .
            $itemInfo["itemID"] . "'");
        echo "<h2><span>" . $itemInfo["title"] . "</span> <a href=\"" . WEB_ROOT . "/edit-library-item/" . $itemInfo["itemID"] .
			"/\" data-tooltip=\"Edit " . $itemInfo["media"] . "\" class=\"button\"><img src=\"style/modify.png\" /></a>";
		if ($session->permissionLevel() == "president")
        {
            echo " <a href=\"" . WEB_ROOT . "/delete-library-item/" . $itemInfo["itemID"] .
                "/\" data-tooltip=\"Remove all copies of this " . $itemInfo["media"] .
				"\" class=\"button\"><img src=\"style/delete.png\" /></a>";
        }
		echo "</h2>\n";
        echo "<div class=\"block left\">\n";
        echo "<b>Title:</b> " . $itemInfo["title"] . "<br />\n";
        echo "<b>Media:</b> " . ucfirst($itemInfo["media"]) . "<br />\n";
        if ($itemInfo["media"] == "book")
        {
            echo "<b>Author:</b> " . $itemInfo["authors"] . "<br />\n";
            echo "<b>ISBN:</b> " . $itemInfo["isbnHyphenated"] . "<br />\n";
        }
        echo "<b>Published:</b> ";
        if ($itemInfo["yearPublished"] != null)
        {
            if ($itemInfo["monthPublished"] != null)
            {
                $months = array("January", "February", "March", "April", "May", "June", "July", "August", "September", "October",
                    "November", "December", "Spring", "Summer", "Fall", "Winter");
                echo $months[$itemInfo["monthPublished"] - 1] . " ";
            }
            echo $itemInfo["yearPublished"];
        }
        else
        {
            echo "<span class=\"unknownValue\">Unspecified</span>";
        }
        echo "<br />\n";
        echo "<b>Copies available:</b> " . ($itemInfo["copies"] - $checkedOutCopies) . "/" . $itemInfo["copies"] . "<br />\n";
        echo "<b>Date first acquired:</b> ";
        if ($itemInfo["dateAcquired"] != null)
        {
            $dateAcquired = strtotime($itemInfo["dateAcquired"]);
            $semester = getSemester($dateAcquired);
            echo date(DATE_FORMAT, $dateAcquired) . " (" . ucfirst($semester[0]) . " " . $semester[1] . ")";
        }
        else
        {
            echo "<span class=\"unknownValue\">Unspecified</span>";
        }
        echo "<div class=\"buttons\">\n";
        if ($checkedOutCopies < $itemInfo["copies"])
        {
            echo "<a href=\"" . WEB_ROOT . "/library-checkout-express/" . $itemInfo["itemID"] .
                "/\" class=\"checkoutButton\">Check-out a copy</a>\n";
        }
        else
        {
            echo "<div class=\"checkoutButton disabled\">No copies available</div>\n";
        }
        
        if ($checkedOutCopies > 0)
        {
            echo "<a href=\"" . WEB_ROOT . "/library-checkin/\" class=\"checkinButton\">Check-in a copy</a>\n";
        }
        else
        {
            echo "<div class=\"checkinButton disabled\">All copies checked-in</div>\n";
        }
        
        
        echo "</div>\n";
        echo "</div>\n";
        
        echo "<h2>Description <a href=\"" . WEB_ROOT . "/library-item-description/" . $itemInfo["itemID"] .
            "/\" data-tooltip=\"Modify description\" class=\"button\"><img src=\"style/modify.png\" /></a></h2>\n";
		if ($itemInfo["description"] != null)
		{
			echo "<div class=\"block left\">" . $itemInfo["description"] . "</div>\n";
		}
		else
		{
			echo "<div class=\"block\"><span class=\"unknownValue\">None provided.</span></div>\n";
		}
        
        echo "<h2>Checkout log</h2>\n";
        $borrowers = $database->query("SELECT members.memberID, members.displayName, checkedOut, checkedIn FROM " .
            "checkouts JOIN members ON checkouts.member = members.memberID WHERE item='" . $itemInfo["itemID"] .
            "' ORDER BY checkedOut DESC");
        if ($borrowers !== false && $borrowers->numberRows() > 0)
        {
            echo "<ul>\n";
            while ($record = $borrowers->fetchArray())
            {
                echo "\t<li><a href=\"" . WEB_ROOT . "/member/" . $record["memberID"] . "/\">" . $record["displayName"] .
                    "</a> (" . date(DATE_FORMAT, strtotime($record["checkedOut"])) . " &ndash; ";
                if ($record["checkedIn"] != "")
                {
                    echo date(DATE_FORMAT, strtotime($record["checkedIn"]));
                }
                echo ")</li>\n";
            }
            echo "</ul>\n";
        }
        else
        {
            echo "<div class=\"block\">This item does not have any records of being borrowed from the library yet.</div>\n";
        }
    }
}
?>