<?php
class Page
{
    private $_loginAttempted = false;
    private $_failedMessage = "";
    function preRender($database, $session, $arguments)
    {
        global $_POST;
        global $args;
        if ($session != null)
        {
            header ("Location: " . WEB_ROOT . "/");
            exit();
        }
        
        if (isset($_POST["loginUsername"]) && isset($_POST["loginPassword"]))
        {
            $this->_loginAttempted = true;
            $username = $database->escapeString($_POST["loginUsername"]);
            $password = md5($_POST["loginPassword"]);
            $memberInfo = $database->querySingle("SELECT memberID, passwordResetHash FROM members WHERE universityHandle='" .
                $username . "' AND password='" . $password . "' LIMIT 1", true);
            if ($memberInfo !== false)
            {
                $newSession = new Session($database, $memberInfo["memberID"]);
                if ($newSession != null && $newSession->memberID() != null)
                {
                    $args->setArg("success", "You have been successfully logged in. Welcome!");
                    if ($memberInfo["passwordResetHash"] != "")
                    {
                        $database->exec("UPDATE members SET passwordResetHash = NULL, passwordResetRequest = NULL WHERE memberID='" .
                            $memberInfo["memberID"] . "'");
                    }
                    $_SESSION[AUTHENTICATION_SESSION] = $newSession->memberID();
                    header ("Location: " . WEB_ROOT . "/");
                    exit();
                }
                else
                {
                    $this->_failedMessage = "Account is not currently an active officer, and cannot log in at this time.";
                }
            }
            else
            {
                $this->_failedMessage = "Invalid username/password combination.";
            }
        }
    }
    function output($session)
    {
        global $report;
        if ($this->_loginAttempted)
        {
            error($this->_failedMessage);
        }
        echo "<form method=\"POST\" action=\"login\">\n";
        echo "\t<label for=\"loginUsername\">UPitt username:</label> <input type=\"text\" id=\"loginUsername\" " .
            "name=\"loginUsername\" autofocus=\"autofocus\" /><br />\n";
        echo "\t<label for=\"loginPassword\">Password:</label> <input type=\"password\" id=\"loginPassword\" name=\"loginPassword\" /><br />\n";
        echo "\t<input type=\"submit\" value=\"Login\" />\n";
        echo "</form>\n";
        echo "<div class=\"block left\">\n";
        echo "<a href=\"forgot-password/\">Forgot your password?</a>\n";
        echo "</div>\n";
    }
}
?>