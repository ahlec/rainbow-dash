<?php
class Page
{
    function preRender($database, $session, $arguments)
    {
        global $args;
        if (isset($_SESSION[AUTHENTICATION_SESSION]))
        {
            unset($_SESSION[AUTHENTICATION_SESSION]);
            $args->setArg("logoutAttempt", "success");
            header ("Location: " . WEB_ROOT . "/login/");
            exit();
        }
    }
    function output($session)
    {
        error("You should not be able to be on this page. Obviously an error has occurred somewhere... Just, ignore it?");
    }
}
?>