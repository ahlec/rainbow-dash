<?php
class Page
{
    private $_attempted = false;
    private $_failedMessage = "";
    function preRender($database, $session, $arguments)
    {
        global $args;
        
        if (count($arguments) == 0 || $database->querySingle("SELECT count(*) FROM meetings " .
            "WHERE meetingID='" . $database->escapeString($arguments[0]) . "' AND date <= '" .
            date("Y-m-d") . "'") != 1)
        {
            $args->setArg("error", "The specified meeting does not exist in our databases, or has not occurred yet. I'm sorry.");
            header ("Location: " . WEB_ROOT . "/browse-meetings/");
            exit();
        }
        
        if (count($_POST) > 0)
        {
            $this->_attempted = true;
            foreach ($_POST as $key => $value)
            {
                if (substr($key, 0, 7) == "member-")
                {
                    $memberID = $database->escapeString(substr($key, 7));
                    if ($database->querySingle("SELECT count(*) FROM members WHERE memberID='" . $memberID . "'") == 0)
                    {
                        $this->_failedMessage = "One or more of the selected members is not a member in the database. Did you even use the form?";
                        return;
                    }
                    
                    if ($value != "add" && $value != "remove")
                    {
                        $this->_failedMessage = "The data sent was invalid. Now I <i>know</i> you didn't use the form.";
                        return;
                    }
                    
                    if ($value == "add")
                    {
                        if ($database->querySingle("SELECT count(*) FROM attendance WHERE member='" . $memberID . "' AND meeting='" .
                            $database->escapeString($arguments[0]) . "'") == 0)
                        {
                            if ($database->exec("INSERT INTO attendance(member, meeting) VALUES('" . $memberID . "','" .
                                $database->escapeString($arguments[0]) . "')"))
                            {
                                continue;
                            }
                            else
                            {
                                $this->_failedMessage = "One or more of the records could not be added to the database. Please try again.";
                                return;
                            }
                        }
                        else
                        {
                            $this->_failedMessage = "Data mismatch on one or more of the members. Please try again.";
                            return;
                        }
                    }
                    else if ($value == "remove")
                    {
                        if ($database->querySingle("SELECT count(*) FROM attendance WHERE member='" . $memberID . "' AND meeting='" .
                            $database->escapeString($arguments[0]) . "'") > 0)
                        {
                            if ($database->exec("DELETE FROM attendance WHERE member='" . $memberID . "' AND meeting='" .
                                $database->escapeString($arguments[0]) . "'"))
                            {
                                continue;
                            }
                            else
                            {
                                $this->_failedMessage = "One or more of the recordscoule not be removed from the database. Please try again.";
                                return;
                            }
                        }
                        else
                        {
                            $this->_failedMessage = "Data mismatch on one or more of the members. Please try again.";
                            return;
                        }
                    }
                }
                else
                {
                    $this->_failedMessage = "Invalid data was submitted. Now I <i>know</i> you didn't use the form.";
                    return;
                }
            }
            
            $args->setArg("success", "Attendance records were modified like you asked. Thank you!");
            header ("Location: " . WEB_ROOT . "/meeting/" . $arguments[0] . "/");
            exit();
        }
        
    }
    function output($session, $database, $arguments)
    {
        if ($this->_attempted)
        {
            error($this->_failedMessage);
        }
        echo "<h2>Remove attendees</h2>\n";
        $attendees = $database->query("SELECT memberID, displayName FROM members JOIN attendance ON members.memberID = " .
            "attendance.member WHERE meeting='" . $database->escapeString($arguments[0]) . "' ORDER BY displayName ASC");
        if ($attendees !== false && $attendees->numberRows() > 0)
        {
            echo "<form method=\"POST\" action=\"meeting-attendance/" . $arguments[0] . "/\">\n";
            while ($attendee = $attendees->fetchArray())
            {
                echo "<input type=\"checkbox\" value=\"remove\" id=\"member-" . $attendee["memberID"] . "\" name=\"member-" .
                    $attendee["memberID"] . "\" /> <label for=\"member-" . $attendee["memberID"] . "\" class=\"checkbox\">" .
                    $attendee["displayName"] . "</label><br />\n";
            }
            echo "<input type=\"submit\" value=\"Remove selected attendees\" />\n";
            echo "</form>\n";
        }
        else
        {
            echo "<div class=\"block\">There are currently no attendees listed for this meeting.</div>\n";
        }
        
        echo "<h2>Add attendees</h2>\n";
        $members = $database->query("SELECT memberID, displayName, meeting FROM members LEFT JOIN attendance ON members.memberID = " .
            "attendance.member AND meeting = '" . $database->escapeString($arguments[0]) .
            "' WHERE meeting IS NULL ORDER BY displayName ASC");
        if ($members !== false && $members->numberRows() > 0)
        {
            echo "<form method=\"POST\" action=\"meeting-attendance/" . $arguments[0] . "/\">\n";
            while ($member = $members->fetchArray())
            {
                echo "<input type=\"checkbox\" value=\"add\" id=\"member-" . $member["memberID"] . "\" name=\"member-" .
                    $member["memberID"] . "\" /><label for=\"member-" . $member["memberID"] . "\" class=\"checkbox\">" .
                    $member["displayName"] . "</label><br />\n";
            }
            echo "<input type=\"submit\" value=\"Add selected attendees\" />\n";
            echo "</form>\n";
        }
        else
        {
            echo "<div class=\"block\">There are no members in our databases who did not attend this meeting.</div>\n";
        }
    }
}
?>