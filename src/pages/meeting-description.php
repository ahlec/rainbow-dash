<?php
class Page
{
    private $_attempted = false;
    private $_failedMessage = "";
    private $_storedDescription = null;
    
    function preRender($database, $session, $arguments)
    {
        global $args;
        
        if (count($arguments) == 0 || $database->querySingle("SELECT count(*) FROM meetings " .
            "WHERE meetingID='" . $database->escapeString($arguments[0]) . "'") != 1)
        {
            $args->setArg("error", "The specified meeting does not exist in our databases. I'm sorry.");
            header ("Location: " . WEB_ROOT . "/browse-meetings/");
            exit();
        }
        
        if (isset($_POST["description"]))
        {
            $this->_attempted = true;
            $description = $database->escapeString($_POST["description"]);
            
            if (mb_strlen($description) == 0 || ctype_space($description))
            {
                $this->_failedMessage = "Description of the meeting must not be empty.";
                return;
            }
            
            if ($database->exec("UPDATE meetings SET description='" . $description . "' WHERE meetingID='" .
                $database->escapeString($arguments[0]) . "'"))
            {
                $args->setArg("success", "The meeting description was modified successfully. Good job!");
                header ("Location: " . WEB_ROOT . "/meeting/" . $arguments[0] . "/");
                exit();
            }
            else
            {
                $this->_storedDescription = $_POST["description"];
                $this->_failedMessage = "There was a problem updating the description in the database. Please try again.";
                return;
            }
        }
    }
    function output($session, $database, $arguments)
    {
        if ($this->_attempted)
        {
            error($this->_failedMessage);
        }
        
        $meetingInfo = $database->querySingle("SELECT name, date, description FROM meetings WHERE meetingID='" .
            $database->escapeString($arguments[0]) . "' LIMIT 1", true);
            
        echo "<form method=\"POST\" action=\"meeting-description/" . $arguments[0] . "/\">\n";
        echo "<label>Meeting:</label> " . $meetingInfo["name"] . " (" . date(DATE_FORMAT, strtotime($meetingInfo["date"])) . ")<br />\n";
        echo "\t<div class=\"textareaBlock\"><label for=\"description\">Description:</label> " .
            "<textarea rows=\"10\" id=\"description\" name=\"description\">";
        if ($this->_storedDescription != null)
        {
            echo $this->_storedDescription;
        }
        else
        {
            echo $meetingInfo["description"];
        }
        echo "</textarea></div>\n";
        echo "<input type=\"submit\" value=\"Modify description\" />\n";
        echo "</form>\n";
    }
}
?>