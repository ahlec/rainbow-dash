<?php
class Page
{
    private $_attempted = false;
    private $_failedMessage = "";
    function preRender($database, $session, $arguments)
    {
        global $args;
        
        if (count($arguments) == 0 || $database->querySingle("SELECT count(*) FROM meetings " .
            "WHERE meetingID='" . $database->escapeString($arguments[0]) . "' AND date <= '" .
            date("Y-m-d") . "'") != 1)
        {
            $args->setArg("error", "The specified meeting does not exist in our databases, or has not ocurred yet. I'm sorry.");
            header ("Location: " . WEB_ROOT . "/browse-meetings/");
            exit();
        }
        
        $meetingInfo = $database->querySingle("SELECT date FROM meetings WHERE meetingID='" .
            $database->escapeString($arguments[0]) . "' LIMIT 1");
        $meeting = getSemester(strtotime($meetingInfo));
        $now = getSemester(time());
        if ($now[0] != $meeting[0] || $now[1] != $meeting[1] || $database->querySingle("SELECT count(*) FROM officers WHERE " .
            "member='" . $session->memberID() . "' AND semester='" . $now[0] . "' AND schoolYear='" . $now[1] . "'") == 0)
        {
            $args->setArg("error", "You were either not an officer when this meeting occurred, or this meeting occurred in a prior semester. I'm sorry, but you can't modify feedback.");
            header ("Location: " . WEB_ROOT . "/meeting/" . $arguments[0] . "/");
            exit();
        }
        
        if (isset($_POST["rating"]))
        {
            $this->_attempted = true;
            $rating = $database->escapeString($_POST["rating"]);
            $comment = $database->escapeString($_POST["comment"]);
            
            if (!ctype_digit($rating) || $rating < 1 || $rating > 5)
            {
                $this->_failedMessage = "Provided rating is not a proper value. Did you even use the form?";
                return;
            }
            
            if (mb_strlen($comment) == 0 || ctype_space($comment))
            {
                $this->_failedMessage = "Comment must not be empty. Please type something.";
                return;
            }
            
            $feedbackID = $database->querySingle("SELECT feedbackID FROM meetingFeedback WHERE meeting='" .
                $database->escapeString($arguments[0]) . "' AND member='" . $database->escapeString($session->memberID()) .
                "' LIMIT 1");
            if (strlen($feedbackID) > 0)
            {
                if ($database->exec("UPDATE meetingFeedback SET rating='" . $rating . "', comment='" . $comment .
                    "', lastModified='" . date("Y-m-d H:i:s") . "', agrees='0', disagrees='0' WHERE feedbackID='" . $feedbackID . "'"))
                {
                    $database->exec("DELETE FROM meetingFeedbackRatings WHERE feedback='" . $feedbackID . "'");
                    $args->setArg("success", "Your feedback has been modified. Thank you!");
                    header ("Location: " . WEB_ROOT . "/meeting/" . $arguments[0] . "/");
                    exit();
                }
                else
                {
                    $this->_failedMessage = "Could not update your feedback in the database. Please try again.";
                    return;
                }
            }
            else
            {
                if ($database->exec("INSERT INTO meetingFeedback(meeting, member, rating, comment, lastModified) VALUES('" .
                    $database->escapeString($arguments[0]) . "','" . $database->escapeString($session->memberID()) . "','" .
                    $rating . "','" . $comment . "','" . date("Y-m-d H:i:s") . "')"))
                {
                    $args->setArg("success", "Your feedback has been posted. Thank you very much!");
                    header ("Location: " . WEB_ROOT . "/meeting/" . $arguments[0] . "/");
                    exit();
                }
                else
                {
                    $this->_failedMessage = "Could not post your feedback into the database. Please try again.";
                    return;
                }
            }
        }
    }
    function output($session, $database, $arguments)
    {
        if ($this->_attempted)
        {
            error($this->_failedMessage);
        }
        
        $feedback = $database->querySingle("SELECT rating, comment FROM meetingFeedback WHERE member='" .
            $database->escapeString($session->memberID()) . "' AND meeting='" . $database->escapeString($arguments[0]) .
            "' LIMIT 1", true);
            
        if ($feedback !== false)
        {
            notice("Modifying your feedback will remove all agrees and disagrees made by other officers.");
        }
        
        $meetingInfo = $database->querySingle("SELECT name, date, time, type, location FROM meetings WHERE meetingID='" .
            $database->escapeString($arguments[0]) . "' LIMIT 1", true);
        
        echo "<form method=\"POST\" action=\"meeting-feedback/" . $arguments[0] . "/\">\n";
        echo "<label for=\"rating\">Rating:</label> <select id=\"rating\" name=\"rating\">";
        for ($stars = 1; $stars <= 5; $stars++)
        {
            echo "<option value=\"" . $stars . "\"" . ($feedback !== false && $feedback["rating"] == $stars ?
                " selected=\"selected\"" : "") . ">";
            echo str_repeat("&#9733;", $stars);
            echo str_repeat("&#9734;", 5 - $stars);
            echo "</option>";
        }
        echo "</select><br />\n";
        echo "\t<div class=\"textareaBlock\"><label for=\"comment\">Comment:</label> " .
            "<textarea rows=\"10\" id=\"comment\" name=\"comment\">" . ($feedback !== false ? $feedback["comment"] : "") .
            "</textarea></div>\n";
        echo "\t<input type=\"submit\" value=\"Submit feedback\" />\n";
        echo "</form>\n";
    }
}
?>