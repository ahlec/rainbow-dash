<?php
class Page
{
    function preRender($database, $session, $arguments)
    {
        global $args;
        
        if (count($arguments) == 0 || $database->querySingle("SELECT count(*) FROM meetings " .
            "WHERE meetingID='" . $database->escapeString($arguments[0]) . "'") != 1)
        {
            $args->setArg("error", "The specified meeting does not exist in our databases. I'm sorry.");
            header ("Location: " . WEB_ROOT . "/browse-meetings/");
            exit();
        }
        
    }
    function output($session, $database, $arguments)
    {
        $meetingInfo = $database->querySingle("SELECT meetingID, name, date, time, type, location, description  FROM meetings " .
            "WHERE meetingID='" . $database->escapeString($arguments[0]) . "' LIMIT 1", true);
        $meetingTypeInfo = $database->querySingle("SELECT typeID, handle FROM meetingTypes WHERE typeID='" .
            $meetingInfo["type"] . "' LIMIT 1", true);
        $date = strtotime($meetingInfo["date"]);
        $alreadyOccurred = (time() > strtotime($meetingInfo["time"] . " " . $meetingInfo["date"]));
        $semesterData = getSemester($date);
        $schoolYear = $semesterData[1];
        $semester = ucfirst($semesterData[0]);
        echo "<h2><span>" . $meetingInfo["name"] . "</span> <a href=\"" . WEB_ROOT . "/edit-meeting/" . $meetingInfo["meetingID"] .
            "/\" data-tooltip=\"Edit meeting\" class=\"button\"><img src=\"style/modify.png\" /></a>";
        if ($session->permissionLevel() == "president")
        {
            echo " <a href=\"" . WEB_ROOT . "/delete-meeting/" . $meetingInfo["meetingID"] .
                "/\" data-tooltip=\"Delete meeting\" class=\"button\"><img src=\"style/delete.png\" /></a>";
        }
        echo "</h2>\n";
        echo "<div class=\"block left\">\n";
        echo "<b>Name:</b> " . $meetingInfo["name"] . "<br />\n";
        $partners = $database->query("SELECT groupID, name FROM partners JOIN partnerGroups ON partners.group = " .
            "partnerGroups.groupID WHERE meeting='" . $meetingInfo["meetingID"] . "' ORDER BY name ASC");
        if ($partners !== false && $partners->numberRows() > 0)
        {
            echo "<b>Partner organization" . ($partners->numberRows() > 1 ? "s" : "") . ":</b> ";
            $isFirstPartner = true;
            while ($partner = $partners->fetchArray())
            {
                if (!$isFirstPartner)
                {
                    echo ", ";
                }
                else
                {
                    $isFirstPartner = false;
                }
                
                echo $partner["name"];
            }
            echo "<br />\n";
        }
        echo "<b>Meeting type:</b> " . $meetingTypeInfo["handle"] . "<br />\n";
        echo "<b>Date:</b> " . date(WEEKDAY_DATE_FORMAT, $date) . "<br />\n";
        echo "<b>Semester:</b> " . $semester . " " . $schoolYear . "<br />\n";
        echo "<b>Time:</b> " . date(TIME_FORMAT, strtotime($meetingInfo["time"])) . "<br />\n";
        
        $locationInfo = $database->querySingle("SELECT locationID, buildings.name AS \"buildingName\", " .
            "buildings.buildingID AS \"buildingID\", room FROM locations JOIN buildings ON locations.building = " .
            "buildings.buildingID WHERE locationID='" . $meetingInfo["location"] . "' LIMIT 1", true);
        echo "<b>Location:</b> " . $locationInfo["room"] . ", " . $locationInfo["buildingName"] . "<br />\n";
        echo "</div>\n";
        
        echo "<h2>Description <a href=\"" . WEB_ROOT . "/meeting-description/" . $meetingInfo["meetingID"] .
            "/\" data-tooltip=\"Modify description\" class=\"button\"><img src=\"style/modify.png\" /></a></h2>\n";
        echo "<div class=\"block left\">" . $meetingInfo["description"] . "</div>\n";
        
        echo "<h2>Attendance";
        if ($alreadyOccurred)
        {
            echo " <a href=\"" . WEB_ROOT . "/meeting-attendance/" . $meetingInfo["meetingID"] .
            "/\" data-tooltip=\"Manage attendance\" class=\"button\"><img src=\"style/attendance.png\" /></a>";
        }
        echo "</h2>\n";
        if ($alreadyOccurred)
        {
            $attendance = $database->query("SELECT members.memberID, members.displayName AS \"memberName\" FROM attendance " .
                "JOIN members ON attendance.member = members.memberID WHERE meeting='" . $meetingInfo["meetingID"] . "'");
            if ($attendance !== false && $attendance->numberRows() > 0)
            {
                echo "<ul class=\"meetingAttendees\">\n";
                while ($record = $attendance->fetchArray())
                {
                    echo "\t<li><a href=\"" . WEB_ROOT . "/member/" . $record["memberID"] . "/\">" . $record["memberName"] .
                        "</a></li>\n";
                }
                echo "</ul>\n";
            }
            else
            {
                echo "<div class=\"block\">There are currently no records of member attendance for this meeting.</div>\n";
            }
        }
        else
        {
            echo "<div class=\"block\">This meeting has not yet occurred.</div>\n";
        }
        
        echo "<h2>Officer feedback";
        $now = getSemester(time());
        print_r($semester);
        print_r($schoolYear);
        if ($alreadyOccurred && $now[0] == strtolower($semester) && $now[1] == $schoolYear &&
            $database->querySingle("SELECT count(*) FROM officers WHERE member='" . $session->memberID() . "' AND semester='" .
            $now[0] . "' AND schoolYear='" . $now[1] . "'") > 0)
        {
            if ($database->querySingle("SELECT count(*) FROM meetingFeedback WHERE member='" . $database->escapeString($session->memberID()) .
                "' AND meeting='" . $meetingInfo["meetingID"] . "'") == 0)
            {
                echo "<a href=\"" . WEB_ROOT . "/meeting-feedback/" . $meetingInfo["meetingID"] .
                    "/\" data-tooltip=\"Write feedback\" class=\"button\"><img src=\"style/write.png\" /></a>";
            }
            else
            {
                echo "<a href=\"" . WEB_ROOT . "/meeting-feedback/" . $meetingInfo["meetingID"] .
                    "/\" data-tooltip=\"Change your feedback\" class=\"button\"><img src=\"style/modify.png\" /></a>";
                echo "<a href=\"" . WEB_ROOT . "/delete-feedback/" . $meetingInfo["meetingID"] .
                    "/\" data-tooltip=\"Delete your feedback\" class=\"button\"><img src=\"style/delete.png\" /></a>";
            }
        }
        echo "</h2>\n";
        if ($alreadyOccurred)
        {        
            $feedbacks = $database->query("SELECT feedbackID, meetingFeedback.member, displayName, meetingFeedback.rating, comment, lastModified, agrees, " .
                "disagrees, meetingFeedbackRatings.rating AS \"myFeedback\", positionID, positions.name AS \"position\" FROM meetingFeedback JOIN members ON meetingFeedback.member = " .
                "members.memberID JOIN officers ON meetingFeedback.member = officers.member AND officers.schoolYear='" .
                $schoolYear . "' AND officers.semester='" . strtolower($semester) . "' JOIN positions ON officers.position = positions.positionID " .
                "LEFT JOIN meetingFeedbackRatings ON meetingFeedback.feedbackID = meetingFeedbackRatings.feedback AND meetingFeedbackRatings.member='" .
                $session->memberID() . "' WHERE meeting='" . $database->escapeString($meetingInfo["meetingID"]) .
                "' ORDER BY agrees DESC, lastModified DESC");
            if ($feedbacks !== false && $feedbacks->numberRows() > 0)
            {
                while ($feedback = $feedbacks->fetchArray())
                {
                    echo "<div class=\"feedback\">\n";
                    if ($feedback["member"] != $session->memberID())
                    {
                        if ($feedback["myFeedback"] != "")
                        {
                        }
                        else
                        {
                            echo "<div class=\"agreeButton\" id=\"agree-" . $feedback["feedbackID"] . "\" onClick=\"agree('" .
                                $feedback["feedbackID"] . "');\"></div>\n";
                            echo "<div class=\"disagreeButton\" id=\"disagree-" . $feedback["feedbackID"] . "\" onClick=\"disagree('" .
                                $feedback["feedbackID"] . "');\"></div>\n";
                        }
                    }
                    echo "<span class=\"label\">Officer:</span> <a href=\"" . WEB_ROOT . "/member/" . $feedback["member"] . "/\">" .
                        $feedback["displayName"] . "</a> (" . $feedback["position"] . ")<br />\n";
                    echo "<span class=\"label\">Rating:</span> " . str_repeat("<img src=\"style/star.png\" class=\"star\" />",
                        $feedback["rating"]) . str_repeat("<img src=\"style/noStar.png\" class=\"star\" />",
                        5 - $feedback["rating"]) . "<br />\n";
                    echo "<span class=\"label\">Last modified:</span> " . date(DATETIME_FORMAT, strtotime($feedback["lastModified"])) .
                        "<br />\n";
                    echo "<div class=\"comment\">" . $feedback["comment"] . "</div>\n";
                    echo "</div>\n";
                }
            }
            else 
            {
                echo "<div class=\"block\">This meeting has had no feedback.</div>\n";
            }
        }
        else
        {
            echo "<div class=\"block\">This meeting has not yet occurred.</div>\n";
        }
    }
}
?>