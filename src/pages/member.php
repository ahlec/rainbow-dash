<?php
class Page
{
    private $_onSubpage = false;
    private $_allYears = array();
    private $_currentSemester;
    private $_currentSchoolYear;
    private $_semester;
    private $_year;
    private $_errorMessage = null;
    private $_semesterStarts;
    private $_semesterEnds;
    private $_officeHoursStart;
    private $_officeHoursEnd;
    
    function preRender($database, $session, $arguments)
    {
        global $args;
        
        if (count($arguments) == 0 || $database->querySingle("SELECT count(*) FROM members " .
            "WHERE memberID='" . $database->escapeString($arguments[0]) . "'") != 1)
        {
            $args->setArg("error", "The specified member does not exist in our databases. I'm sorry.");
            header ("Location: " . WEB_ROOT . "/browse-members/");
            exit();
        }
        
        $presentSchoolYears = $database->query("SELECT DISTINCT schoolYear FROM attendance JOIN meetings ON meetings.meetingID = " .
            "attendance.meeting WHERE attendance.member='" . $database->escapeString($arguments[0]) . "'");
        while ($presentSchoolYear = $presentSchoolYears->fetchArray())
        {
            $this->_allYears[] = $presentSchoolYear["schoolYear"];
        }
        $this->_year = date("Y");
        $this->_semester = "fall";
        if (date("m") <= 6)
        {
            $this->_year--;
            $this->_semester = "spring";
        }
        
        $this->_currentSemester = $this->_semester;
        $this->_currentSchoolYear = $this->_year;
        if (!in_array($this->_year, $this->_allYears))
        {
            $this->_allYears[] = $this->_year;
        }
        rsort($this->_allYears);
        
        if (count($arguments) > 1)
        {
            $subpageInfo = $arguments[1];
            $subpageSemester = null;
            $subpageYear = null;
            if (strpos($subpageInfo, "fall-") === 0)
            {
                $subpageSemester = "fall";
                $subpageYear = substr($subpageInfo, 5);
            }
            else if (strpos($subpageInfo, "spring-") === 0)
            {
                $subpageSemester = "spring";
                $subpageYear = substr($subpageInfo, 7);
            }
            
            if ($subpageYear != null && strlen($subpageYear) == 4 && ctype_digit($subpageYear))
            {
                if ($database->querySingle("SELECT count(*) FROM meetings WHERE schoolYear='" .
                    $database->escapeString(($subpageSemester == "spring" ? $subpageYear - 1 : $subpageYear)) .
                    "'") > 0)
                {
                    $this->_semester = $subpageSemester;
                    $this->_year = $subpageYear;
                    $this->_onSubpage = true;
                }
                else
                {
                    $this->_errorMessage = "There are no meetings that are scheduled for the target year.";
                }
            }
            else
            {
                $this->_errorMessage = "Invalid format was provided for the target semester/school year combination.";
            }
        }
        
        $semesterInfo = $database->querySingle("SELECT officeHoursStart, officeHoursEnd, starts, ends FROM semesters WHERE semester='" .
            $this->_semester . "' AND schoolYear='" . $this->_year . "' LIMIT 1", true);
        $this->_officeHoursStart = $semesterInfo["officeHoursStart"];
        $this->_officeHoursEnd = $semesterInfo["officeHoursEnd"];
        $this->_semesterStarts = $semesterInfo["starts"];
        $this->_semesterEnds = $semesterInfo["ends"];
    }
    function output($session, $database, $arguments)
    {
        if ($this->_errorMessage != null)
        {
            error($this->_errorMessage);
        }
        $memberInfo = $database->querySingle("SELECT memberID, firstName, lastName, displayName, universityHandle, " .
            "dateCreated FROM members WHERE memberID='" . $database->escapeString($arguments[0]) . "' LIMIT 1", true);
        
        echo "<h2>" . $memberInfo["displayName"] . "</h2>\n";
        echo "<div class=\"block left\">\n";
        echo "<b>Display name:</b> " . $memberInfo["displayName"] . "<br />\n";
        if (!$this->_onSubpage)
        {
            echo "<b>Current legal name:</b> " . $memberInfo["firstName"] . " " . $memberInfo["lastName"] . "<br />\n";
            echo "<b>University handle:</b> " . (strlen($memberInfo["universityHandle"]) > 0 ? $memberInfo["universityHandle"] :
                "<i>(none)</i>") . "<br />\n";
            echo "<b>Date added:</b> " . date(DATETIME_FORMAT, strtotime($memberInfo["dateCreated"])) . "<br />\n";
        }
        echo "</div>\n";
        
        $today = time();
        $attendance = $database->query("SELECT meetingID, name, date, schoolYear, member, isLounge FROM meetings " .
            "JOIN meetingTypes ON meetings.type = meetingTypes.typeID LEFT JOIN attendance ON " .
            "meetings.meetingID = attendance.meeting AND member='" . $memberInfo["memberID"] . "' WHERE schoolYear='" .
            ($this->_semester == "spring" ? $this->_year - 1 : $this->_year) . "' AND date LIKE '" . $this->_year .
            "%' ORDER BY date DESC");
        $totalMeetingsCounted = 0;
        $totalLoungesCounted = 0;
        $totalLoungeAttendance = 0;
        $totalMeetingAttendance = 0;
        $lastWeekCounted = null;
        $thisWeekAttended = false;
        $uniqueWeeksCounted = 0;
        $uniqueWeekAttendance = 0;
        echo "<h2>Attendance: " . ($this->_semester == "fall" ? "Fall" : "Spring") . " " . $this->_year . "</h2>\n";
        if ($attendance !== false && $attendance->numberRows() > 0)
        {
            while ($record = $attendance->fetchArray())
            {
                $meetingDate = strtotime($record["date"]);
                $hasOccurred = ($meetingDate <= $today);
                $attended = ($record["member"] == $memberInfo["memberID"]);
                if ($hasOccurred)
                {
                    $thisWeek = date("W", $meetingDate);
                    if ($thisWeek != $lastWeekCounted)
                    {
                        $uniqueWeeksCounted++;
                        $lastWeekCounted = $thisWeek;
                        if ($attended)
                        {
                            $uniqueWeekAttendance++;
                            $thisWeekAttended = true;
                        }
                        else
                        {
                            $thisWeekAttended = false;
                        }
                    }
                    else if ($thisWeek == $lastWeekCounted && !$thisWeekAttended && $attended)
                    {
                        $uniqueWeekAttendance++;
                        $thisWeekAttended = true;
                    }
                    
                    
                    if ($record["isLounge"] == "1")
                    {
                        $totalLoungesCounted++;
                        if ($attended)
                        {
                            $totalLoungeAttendance++;
                        }
                    }
                    else
                    {
                        $totalMeetingsCounted++;
                        if ($attended)
                        {
                            $totalMeetingAttendance++;
                        }
                    }
                }
                
                echo "\t<a class=\"attendance " . ($hasOccurred ? ($attended ? "attended" : "absent") : "uncounted") . "\" href=\"" .
                    WEB_ROOT . "/meeting/" . $record["meetingID"] . "/\">" . $record["name"] . " <span class=\"date\">(" .
                    date(DATE_FORMAT, $meetingDate) . ")</span></a>\n";
            }
        
            echo "<div class=\"block left attendanceBreakdown\">\n";
            
            echo "<b>General body meetings attended:</b> <sup>";
            echo $totalMeetingAttendance;
            echo "</sup>/<sub>";
            echo $totalMeetingsCounted;
            echo "</sub> (";
            if ($totalMeetingsCounted > 0)
            {
                echo round(($totalMeetingAttendance / $totalMeetingsCounted) * 100);
                echo "%";
            }
            else
            {
                echo " <small><sup>N</sup>/<sub>A</sub></small> ";
            }
            echo ")<br />\n";
            
            echo "<b>Lounges attended:</b> <sup>";
            echo $totalLoungeAttendance;
            echo "</sup>/<sub>";
            echo $totalLoungesCounted;
            echo "</sub> (";
            if ($totalLoungesCounted > 0)
            {
                echo round(($totalLoungeAttendance / $totalLoungesCounted) * 100);
                echo "%";
            }
            else
            {
                echo " <small><sup>N</sup>/<sub>A</sub></small> ";
            }
            echo ")<br />\n";
            
            echo "<b>Unique weeks attended:</b> <sup>";
            echo $uniqueWeekAttendance;
            echo "</sup>/<sub>";
            echo $uniqueWeeksCounted;
            echo "</sub> (";
            if ($uniqueWeeksCounted > 0)
            {
                echo round(($uniqueWeekAttendance / $uniqueWeeksCounted) * 100);
                echo "%";
            }
            else
            {
                echo " <small><sup>N</sup>/<sub>A</sub></small> ";
            }
            echo ")<br />\n";
            
            echo "<b>Total attendance:</b> <sup>";
            echo ($totalLoungeAttendance + $totalMeetingAttendance);
            echo "</sup>/<sub>";
            echo ($totalLoungesCounted + $totalMeetingsCounted);
            echo "</sub> (";
            if ($totalLoungesCounted + $totalMeetingsCounted > 0)
            {
                echo round((($totalLoungeAttendance + $totalMeetingAttendance) /
                    ($totalLoungesCounted + $totalMeetingsCounted)) * 100);
                echo "%";
            }
            else
            {
                echo " <small><sup>N</sup>/<sub>A</sub></small> ";
            }
            echo ")\n";
            
            echo "</div>\n";
        }
        else
        {
            echo "<div class=\"block\">No meetings seem to be scheduled for this semester.</div>\n";
        }
        
        echo "<h2>Officer position: " . ($this->_semester == "fall" ? "Fall" : "Spring") . " " . $this->_year . "</h2>\n";
        $positionInfo = $database->querySingle("SELECT position, positions.handle AS \"positionHandle\", positions.name AS " .
            "\"positionName\", schoolYear, semester, isPerpetual, positions.officeHourRequirements FROM officers JOIN positions ON officers.position = " .
            "positions.positionID WHERE member='" . $memberInfo["memberID"] . "' AND semester='" . $this->_semester .
            "' AND schoolYear='" . $this->_year . "' LIMIT 1", true);
        if ($positionInfo !== false)
        {
            echo "<div class=\"block left\">\n";
            echo "<b>Position:</b> <a href=\"" . WEB_ROOT . "/positions/" . $positionInfo["positionHandle"] . "/\">" .
                $positionInfo["positionName"] . "</a><br />\n";
            if ($positionInfo["officeHourRequirements"] != 0)
            {
                $officeHours = $database->query("SELECT hourID, day, hour, minutes, isHalfHour, effective, terminates FROM " .
                    "officeHours WHERE semester='" . $this->_semester . "' AND schoolYear='" . $positionInfo["schoolYear"] .
                    "' AND member='" . $memberInfo["memberID"] . "' ORDER BY GREATEST(IFNULL(effective, 2), IFNULL(terminates, 2)) DESC");
                $timeBlocks = array("0" => array(0));//0);
                while ($officeHour = $officeHours->fetchArray())
                {
                    $starts = ($officeHour["effective"] != null ? strtotime($officeHour["effective"]) : 0);
                    $ends = ($officeHour["terminates"] != null ? strtotime($officeHour["terminates"]) : -1);
                    if (count($timeBlocks) == 0)
                    {
                        $timeBlocks[$starts] = array(0);//0;
                    }
                    if ($ends >= 0 && !array_key_exists($ends, $timeBlocks))
                    {
                        $previousBlock = $timeBlocks[0];
                        foreach ($timeBlocks as $time => $amount)
                        {
                            if ($time > $ends)
                            {
                                break;
                            }
                            $previousBlock = $amount;
                        }
                        $timeBlocks[$ends] = $previousBlock;
                        ksort($timeBlocks);
                    }
                    if (!array_key_exists($starts, $timeBlocks))
                    {
                        $previousBlock = $timeBlocks[0];
                        foreach ($timeBlocks as $time => $amount)
                        {
                            if ($time > $starts)
                            {
                                break;
                            }
                            $previousBlock = $amount;
                        }
                        $timeBlocks[$starts] = $previousBlock;
                        ksort($timeBlocks);
                    }
                    
                    foreach (array_keys($timeBlocks) as $time)
                    {
                        if ($ends >= 0 && $ends <= $time)
                        {
                            break;
                        }
                        if ($starts > $time)
                        {
                            continue;
                        }
                        $timeBlocks[$time][0] += ($officeHour["isHalfHour"] == "0" ? 1 : 0.5);
                        $timeBlocks[$time][] = $officeHour;
                    }
                }
                
                echo "<div class=\"togetherBlock\"><b>Office hours:</b> <div class=\"togetherBlock\"><ul>\n";
                $index = 0;
                $count = count($timeBlocks);
                $times = array_keys($timeBlocks);
                function convertDay($day)
                {
                    switch ($day)
                    {
                        case "m": return 0;
                        case "t": return 1;
                        case "w": return 2;
                        case "h": return 3;
                        case "f": return 4;
                    }
                }
                function compareHours($hourA, $hourB)
                {
                    if ($hourA["hourID"] == $hourB["hourID"])
                    {
                        return 0;
                    }
                    $dayA = convertDay($hourA["day"]);
                    $dayB = convertDay($hourB["day"]);
                    if ($dayA != $dayB)
                    {
                        return ($dayA - $dayB);
                    }
                    
                    if ($hourA["hour"] != $hourB["hour"])
                    {
                        return ($hourA["hour"] - $hourB["hour"]);
                    }
                    
                    if ($hourA["minutes"] != $hourB["minutes"])
                    {
                        return ($hourA["minutes"] - $hourB["minutes"]);
                    }
                    
                    trigger_error("/member/ error 1: Failed to sort two objects that were not equal but really were.", E_USER_ERROR);
                }
                foreach ($timeBlocks as $time => $hours)
                {
                    $attendance = $database->querySingle("SELECT SUM(attended) AS \"attendedHours\", COUNT(*) AS \"allHours\" " .
                        "FROM officeHourAttendance WHERE member='" . $memberInfo["memberID"] . "' AND `date`>='" .
                        ($time === 0 ? $this->_officeHoursStart : date("Y-m-d", $time)) . "' AND `date`<='" .
                        ($index == $count - 1 ? $this->_officeHoursEnd : date("Y-m-d", $times[$index + 1])) . "'", true);
                    $percentAttended = ($attendance["allHours"] > 0 ? round(($attendance["attendedHours"] /
                        $attendance["allHours"]) * 100) : 100);
                    echo "<li><u>" . ($time === 0 ? "Start" : date(DAYMONTH_FORMAT, $time)) . " &ndash; " .
                        ($index == $count - 1 ? "End" : date(DAYMONTH_FORMAT, $times[$index + 1])) . ":</u> " .
                        array_shift($hours) . " out of " . $positionInfo["officeHourRequirements"] . " hour" .
                        ($positionInfo["officeHourRequirements"] != 1 ? "s" : "") . " scheduled. <span class=\"officeHoursAttended " .
                        ($percentAttended >= OFFICE_HOURS_PERCENT_REQUIRED ? "yes" : "no") . "\" title=\"" .
                        ($percentAttended >= OFFICE_HOURS_PERCENT_REQUIRED ? "Met" : "Did not meet") . " the " .
                        OFFICE_HOURS_PERCENT_REQUIRED . "% threshold.\"><sup>" .
                        ($attendance["attendedHours"] == "" ? "0" : $attendance["attendedHours"]) . "</sup>/<sub>" .
                        $attendance["allHours"] . "</sub> hour" .
                        ($attendance["allHours"] != 1 ? "s" : "") . " attended (" . $percentAttended . "%).</span>";
                    usort($hours, "compareHours");
                    echo "<ul>\n";
                    foreach ($hours as $hour)
                    {
                        echo "<li>";
                        switch ($hour["day"])
                        {
                            case "m":
                            {
                                echo "Monday";
                                break;
                            }
                            case "t":
                            {
                                echo "Tuesday";
                                break;
                            }
                            case "w":
                            {
                                echo "Wednesday";
                                break;
                            }
                            case "h":
                            {
                                echo "Thursday";
                                break;
                            }
                            case "f":
                            {
                                echo "Friday";
                                break;
                            }
                        }
                        echo ", ";
                        $twelveMod = $hour["hour"] % 12;
                        $pm = ($hour["hour"] >= 12);
                        if ($twelveMod == 0)
                        {
                            $twelveMod = 12;
                        }
                        $minutes = $hour["minutes"];
                        echo $twelveMod . ":" . $minutes . " " . ($pm ? "PM" : "AM") . " &ndash; ";
                        if ($hour["isHalfHour"] == "1")
                        {
                            $minutes += 30;
                            if ($minutes >= 60)
                            {
                                $minutes = $minutes % 60;
                                $twelveMod++;
                            }
                        }
                        else
                        {
                            $twelveMod++;
                        }                        
                        if ($twelveMod >= 12)
                        {
                            $twelveMod = $twelveMod % 12;
                            if ($twelveMod == 12)
                            {
                                $pm = !$pm;
                            }
                            if ($twelveMod == 0)
                            {
                                $twelveMod = 12;
                            }
                        }
                        echo $twelveMod . ": " . $minutes . " " . ($pm ? "PM" : "AM");
                        echo "</li>\n";
                    }
                    echo "</ul>\n";
                    echo "</li>\n";
                    $index++;
                }
                echo "</ul></div></div>\n";
            }
            /*echo "<pre>\n";
            var_dump($positionInfo);
            echo "</pre>\n";*/
            echo "</div>\n";
        }
        else
        {
            echo "<div class=\"block\">This member did not have an officer position during this semester.</div>\n";
        }
        
        echo "<h2>Library check-outs</h2>\n";
        $checkouts = $database->query("SELECT item, title, media, checkedOut, checkedIn FROM checkouts JOIN libraryItems ON " .
            "checkouts.item = libraryItems.itemID WHERE member='" . $memberInfo["memberID"] . "' AND checkedOut <= '" .
            $this->_semesterEnds . "' AND (checkedIn IS NULL OR checkedIn >= '" . $this->_semesterStarts . "') ORDER BY " .
            "checkedOut ASC");
        if ($checkouts !== false && $checkouts->numberRows() > 0)
        {
            echo "<div class=\"block left\"><ul>\n";
            while ($checkout = $checkouts->fetchArray())
            {
                echo "<li><a href=\"" . WEB_ROOT . "/library-item/" . $checkout["item"] . "/\">" . $checkout["title"] . "</a>, " .
                    ucfirst($checkout["media"]) . ". (" . date(DATE_FORMAT, strtotime($checkout["checkedOut"])) . " &ndash; ";
                if ($checkout["checkedIn"] != "")
                {
                    echo date(DATE_FORMAT, strtotime($checkout["checkedIn"]));
                }
                else
                {
                    echo "<i>Outstanding</i>";
                }
                echo ").</li>\n";
            }
            echo "</ul></div>\n";
        }
        else
        {
            echo "<div class=\"block\">This member did not have any library check-outs during this semester.</div>\n";
        }
        
        echo "<hr />\n";
        echo "<h2>View other semester records</h2>\n";
        echo "<ul class=\"semesterRecords\">\n";
        foreach ($this->_allYears as $year)
        {            
            echo "<li><a href=\"" . WEB_ROOT . "/member/" . $memberInfo["memberID"] . "/spring-" . ($year + 1) . "/\"" .
                (($year + 1) == $this->_year && $this->_semester == "spring" ? " class=\"current\"" : "") .
                ">Spring " . ($year + 1) . "</a>";
            if ($this->_currentSchoolYear == $year && $this->_currentSemester == "spring")
            {
                echo " <span>(Current semester)</span>";
            }
            echo "</li>\n";
            echo "<li><a href=\"" . WEB_ROOT . "/member/" . $memberInfo["memberID"] . "/fall-" . $year . "/\"" .
                ($year == $this->_year && $this->_semester == "fall" ? " class=\"current\"" : "") . ">Fall " . $year . "</a>";
            if ($this->_currentSchoolYear == $year && $this->_currentSemester == "fall")
            {
                echo " <span>(Current semester)</span>";
            }
            echo "</li>\n";
        }
    }
}
?>