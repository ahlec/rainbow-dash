<?php
class Page
{
    function preRender($database, $session, $arguments)
    {
        global $args;
		
		$today = time();
		if (date("w", $today) == 0 || date("w", $today) == 6)
		{
			$this->finish("error", "The office isn't open&mdash;it's the weekend! Go have some fun somewhere else!");
		}
		if (date("G", $today) < OFFICE_HOURS_BEGIN || date("G", $today) > OFFICE_HOURS_FINISH)
		{
			$this->finish("error", "The office isn't open right now. You should come back during normal office hours.");
		}
		if ($database->querySingle("SELECT count(*) FROM semesters WHERE officeHoursStart <= '" .
			date("Y-m-d", $today) . "' AND officeHoursEnd >= '" . date("Y-m-d", $today) . "'") == 0)
		{
			$this->finish("error", "Office hours don't begin yet. You don't need to worry about signing in right now.");
		}
		if ($database->querySingle("SELECT count(*) FROM officeHolidays WHERE day='" . date("Y-m-d", $today) . "'") > 0)
		{
			$this->finish("error", "Today is a holiday! The office is closed. So, you don't have your office hours today.");
		}
		
		$semesterInfo = getSemester($today);
		$dayEnum = getDayEnum($today);
		
		$officeHourSignins = $database->query("SELECT hourID, hour FROM officeHours WHERE member='" .
			$session->memberID() . "' AND day='" . $dayEnum . "' AND hour >= '" . date("H",
			strtotime("+30 minutes", $today)) . "' AND hour <= '" . date("H", strtotime("+3 hours 30 minutes", $today)) .
			"' AND (effective IS NULL OR effective <= '" . date("Y-m-d", $today) . "') AND (terminates IS NULL OR terminates >= '" .
			date("Y-m-d", $today) . "') AND semester='" . $semesterInfo[0] . "' AND schoolYear='" . $semesterInfo[1] .
			"' ORDER BY hour ASC");
		
		$startHour = null;
		$endHour = null;
		$officeHourIDs = array();
		
		if ($officeHourSignins === false || $officeHourSignins->numberRows() == 0)
		{
			$this->finish("error", "A problem was encountered reading from the database. Please try again.");
		}
		
		while ($signinHour = $officeHourSignins->fetchArray())
		{
			if ($startHour == null)
			{
				if ($signinHour["hour"] > date("H", strtotime("+30 minutes", $today)))
				{
					break;
				}
				$startHour = $signinHour["hour"];
				$endHour = $signinHour["hour"] + 1;
				$officeHourIDs[] = $signinHour["hourID"];
			}
			else if ($endHour >= $signinHour["hour"])
			{
				$endHour = $signinHour["hour"] + 1;
				$officeHourIDs[] = $signinHour["hourID"];
			}
			else
			{
				break;
			}
		}
		
		if (count($officeHourIDs) == 0)
		{
			$this->finish("error", "None of the office hours you attempted to sign in for are currently accepting sign-ins.");
		}
	
		$officeHourIDs = array_unique($officeHourIDs);
		
		$signinIDs = array();
		foreach ($arguments as $officeHourID)
		{
			if (!in_array($officeHourID, $officeHourIDs))
			{
				$this->finish("error", "One or more of the office hours you attempted to sign in for are currently not valid, or not accepting sign-in.");
			}
			$signinIDs[] = $officeHourID;
		}
		
		if (count($officeHourIDs) == 0)
		{
			$this->finish("error", "You must specify at least one office hour in order to sign-in.");
		}
		
		$signinsMade = 0;
		
		foreach ($signinIDs as $officeHourID)
		{
			if ($database->querySingle("SELECT count(*) FROM officeHourAttendance WHERE officeHour='" .
				$database->escapeString($officeHourID) . "' AND `date`='" . date("Y-m-d", $today) . "'") > 0)
			{
				continue;
			}
			
			if (!$database->exec("INSERT INTO officeHourAttendance(officeHour, member, `date`, attended, signedIn) VALUES('" .
				$database->escapeString($officeHourID) . "','" . $database->escapeString($session->memberID()) . "','" .
				date("Y-m-d", $today) . "','1','" . date("Y-m-d H:i:s", $today) . "')"))
			{
				$this->finish("error", "One or more of the office hours could not be marked for attendance in the database. Please try again.");
			}
			else
			{
				$signinsMade++;
			}
		}
		
		if ($signinsMade == 0)
		{
			$this->finish("notice", "All office hours in the selected time period have already been signed in for today. Seems like you're trying to be an overachiever.");
		}
		else
		{
			$this->finish("success", "Your attendance has been recorded. Thanks, and enjoy your time in the office!");
		}
	}
    
	function finish($status, $message)
	{
        global $args;
		global $report;
        $args->setArg($status, $message);
		if ($report == null)
		{
			header ("Location: " . WEB_ROOT . "/");
		}
		else
		{
			header ("Location: " . WEB_ROOT . "/" . $report->pagePath());
		}
		exit();
	}
	function output($session)
    {
        error("You should not be able to be on this page. Obviously an error has occurred somewhere... Just, ignore it?");
    }
}
?>