<?php
class Page
{
    private $_periods = array();
    private $_periodStart = null;
    private $_periodEnd = null;
    private $_semester;
    private $_schoolYear;
    
    function preRender($database, $session, $arguments)
    {
        $semester = getSemester(time());
        $this->_semester = $semester[0];
        $this->_schoolYear = $semester[1];
        $periods = $database->query("SELECT DISTINCT effective AS \"timeKey\" FROM officeHours WHERE semester='" .
            $semester[0] . "' AND schoolYear='" . $semester[1] . "' UNION DISTINCT SELECT terminates AS \"timeKey\" FROM " .
            "officeHours WHERE semester='" . $semester[0] . "' AND schoolYear='" . $semester[1] . "'");
        while ($period = $periods->fetchArray())
        {
            if ($period["timeKey"] == null)
            {
                $this->_periods[] = null;
            }
            else
            {
                $this->_periods[] = strtotime($period["timeKey"]);
            }
        }
        
        function compareTimeKeys($keyA, $keyB)
        {
            if ($keyA == $keyB)
            {
                return 0;
            }
            else if ($keyA == null)
            {
                return -1;
            }
            else if ($keyB == null)
            {
                return 1;
            }
            
            return ($keyA - $keyB);
        }
        
        usort($this->_periods, "compareTimeKeys");
        
        $rightNow = mktime(0, 0, 0);
        foreach ($this->_periods as $timeKey)
        {
            if ($rightNow < $timeKey)
            {
                $this->_periodEnd = $timeKey;
                break;
            }
            else
            {
                $this->_periodStart = $timeKey;
            }
        }
    }
    
    function output($session, $database, $arguments)
    {
        echo "<table class=\"officeHoursChart\">\n";
        
        echo "<tr class=\"header\">\n";
        echo "<td class=\"empty\"></td>\n";
        echo "<th>Monday</th>\n";
        echo "<th>Tuesday</th>\n";
        echo "<th>Wednesday</th>\n";
        echo "<th>Thursday</th>\n";
        echo "<th>Friday</th>\n";
        echo "</tr>\n";
        
        $officeHours = $database->query("SELECT hourID, member, displayName, day, hour, minutes, isHalfHour FROM officeHours " .
            "JOIN members ON officeHours.member = members.memberID WHERE semester='" . $this->_semester . "' AND schoolYear='" .
            $this->_schoolYear . "' AND (effective " . ($this->_periodStart == null ? "IS NULL" : "<='" . date("Y-m-d",
            $this->_periodStart) . "' OR effective IS NULL") . ") AND (terminates " . ($this->_periodEnd == null ? "IS NULL" :
            ">='" . date("Y-m-d", $this->_periodEnd) . "' OR terminates IS NULL") . ") ORDER BY hour ASC, minutes ASC, day ASC, displayName ASC");
        
        $currentOfficeHour = $officeHours->fetchArray();
        
        for ($hour = 9; $hour <= 17; $hour++)
        {
            echo "<tr>\n";
            
            echo "  <th>";
            $twelveMod = $hour % 12;
            if ($twelveMod == 0)
            {
                $twelveMod = "Noon";
            }
            else
            {
                $twelveMod .= ":00";
            }
            $upperMod = ($hour + 1) % 12;
            if ($upperMod == 0)
            {
                $upperMod = "Noon";
            }
            else
            {
                $upperMod .= ":00";
            }
            echo $twelveMod;
            echo " &ndash; ";
            echo $upperMod;
            echo "</th>\n";
            
            echo "<td>";
            while ($currentOfficeHour !== false && $currentOfficeHour["day"] == "m" && $currentOfficeHour["hour"] == $hour)
            {
                echo "<a href=\"/member/" . $currentOfficeHour["member"] . "/\">" . $currentOfficeHour["displayName"] . "</a>";
                $currentOfficeHour = $officeHours->fetchArray();
            }
            echo "</td>\n";
            
            echo "<td>";
            while ($currentOfficeHour !== false && $currentOfficeHour["day"] == "t" && $currentOfficeHour["hour"] == $hour)
            {
                echo "<a href=\"/member/" . $currentOfficeHour["member"] . "/\">" . $currentOfficeHour["displayName"] . "</a>";
                $currentOfficeHour = $officeHours->fetchArray();
            }
            echo "</td>\n";
            
            echo "<td>";
            while ($currentOfficeHour !== false && $currentOfficeHour["day"] == "w" && $currentOfficeHour["hour"] == $hour)
            {
                echo "<a href=\"/member/" . $currentOfficeHour["member"] . "/\">" . $currentOfficeHour["displayName"] . "</a>";
                $currentOfficeHour = $officeHours->fetchArray();
            }
            echo "</td>\n";
            
            echo "<td>";
            while ($currentOfficeHour !== false && $currentOfficeHour["day"] == "h" && $currentOfficeHour["hour"] == $hour)
            {
                echo "<a href=\"/member/" . $currentOfficeHour["member"] . "/\">" . $currentOfficeHour["displayName"] . "</a>";
                $currentOfficeHour = $officeHours->fetchArray();
            }
            echo "</td>\n";
            
            echo "<td>";
            while ($currentOfficeHour !== false && $currentOfficeHour["day"] == "f" && $currentOfficeHour["hour"] == $hour)
            {
                echo "<a href=\"/member/" . $currentOfficeHour["member"] . "/\">" . $currentOfficeHour["displayName"] . "</a>";
                $currentOfficeHour = $officeHours->fetchArray();
            }
            echo "</td>\n";
            
            echo "</tr>\n";
        }
        
        echo "</table>\n";
    }
}
?>