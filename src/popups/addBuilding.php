<?php
/* setup */
require_once ("/home/deitloff/public_html/rainbow-dash/lib/config.inc.php");
$session = session();
$database = database();
$report = report();

$error = null;

if (isset($_POST["name"]))
{
    $name = $database->escapeString($_POST["name"]);
    $streetAddress = $database->escapeString($_POST["streetAddress"]);
    $isUniversity = ($_POST["isUniversity"] == "true");
    $university = ($isUniversity ? $database->escapeString($_POST["university"]) : null);
    
    if (mb_strlen($name) > 0 && mb_strlen($name) <= 50)
    {
        if (!$isUniversity || $database->querySingle("SELECT count(*) FROM universities WHERE universityID='" .
            $university . "'") == 1)
        {
            if ($database->querySingle("SELECT count(*) FROM buildings WHERE name like '" . $name . "'" .
                ($isUniversity ? " AND isUniversity='1' AND university='" . $university . "'" : " AND isUniversity='0'")) == 0)
            {
                if (mb_strlen($streetAddress) > 0 && mb_strlen($streetAddress) <= 100)
                {
                    if ($database->exec("INSERT INTO buildings(name, streetAddress, isUniversity, university) VALUES('" .
                        $name . "','" . $streetAddress . "','" . ($isUniversity ? "1" : "0") . "','" . ($isUniversity ? 
                        $university : "") . "')"))
                    {
                        $buildingID = $database->getLastAutoInc();
                        echo "<script>
                            var buildingsSelect = window.opener.document.getElementById('building');
                            if (buildingsSelect)
                            {
                                var newOption = window.opener.document.createElement('option');
                                newOption.value = '" . $buildingID . "';
                                newOption.text = '" . $name . ($isUniversity ? " (" .
                                    $database->querySingle("SELECT name FROM universities WHERE universityID='" .
                                    $university . "' LIMIT 1") . ")" : "") . "';
                                try
                                {
                                    buildingsSelect.add(newOption, null);
                                }
                                catch (ex)
                                {
                                    buildingsSelect.add(newOption);
                                }
                                buildingsSelect.selectedIndex = buildingsSelect.options.length - 1;
                            }
                            window.close();
                            </script>";
                        exit();
                    }
                    else
                    {
                        $error = "Could not add the building. There was a database error. Please try again.";
                    }
                }
                else
                {
                    $streetAddrerss = "Street address must be at least one character and less than or equal to one hundred characters in length.";
                }
            }
            else
            {
                $error = "There exists a building already " . ($isUniversity ? "at this university " : "") . "with this name.";
            }
        }
        else
        {
            $error = "Universities must exist within the database. You didn't even use the form, did you?";
        }
    }
    else
    {
        $error = "Names must be at least one character and less than fifty characters in length.";
    }
    
    if (mb_strlen($handle) > 0)
    {
        if ($database->querySingle("SELECT count(*) FROM meetingTypes WHERE handle='" . $handle . "'") == 0)
        {
            if ($database->exec("INSERT INTO meetingTypes(handle) VALUES('" . $handle . "')"))
            {
                $meetingTypeID = $database->getLastAutoInc();
                echo "<script>
                    var meetingTypesSelect = window.opener.document.getElementById('meetingType');
                    if (meetingTypesSelect)
                    {
                        var newOption = window.opener.document.createElement('option');
                        newOption.value = '" . $meetingTypeID . "';
                        newOption.text = '" . $handle . "';
                        try
                        {
                            meetingTypesSelect.add(newOption, null);
                        }
                        catch (ex)
                        {
                            meetingTypesSelect.add(newOption);
                        }
                        meetingTypesSelect.selectedIndex = meetingTypesSelect.options.length - 1;
                    }
                    window.close();
                    </script>";
                exit();
            }
            else
            {
                $error = "Could not add the meeting type. There was a database error. Please try again.";
            }
        }
        else
        {
            $error = "A meeting type with this name already exists.";
        }
    }
    else
    {
        $error = "Names must be at least one character in length.";
    }
}
?>
<!DOCTYPE html>
<html>
  <head>
    <title>Rainbow Dash</title>
    <meta charset="UTF-8">
    <meta http-equiv="content-type" content="text/html; charset=utf-8">
    <link rel="stylesheet" href="<?php echo WEB_ROOT; ?>/style/content.css" />
    <link rel="stylesheet" href="<?php echo WEB_ROOT; ?>/style/popup.css" />
    <script src="<?php echo WEB_ROOT; ?>/style/popups.js"></script>
    <script> var WEB_ROOT = '<?php echo WEB_ROOT; ?>';</script>
    <script>
        function toggledIsUniversity(checkboxElement)
        {
            document.getElementById("university-block").style.display = (checkboxElement.checked ? "block" : "none");
        }
    </script>
	<base href="<?php echo WEB_ROOT; ?>/"/>
  </head>
  <body>
	<div class="container">
		<div class="center">
        <h1>Add new building</h1>
        <?php
            if ($error != null)
            {
                error($error);
            }
        ?>
            <form method="POST" action="<?php echo WEB_ROOT; ?>/popups/addBuilding.php">
                <label for="name">Name:</label>
                <input type="text" id="name" name="name" maxlength="50" autofocus="autofocus" /><br />
                <label for="streetAdress">Street address:</label>
                <input type="text" id="streetAddress" name="streetAddress" maxlength="100" /><br />
                <label for="isUniversity">Is university building:</label>
                <input type="checkbox" id="isUniversity" name="isUniversity" checked="checked" value="true"
                    onchange="toggledIsUniversity(this);" /><small>(<i>any</i> university)</small><br />
                <div id="university-block">
                <label for="university">University:</label>
                <select id="university" name="university" class="followed">
                <?php
                    $universities = $database->query("SELECT universityID, name FROM universities");
                    while ($university = $universities->fetchArray())
                    {
                        echo "<option value=\"" . $university["universityID"] . "\">" . $university["name"] . "</option>\n";
                    }
                ?>
                </select>
                <input type="button" value="Add" class="addButton" onclick="addUniversity();" /><br />
                </div>
                <input type="submit" value="Add building" />
            </form>
        </div>
	</div>
  </body>
</html>