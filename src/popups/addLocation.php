<?php
/* setup */
require_once ("/home/deitloff/public_html/rainbow-dash/lib/config.inc.php");
$session = session();
$database = database();
$report = report();

$error = null;

if (isset($_POST["building"]) && isset($_POST["room"]) && isset($_POST["capacity"]))
{
    $building = $database->escapeString($_POST["building"]);
    $room = $database->escapeString($_POST["room"]);
    $capacity = $database->escapeString($_POST["capacity"]);
    if ($database->querySingle("SELECT count(*) FROM buildings WHERE buildingID='" . $building . "'") == 1)
    {
        if (mb_strlen($room) > 0 && mb_strlen($room) <= 40)
        {
            if ($database->querySingle("SELECT count(*) FROM locations WHERE room like '" . $room . "' AND building='" .
                $building ."'") == 0)
            {
                if ($database->exec("INSERT INTO locations(building, room, capacity) VALUES('" . $building . "','" .
                    $room . "','" . $capacity . "')"))
                {
                    $locationID = $database->getLastAutoInc();
                    echo "<script>
                        var locationsSelect = window.opener.document.getElementById('location');
                        if (locationsSelect)
                        {
                            var newOption = window.opener.document.createElement('option');
                            newOption.value = '" . $locationID . "';
                            newOption.text = '" . $room . ", " .
                                $database->escapeString($database->querySingle("SELECT name FROM buildings WHERE buildingID='" .
                                $building . "' LIMIT 1")) . "';
                            try
                            {
                                locationsSelect.add(newOption, null);
                            }
                            catch (ex)
                            {
                                locationsSelect.add(newOption);
                            }
                            locationsSelect.selectedIndex = locationsSelect.options.length - 1;
                        }
                        window.close();
                        </script>";
                    exit();
                }
                else
                {
                    $error = "Could not add the location. There was a database error. Please try again.";
                }
            }
            else
            {
                $error = "This room and building combination already exists in the database.";
            }
        }
        else
        {
            $error = "Room specifications must be at least one character and less than or equal to forty characters in length.";
        }
    }
    else
    {
        $error = "Buildings must exist in the database. Did you even use the form?";
    }
}
?>
<!DOCTYPE html>
<html>
  <head>
    <title>Rainbow Dash</title>
    <meta charset="UTF-8">
    <meta http-equiv="content-type" content="text/html; charset=utf-8">
    <link rel="stylesheet" href="<?php echo WEB_ROOT; ?>/style/content.css" />
    <link rel="stylesheet" href="<?php echo WEB_ROOT; ?>/style/popup.css" />
    <script src="<?php echo WEB_ROOT; ?>/style/popups.js"></script>
    <script> var WEB_ROOT = '<?php echo WEB_ROOT; ?>';</script>
	<base href="<?php echo WEB_ROOT; ?>/"/>
  </head>
  <body>
	<div class="container">
		<div class="center">
        <h1>Add new location</h1>
        <?php
            if ($error != null)
            {
                error($error);
            }
        ?>
            <form method="POST" action="<?php echo WEB_ROOT; ?>/popups/addLocation.php">
                <label for="building">Building:</label>
                <select name="building" id="building" autofocus="autofocus" class="followed">
                <?php
                    $buildings = $database->query("SELECT buildingID, buildings.name, isUniversity, " .   
                        "universities.name AS \"universityName\" FROM buildings JOIN universities ON buildings.university = " .
                        "universities.universityID ORDER BY buildingID ASC");
                    while ($building = $buildings->fetchArray())
                    {
                        echo "<option value=\"" . $building["buildingID"] . "\">" . $building["name"];
                        if ($building["isUniversity"] == "1")
                        {
                            echo " (" . $building["universityName"] . ")";
                        }
                        echo "</option>";
                    }
                ?>
                </select>
                <input type="button" value="Add" class="addButton" onclick="addBuilding();" /><br />
                <label for="room">Room:</label>
                <input type="text" id="room" name="room" maxlength="40" /><br />
                <label for="capacity">Capacity:</label>
                <input type="text" id="capacity" name="capacity" /><br />
                <input type="submit" value="Add location" />
            </form>
        </div>
	</div>
  </body>
</html>