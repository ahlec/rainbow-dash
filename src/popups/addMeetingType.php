<?php
/* setup */
require_once ("/home/deitloff/public_html/rainbow-dash/lib/config.inc.php");
$session = session();
$database = database();
$report = report();

$error = null;

if (isset($_POST["handle"]))
{
    $handle = $database->escapeString($_POST["handle"]);
    $isLounge = (isset($_POST["isLounge"]) && $_POST["isLounge"] == "yes");
    if (mb_strlen($handle) > 0)
    {
        if ($database->querySingle("SELECT count(*) FROM meetingTypes WHERE handle='" . $handle . "'") == 0)
        {
            if ($database->exec("INSERT INTO meetingTypes(handle, isLounge) VALUES('" . $handle . "','" . ($isLounge ?
                "1" : "0") . "')"))
            {
                $meetingTypeID = $database->getLastAutoInc();
                echo "<script>
                    var meetingTypesSelect = window.opener.document.getElementById('meetingType');
                    if (meetingTypesSelect)
                    {
                        var newOption = window.opener.document.createElement('option');
                        newOption.value = '" . $meetingTypeID . "';
                        newOption.text = '" . ($isLounge ? "[Lounge] " : "") . $handle . "';
                        try
                        {
                            meetingTypesSelect.add(newOption, null);
                        }
                        catch (ex)
                        {
                            meetingTypesSelect.add(newOption);
                        }
                        meetingTypesSelect.selectedIndex = meetingTypesSelect.options.length - 1;
                    }
                    window.close();
                    </script>";
                exit();
            }
            else
            {
                $error = "Could not add the meeting type. There was a database error. Please try again.";
            }
        }
        else
        {
            $error = "A meeting type with this name already exists.";
        }
    }
    else
    {
        $error = "Names must be at least one character in length.";
    }
}
?>
<!DOCTYPE html>
<html>
  <head>
    <title>Rainbow Dash</title>
    <meta charset="UTF-8">
    <meta http-equiv="content-type" content="text/html; charset=utf-8">
    <link rel="stylesheet" href="<?php echo WEB_ROOT; ?>/style/popup.css" />
    <link rel="stylesheet" href="<?php echo WEB_ROOT; ?>/style/content.css" />
	<base href="<?php echo WEB_ROOT; ?>/"/>
  </head>
  <body>
	<div class="container">
		<div class="center">
        <h1>Add new meeting type</h1>
        <?php
            if ($error != null)
            {
                error($error);
            }
        ?>
            <form method="POST" action="<?php echo WEB_ROOT; ?>/popups/addMeetingType.php">
                <label for="handle">Name:</label>
                <input type="text" id="handle" name="handle" autofocus="autofocus" /><br />
                <label for="isLounge">Is Lounge:</label>
                <input type="checkbox" id="isLounge" name="isLounge" value="yes" /><br />
                <input type="submit" value="Add meeting type" />
            </form>
        </div>
	</div>
  </body>
</html>