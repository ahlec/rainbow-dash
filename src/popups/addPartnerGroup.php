<?php
/* setup */
require_once ("/home/deitloff/public_html/rainbow-dash/lib/config.inc.php");
$session = session();
$database = database();
$report = report();

$error = null;

if (isset($_POST["name"]))
{
    $name = $database->escapeString($_POST["name"]);
    $isUniversity = ($_POST["isUniversity"] == "true");
    $university = ($isUniversity ? $database->escapeString($_POST["university"]) : null);
    
    if (mb_strlen($name) > 0 && mb_strlen($name) <= 50 && !ctype_space($name))
    {
        if (!$isUniversity || $database->querySingle("SELECT count(*) FROM universities WHERE universityID='" .
            $university . "'") == 1)
        {
            if ($database->querySingle("SELECT count(*) FROM partnerGroups WHERE name like '" . $name . "'" .
                ($isUniversity ? " AND isUniversityGroup='1' AND university='" . $university . "'" :
                " AND isUniversityGroup='0'")) == 0)
            {
                if ($database->exec("INSERT INTO partnerGroups(name, isUniversityGroup, university) VALUES('" .
                    $name . "','" . ($isUniversity ? "1" : "0") . "','" . ($isUniversity ? 
                    $university : "") . "')"))
                {
                    $groupID = $database->getLastAutoInc();
                    echo "<script>
                        var partnersSelect = window.opener.document.getElementById('partners');
                        if (partnersSelect)
                        {
                            var newOption = window.opener.document.createElement('option');
                            newOption.value = '" . $groupID . "';
                            newOption.text = '" . $name . "';
                            try
                            {
                                partnersSelect.add(newOption, null);
                            }
                            catch (ex)
                            {
                                partnersSelect.add(newOption);
                            }
                            if (partnersSelect.disabled)
                            {
                                partnersSelect.disabled = false;
                            }
                        }
                        window.close();
                        </script>";
                    exit();
                }
                else
                {
                    $error = "Could not add the partner group. There was a database error. Please try again.";
                }
            }
            else
            {
                $error = "There exists a partner group already " . ($isUniversity ? "at this university " : "") . "with this name.";
            }
        }
        else
        {
            $error = "Universities must exist within the database. You didn't even use the form, did you?";
        }
    }
    else
    {
        $error = "Names must be at least one character in length.";
    }
}
?>
<!DOCTYPE html>
<html>
  <head>
    <title>Rainbow Dash</title>
    <meta charset="UTF-8">
    <meta http-equiv="content-type" content="text/html; charset=utf-8">
    <link rel="stylesheet" href="<?php echo WEB_ROOT; ?>/style/content.css" />
    <link rel="stylesheet" href="<?php echo WEB_ROOT; ?>/style/popup.css" />
    <script src="<?php echo WEB_ROOT; ?>/style/popups.js"></script>
    <script> var WEB_ROOT = '<?php echo WEB_ROOT; ?>';</script>
    <script>
        function toggledIsUniversity(checkboxElement)
        {
            document.getElementById("university-block").style.display = (checkboxElement.checked ? "block" : "none");
        }
    </script>
	<base href="<?php echo WEB_ROOT; ?>/"/>
  </head>
  <body>
	<div class="container">
		<div class="center">
        <h1>Add new partner group</h1>
        <?php
            if ($error != null)
            {
                error($error);
            }
        ?>
            <form method="POST" action="<?php echo WEB_ROOT; ?>/popups/addPartnerGroup.php">
                <label for="name">Name:</label>
                <input type="text" id="name" name="name" autofocus="autofocus" /><br />
                <label for="isUniversity">Is university group:</label>
                <input type="checkbox" id="isUniversity" name="isUniversity" checked="checked" value="true"
                    onchange="toggledIsUniversity(this);" /><small>(<i>any</i> university)</small><br />
                <div id="university-block">
                <label for="university">University:</label>
                <select id="university" name="university" class="followed">
                <?php
                    $universities = $database->query("SELECT universityID, name FROM universities");
                    while ($university = $universities->fetchArray())
                    {
                        echo "<option value=\"" . $university["universityID"] . "\">" . $university["name"] . "</option>\n";
                    }
                ?>
                </select>
                <input type="button" value="Add" class="addButton" onclick="addUniversity();" /><br />
                </div>
                <input type="submit" value="Add partner group" />
            </form>
        </div>
	</div>
  </body>
</html>