<?php
/* setup */
require_once ("/home/deitloff/public_html/rainbow-dash/lib/config.inc.php");
$session = session();
$database = database();
$report = report();

$error = null;

if (isset($_POST["name"]))
{
    $name = $database->escapeString($_POST["name"]);
    if (mb_strlen($name) > 0 && mb_strlen($name) <= 100)
    {
        if ($database->querySingle("SELECT count(*) FROM universities WHERE name LIKE '" . $name . "'") == 0)
        {
            if ($database->exec("INSERT INTO universities(name) VALUES('" . $name . "')"))
            {
                $universityID = $database->getLastAutoInc();
                echo "<script>
                    var universitiesSelect = window.opener.document.getElementById('university');
                    if (universitiesSelect)
                    {
                        var newOption = window.opener.document.createElement('option');
                        newOption.value = '" . $universityID . "';
                        newOption.text = '" . $name . "';
                        try
                        {
                            universitiesSelect.add(newOption, null);
                        }
                        catch (ex)
                        {
                            universitiesSelect.add(newOption);
                        }
                        universitiesSelect.selectedIndex = universitiesSelect.options.length - 1;
                    }
                    window.close();
                    </script>";
                exit();
            }
            else
            {
                $error = "Could not add the university. There was a database error. Please try again.";
            }
        }
        else
        {
            $error = "A university with this name already exists in the database.";
        }
    }
    else
    {
        $error = "Names must be at least one character and less than a hundred characters in length.";
    }
}
?>
<!DOCTYPE html>
<html>
  <head>
    <title>Rainbow Dash</title>
    <meta charset="UTF-8">
    <meta http-equiv="content-type" content="text/html; charset=utf-8">
    <link rel="stylesheet" href="<?php echo WEB_ROOT; ?>/style/popup.css" />
    <link rel="stylesheet" href="<?php echo WEB_ROOT; ?>/style/content.css" />
	<base href="<?php echo WEB_ROOT; ?>/"/>
  </head>
  <body>
	<div class="container">
		<div class="center">
        <h1>Add new university</h1>
        <?php
            if ($error != null)
            {
                error($error);
            }
        ?>
            <form method="POST" action="<?php echo WEB_ROOT; ?>/popups/addUniversity.php">
                <label for="name">Name:</label>
                <input type="text" id="name" name="name" autofocus="autofocus" /><br />
                <input type="submit" value="Add university" />
            </form>
        </div>
	</div>
  </body>
</html>