function onMediaTypeChanged()
{
    var mediaTypeSelect = document.getElementById("type");
    var selectedType = mediaTypeSelect.options[mediaTypeSelect.selectedIndex].value;
    if (selectedType == "book")
    {
        document.getElementById("bookInputMode-block").style.display = "block";
        document.getElementById("publication-block").style.display = "none";
        document.getElementById("isbn-block").style.display = (document.getElementById("bookInputMode-isbn").checked ?
            "block" : "none");
        document.getElementById("authors-block").style.display = (document.getElementById("bookInputMode-manual").checked ?
            "block" : "none");
        document.getElementById("description-block").style.display = (document.getElementById("bookInputMode-manual").checked ?
            "block" : "none");
        document.getElementById("manual-block").style.display = (document.getElementById("bookInputMode-manual").checked ?
            "block" : "none");
    }
    else if (selectedType == "magazine")
    {
        document.getElementById("isbn-block").style.display = "none";
        document.getElementById("authors-block").style.display = "none";
        document.getElementById("bookInputMode-block").style.display = "none";
        document.getElementById("description-block").style.display = "none";
        document.getElementById("manual-block").style.display = "block";
        document.getElementById("publication-block").style.display = "block";
    }
    else
    {
        document.getElementById("isbn-block").style.display = "none";
        document.getElementById("authors-block").style.display = "none";
        document.getElementById("bookInputMode-block").style.display = "none";
        document.getElementById("description-block").style.display = "block";
        document.getElementById("manual-block").style.display = "block";
        document.getElementById("publication-block").style.display = "none";
    }
}

function onBookInputModeChanged()
{
    if (document.getElementById("bookInputMode-manual").checked)
    {
        document.getElementById("isbn-block").style.display = "none";
        document.getElementById("manual-block").style.display = "block";
        document.getElementById("authors-block").style.display = "block";
    }
    else
    {
        document.getElementById("isbn-block").style.display = "block";
        document.getElementById("authors-block").style.display = "none";
        document.getElementById("manual-block").style.display = "none";
    }
}