function partnerSelectSwitched()
{
    var partnersSelect = document.getElementById("partners");
    var partnersBlock = document.getElementById("partners-block");
    
    var selectedPartner = partnersSelect.options[partnersSelect.selectedIndex];
    
    if (selectedPartner.value == "EMPTY")
    {
        return;
    }
    
    partnersSelect.remove(partnersSelect.selectedIndex);
    
    if (partnersSelect.length == 1)
    {
        partnersSelect.disabled = true;
        partnersSelect.blur();
    }
    
    var partnerLabel = document.createElement("span");
    partnerLabel.setAttribute("id", "partnerLabel-" + selectedPartner.value);
    partnerLabel.className = "partnerLabel";
    
    var partnerHiddenLabel = document.createElement("input");
    partnerHiddenLabel.setAttribute("type", "hidden");
    partnerHiddenLabel.setAttribute("name", "partner-" + selectedPartner.value);
    partnerHiddenLabel.setAttribute("value", "1");
    
    partnerLabel.appendChild(partnerHiddenLabel);
    
    var remove = document.createElement("span");
    remove.className = "remove";
    remove.setAttribute("onclick", "removePartner('" + selectedPartner.value + "')");
    
    var partnerName = document.createTextNode(selectedPartner.text);
    partnerLabel.appendChild(remove);
    partnerLabel.appendChild(partnerName);
    
    partnersBlock.appendChild(partnerLabel);
}

function removePartner(partnerID)
{
    var label = document.getElementById("partnerLabel-" + partnerID);
    label.parentNode.removeChild(label);
    
    var partnersSelect = document.getElementById("partners");
    
    var partnerOption = document.createElement("option");
    partnerOption.value = partnerID;
    partnerOption.text = label.childNodes[2].data;
    
    partnersSelect.add(partnerOption);
    if (partnersSelect.disabled)
    {
        partnersSelect.disabled = false;
    }
}