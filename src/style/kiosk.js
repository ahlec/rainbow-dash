var pfx = ["webkit", "moz", "ms", "o", ""];
function RunPrefixMethod(obj, method) {
	var p = 0, m, t;
	while (p < pfx.length && !obj[m]) {
		m = method;
		if (pfx[p] == "") {
			m = m.substr(0,1).toLowerCase() + m.substr(1);
		}
		m = pfx[p] + m;
		t = typeof obj[m];
		if (t != "undefined") {
			pfx = [pfx[p]];
			return (t == "function" ? obj[m](Element.ALLOW_KEYBOARD_INPUT) : obj[m]);
		}
		p++;
	}
}
function requestFullScreen()
{
    if (!RunPrefixMethod(document, "FullScreen") && !RunPrefixMethod(document, "IsFullScreen"))
    {
		RunPrefixMethod(document.getElementById("kiosk"), "RequestFullScreen");
	}
}

function performAJAX(ajaxCall, successCallback)
{
  var AJAX;
  try
  {
    AJAX = new XMLHttpRequest();
  }
  catch(e)
  {
    try
    {
      AJAX = new ActiveXObject("Msxml2.XMLHTTP");
    }
    catch(e)
    {
      try
      {
        AJAX = new ActiveXObject("Microsoft.XMLHTTP");
      }
      catch(e)
      {
        alert("Your browser does not support AJAX.");
        return false;
      }
    }
  }
  AJAX.onreadystatechange = function()
  {
    if (AJAX.readyState == 4)
    {
      if (AJAX.status == 200)
	  {
        successCallback.call(null, AJAX.responseText);
      }
	  else
        alert("There was a problem! Please try again!");
    }
  }
  AJAX.open("get", WEB_ROOT + "/kiosk-scripts/" + ajaxCall, true);
  AJAX.send(null);
}

function signin(meetingID)
{
    var usernameBox = document.getElementById("username");
    var submitButton = document.getElementById("loginButton");
    performAJAX("signin.php?meetingID=" + meetingID + "&username=" + usernameBox.value, signinCallback);
    usernameBox.disabled = true;
    submitButton.disabled = true;
}
function signinCallback(returnValue)
{
    var usernameBox = document.getElementById("username");
    var submitButton = document.getElementById("loginButton");
    if (returnValue == "success")
    {
        alert("You're all signed in! Enjoy the meeting!");
        usernameBox.value = "";
        usernameBox.disabled = false;
        submitButton.disabled = false;
    }
    else if (returnValue == "notRegistered")
    {
        if (confirm("This username is not currently registered in the database. In order to sign in, you need to register yourself. Would you like to do so now?"))
        {
            firstName = prompt("What is your first name (legal/preferred full first name, please -- we'll get to nicknames later). Note that if your legal name is not your preferred name, you may use the latter, but please use your full preferred name.");
            lastName = prompt("What is your last name?");
            displayName = prompt("How would you like your name to be displayed in the system? You may choose anything for this, so long as no one else has already chosen it as their own.", firstName + " " + lastName);
            alert("First name: '" + firstName + "'\nLast name: '" + lastName + "'\nDisplay name: '" + displayName + "'");
        }
        else
        {
            alert("We understand :) If you change your mind, you can always come back here later.");
            usernameBox.disabled = false;
            usernameBox.value = "";
            submitButton.disabled = false;
        }
    }
    else
    {
        alert(returnValue);
        usernameBox.disabled = false;
        submitButton.disabled = false;
    }
}