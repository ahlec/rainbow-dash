function addMeetingType()
{
    window.open(WEB_ROOT + "/popups/addMeetingType.php", "",
        "height=300,width=600,resizable=0,scrollbars=0,location=no,toolbar=0,status=0,dialog=yes,left=" +
        ((screen.width / 2) - 300) + ",top=" + ((screen.height / 2) - 150) + ",modal=yes");
}
function addLocation()
{
    window.open(WEB_ROOT + "/popups/addLocation.php", "",
        "height=400,width=600,resizable=0,scrollbars=0,location=no,toolbar=0,status=0,dialog=yes,left=" +
        ((screen.width / 2) - 300) + ",top=" + ((screen.height / 2) - 200) + ",modal=yes");
}
function addBuilding()
{
    window.open(WEB_ROOT + "/popups/addBuilding.php", "",
        "height=400,width=600,resizable=0,scrollbars=0,location=no,toolbar=0,status=0,dialog=yes,left=" +
        ((screen.width / 2) - 300) + ",top=" + ((screen.height / 2) - 200) + ",modal=yes");
}
function addUniversity()
{
    window.open(WEB_ROOT + "/popups/addUniversity.php", "",
        "height=300,width=600,resizable=0,scrollbars=0,location=no,toolbar=0,status=0,dialog=yes,left=" +
        ((screen.width / 2) - 300) + ",top=" + ((screen.height / 2) - 150) + ",modal=yes");
}
function addPartnerGroup()
{
    window.open(WEB_ROOT + "/popups/addPartnerGroup.php", "",
        "height=300,width=600,resizable=0,scrollbars=0,location=no,toolbar=0,status=0,dialog=yes,left=" +
        ((screen.width / 2) - 300) + ",top=" + ((screen.height / 2) - 150) + ",modal=yes");
}